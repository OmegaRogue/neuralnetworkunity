﻿namespace NeuralNodes.Editor
{
	public static class SlotValueTypeUtil
	{
		public static string ToClassName(this SlotValueType type)
		{
			return ConcreteSlotValueTypeClassNames[(int)type];
		}

		private static readonly string[] ConcreteSlotValueTypeClassNames =
		{
			null,
			"typeVector4",
			"typeVector3",
			"typeVector2",
			"typeVector1",
			"typeBoolean",
			"typeCurvePrimitive",
			"typeMesh",
			"typeVertexList",
			"typeFunctionOutput"
			
		};
	}
}