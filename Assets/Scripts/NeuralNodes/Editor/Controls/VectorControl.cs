using System;
using System.Linq;
using System.Reflection;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Extensions;

using UnityEditor;
using UnityEditor.UIElements;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Controls
{
    [AttributeUsage(AttributeTargets.Property)]
    public class VectorControlAttribute : Attribute, INodeControlAttribute
    {
        private readonly string _mLabel;
        private readonly string _mSubLabel1;
        private readonly string _mSubLabel2;
        private readonly string _mSubLabel3;
        private readonly string _mSubLabel4;

        public VectorControlAttribute(string label = null, string subLabel1 = "X", string subLabel2 = "Y", string subLabel3 = "Z", string subLabel4 = "W")
        {
            _mSubLabel1 = subLabel1;
            _mSubLabel2 = subLabel2;
            _mSubLabel3 = subLabel3;
            _mSubLabel4 = subLabel4;
            _mLabel = label;
        }

        public VisualElement InstantiateControl(AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo)
        {
            if (!VectorControlView.ValidTypes.Contains(propertyInfo.PropertyType))
                return null;
            return new VectorControlView(_mLabel, _mSubLabel1, _mSubLabel2, _mSubLabel3, _mSubLabel4, neuralNodeEditor, propertyInfo);
        }
    }

    public class VectorControlView : VisualElement
    {
        public static Type[] ValidTypes = { typeof(float), typeof(Vector2), typeof(Vector3), typeof(Vector4) };

        private readonly AbstractNeuralNodeEditor _mNeuralNodeEditor;
        private readonly PropertyInfo _mPropertyInfo;
        private Vector4 _mValue;
        private int _mUndoGroup = -1;

        public VectorControlView(string label, string subLabel1, string subLabel2, string subLabel3, string subLabel4, AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo)
        {
            var components = Array.IndexOf(ValidTypes, propertyInfo.PropertyType) + 1;
            if (components == -1)
                throw new ArgumentException("Property must be of type float, Vector2, Vector3 or Vector4.", "propertyInfo");

            this.LoadAndAddStyleSheet("Styles/VectorControlView");
            _mNeuralNodeEditor = neuralNodeEditor;
            _mPropertyInfo = propertyInfo;

            label = label ?? ObjectNames.NicifyVariableName(propertyInfo.Name);
            if (!string.IsNullOrEmpty(label))
                Add(new Label(label));

            _mValue = GetValue();
            AddField(0, subLabel1);
            if (components > 1)
                AddField(1, subLabel2);
            if (components > 2)
                AddField(2, subLabel3);
            if (components > 3)
                AddField(3, subLabel4);
        }

        private void AddField(int index, string subLabel)
        {
            var dummy = new VisualElement { name = "dummy" };
            var label = new Label(subLabel);
            dummy.Add(label);
            Add(dummy);
            var field = new FloatField { userData = index, value = _mValue[index] };
//            var dragger = new FieldMouseDragger<float>(field);
//            dragger.SetDragZone(label);
            field.RegisterCallback<MouseDownEvent>(Repaint);
            field.RegisterCallback<MouseMoveEvent>(Repaint);
            field.RegisterValueChangedCallback(evt =>
                {
                    var value = GetValue();
                    value[index] = (float)evt.newValue;
                    SetValue(value);
                    _mUndoGroup = -1;
                    MarkDirtyRepaint();
                });
            field.RegisterCallback<InputEvent>(evt =>
                {
                    if (_mUndoGroup == -1)
                    {
                        _mUndoGroup = Undo.GetCurrentGroup();
                        _mNeuralNodeEditor.Owner.NeuralGraphEditorObject.RegisterCompleteObjectUndo("Change " + _mNeuralNodeEditor.ToString());
                    }

                    if (!float.TryParse(evt.newData, out var newValue))
                        newValue = 0f;
                    var value = GetValue();
                    value[index] = newValue;
                    SetValue(value);
                    MarkDirtyRepaint();
                });
            field.RegisterCallback<KeyDownEvent>(evt =>
                {
                    if (evt.keyCode == KeyCode.Escape && _mUndoGroup > -1)
                    {
                        Undo.RevertAllDownToGroup(_mUndoGroup);
                        _mUndoGroup = -1;
                        _mValue = GetValue();
                        evt.StopPropagation();
                    }
                    MarkDirtyRepaint();
                });
            Add(field);
        }

        private object ValueToPropertyType(Vector4 value)
        {
            if (_mPropertyInfo.PropertyType == typeof(float))
                return value.x;
            if (_mPropertyInfo.PropertyType == typeof(Vector2))
                return (Vector2)value;
            if (_mPropertyInfo.PropertyType == typeof(Vector3))
                return (Vector3)value;
            return value;
        }

        private Vector4 GetValue()
        {
            var value = _mPropertyInfo.GetValue(_mNeuralNodeEditor, null);
            if (_mPropertyInfo.PropertyType == typeof(float))
                return new Vector4((float)value, 0f, 0f, 0f);
            if (_mPropertyInfo.PropertyType == typeof(Vector2))
                return (Vector2)value;
            if (_mPropertyInfo.PropertyType == typeof(Vector3))
                return (Vector3)value;
            return (Vector4)value;
        }

        private void SetValue(Vector4 value)
        {
            _mPropertyInfo.SetValue(_mNeuralNodeEditor, ValueToPropertyType(value), null);
        }

        private void Repaint<T>(MouseEventBase<T> evt) where T : MouseEventBase<T>, new()
        {
            evt.StopPropagation();
            MarkDirtyRepaint();
        }
    }
}
