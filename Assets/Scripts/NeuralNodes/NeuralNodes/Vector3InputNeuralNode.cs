﻿using System;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
    public class Vector3InputNeuralNode : NeuralNode
    {
        public event Action<Vector3> Vector3Output;
       
        [Vector3Input]
        public void Vector3Input(Vector3 value)
        {
            Debug.Log("Vector1InputNeuralNode Vector3Input " + value);
            if (Vector3Output != null) Vector3Output(value);
        }
    }
}