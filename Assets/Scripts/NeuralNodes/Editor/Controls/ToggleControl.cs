﻿using System;
using System.Reflection;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Extensions;

using UnityEditor;

using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Controls
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NodeToggleControlAttribute : Attribute, INodeControlAttribute
    {
        private readonly string _label;

        public NodeToggleControlAttribute(string label = null)
        {
            _label = label;
        }

        public VisualElement InstantiateControl(AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo)
        {
            return new ToggleControlView(_label, neuralNodeEditor, propertyInfo);
        }
    }

    public class ToggleControlView : VisualElement
    {
        private readonly AbstractNeuralNodeEditor _neuralNodeEditor;
        private readonly PropertyInfo _propertyInfo;
        private readonly Toggle _toggle;

        public ToggleControlView(string label, AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo)
        {
            _neuralNodeEditor = neuralNodeEditor;
            _propertyInfo = propertyInfo;
            this.LoadAndAddStyleSheet("Styles/Controls/ToggleControlView");

            if (propertyInfo.PropertyType != typeof(bool))
                throw new ArgumentException("Property must be a Toggle.", "propertyInfo");

            label = label ?? ObjectNames.NicifyVariableName(propertyInfo.Name);

            var value = (bool)_propertyInfo.GetValue(_neuralNodeEditor, null);
            var panel = new VisualElement { name = "togglePanel" };
            if (!string.IsNullOrEmpty(label))
                panel.Add(new Label(label));
            _toggle = new Toggle {value = value};
            _toggle.RegisterValueChangedCallback(OnChangeToggle);

            panel.Add(_toggle);
            Add(panel);
        }

        private void OnChangeToggle(ChangeEvent<bool> evt)
        {
            _neuralNodeEditor.Owner.NeuralGraphEditorObject.RegisterCompleteObjectUndo("Toggle Change " + _neuralNodeEditor.NodeType());
            var value = (bool)_propertyInfo.GetValue(_neuralNodeEditor, null);
            value = !value;
            _propertyInfo.SetValue(_neuralNodeEditor, value, null);
            MarkDirtyRepaint();            
        }
    }
}
