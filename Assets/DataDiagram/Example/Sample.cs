﻿using System.Collections.Generic;

using UnityEngine;

public class Sample : MonoBehaviour
{
	private readonly List<GameObject> lineList = new List<GameObject>();
	private          float            h;

	private DD_DataDiagram m_DataDiagram;

	private float m_Input;

	//private RectTransform DDrect;

	private bool m_IsContinueInput;

	private void AddALine()
	{
		if (null == m_DataDiagram)
			return;

		var color = Color.HSVToRGB((h += 0.1f) > 1 ? h - 1 : h, 0.8f, 0.8f);
		var line  = m_DataDiagram.AddLine(color.ToString(), color);
		if (null != line)
			lineList.Add(line);
	}

	// Use this for initialization
	private void Start()
	{
		var dd = GameObject.Find("DataDiagram");
		if (null == dd)
		{
			Debug.LogWarning("can not find a gameobject of DataDiagram");
			return;
		}

		m_DataDiagram = dd.GetComponent<DD_DataDiagram>();

		m_DataDiagram.PreDestroyLineEvent += (s, e) => { lineList.Remove(e.line); };

		AddALine();
	}

	// Update is called once per frame
	private void Update()
	{
	}

	private void FixedUpdate()
	{
		m_Input += Time.deltaTime;
		ContinueInput(m_Input);
	}

	private void ContinueInput(float f)
	{
		if (null == m_DataDiagram)
			return;

		if (false == m_IsContinueInput)
			return;

		var d = 0f;
		foreach (var l in lineList)
		{
			m_DataDiagram.InputPoint(l,
									 new Vector2(0.1f,
												 (Mathf.Sin(f + d) + 1f) * 2f));
			d += 1f;
		}
	}

	public void onButton()
	{
		if (null == m_DataDiagram)
			return;

		foreach (var l in lineList)
			m_DataDiagram.InputPoint(l, new Vector2(1, Random.value * 4f));
	}

	public void OnAddLine()
	{
		AddALine();
	}

	public void OnRectChange()
	{
		if (null == m_DataDiagram)
			return;

		var rect = new Rect(Random.value                 * Screen.width,
							Random.value                 * Screen.height,
							Random.value * Screen.width  / 2,
							Random.value * Screen.height / 2);

		m_DataDiagram.rect = rect;
	}

	public void OnContinueInput()
	{
		m_IsContinueInput = !m_IsContinueInput;
	}
}