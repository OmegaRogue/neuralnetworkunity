using UnityEditor;

using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(DisplayNn))]
public class NeuralNetworkEditor : Editor
{
	public override VisualElement CreateInspectorGUI()
	{
		var visualTree = Resources.Load("NeuralNetworkEditor") as VisualTreeAsset;
		var uxmlVE     = visualTree.CloneTree();
		uxmlVE.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/UI/NeuralNetworkEditor.uss"));
		return uxmlVE;
	}
}