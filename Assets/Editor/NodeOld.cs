﻿using System;

using Enums;

using NeuralNodes.Types;

using UnityEditor;

using UnityEngine;

public abstract class NodeOld
{
	public GUIStyle DefaultNodeStyle;

	public float Delta;

	public float Error;


	public ConnectionPoint InPoint;

	public EInputFunction InputFunction;
	public bool           IsDragged;
	public bool           IsSelected;

	public Action<NodeOld> OnRemoveNode;
	public ConnectionPoint OutPoint;


	public Rect     Rect;
	public GUIStyle SelectedNodeStyle;

	public GUIStyle          Style;
	public string            Title;
	public float             TotalError;
	public ETransferFunction TransferFunction;


	public ENeuronType    Type;
	public FunctionOutput Value;

	public NodeOld(
		Vector2                 position,
		float                   width,
		float                   height,
		GUIStyle                nodeStyle,
		GUIStyle                selectedStyle,
		GUIStyle                inPointStyle,
		GUIStyle                outPointStyle,
		Action<ConnectionPoint> onClickInPoint,
		Action<ConnectionPoint> onClickOutPoint,
		Action<NodeOld>         onClickRemoveNode
	)
	{
		Rect              = new Rect(position.x, position.y, width, height);
		Style             = nodeStyle;
		InPoint           = new ConnectionPoint(this, ConnectionPointType.In,  inPointStyle,  onClickInPoint);
		OutPoint          = new ConnectionPoint(this, ConnectionPointType.Out, outPointStyle, onClickOutPoint);
		DefaultNodeStyle  = nodeStyle;
		SelectedNodeStyle = selectedStyle;
		OnRemoveNode      = onClickRemoveNode;
		InputFunction     = EInputFunction.WeightedSum;
		TransferFunction  = ETransferFunction.Sigmoid;
	}

	public abstract FunctionOutput CalculateValue();

	public void Drag(Vector2 delta)
	{
		Rect.position += delta;
	}

	public virtual void Draw()
	{
		InPoint.Draw();
		OutPoint.Draw();
		GUILayout.BeginArea(Rect, Style);
		GUILayout.Label(Title, NodeEditorStyles.GetLabelStyle());
		GUILayout.EndArea();
	}

	public virtual bool ProcessEvents(Event e)
	{
		switch (e.type)
		{
			case EventType.MouseDown:
				if (e.button == 0)
				{
					if (Rect.Contains(e.mousePosition))
					{
						IsDragged   = true;
						GUI.changed = true;
						IsSelected  = true;
						Style       = SelectedNodeStyle;
					}
					else
					{
						GUI.changed = true;
						IsSelected  = false;
						Style       = DefaultNodeStyle;
					}
				}

				if (e.button == 1 && IsSelected && Rect.Contains(e.mousePosition))
				{
					ProcessContextMenu();
					e.Use();
				}

				break;

			case EventType.MouseUp:
				IsDragged = false;
				break;

			case EventType.MouseDrag:
				if (e.button == 0 && IsDragged)
				{
					Drag(e.delta);
					e.Use();
					return true;
				}

				break;
			case EventType.KeyUp:
				if (!IsSelected)
					break;
				if (e.keyCode != KeyCode.Delete)
					break;
				e.Use();
				OnClickRemoveNode();
				break;
		}


		return false;
	}

	protected virtual void ProcessContextMenu()
	{
		var genericMenu = new GenericMenu();
		genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);


		genericMenu.AddSeparator("");
		genericMenu.AddItem(new GUIContent("InputFunction/And"),
							InputFunction == EInputFunction.And,
							() => InputFunction = EInputFunction.And);
		genericMenu.AddItem(new GUIContent("InputFunction/Difference"),
							InputFunction == EInputFunction.Difference,
							() => InputFunction = EInputFunction.Difference);
		genericMenu.AddItem(new GUIContent("InputFunction/EuclideanRbf"),
							InputFunction == EInputFunction.EuclideanRbf,
							() => InputFunction = EInputFunction.EuclideanRbf);
		genericMenu.AddItem(new GUIContent("InputFunction/Max"),
							InputFunction == EInputFunction.Max,
							() => InputFunction = EInputFunction.Max);
		genericMenu.AddItem(new GUIContent("InputFunction/Min"),
							InputFunction == EInputFunction.Min,
							() => InputFunction = EInputFunction.Min);
		genericMenu.AddItem(new GUIContent("InputFunction/Or"),
							InputFunction == EInputFunction.Or,
							() => InputFunction = EInputFunction.Or);
		genericMenu.AddItem(new GUIContent("InputFunction/Product"),
							InputFunction == EInputFunction.Product,
							() => InputFunction = EInputFunction.Product);
		genericMenu.AddItem(new GUIContent("InputFunction/Sum"),
							InputFunction == EInputFunction.Sum,
							() => InputFunction = EInputFunction.Sum);
		genericMenu.AddItem(new GUIContent("InputFunction/SumSqr"),
							InputFunction == EInputFunction.SumSqr,
							() => InputFunction = EInputFunction.SumSqr);
		genericMenu.AddItem(new GUIContent("InputFunction/WeightedSum"),
							InputFunction == EInputFunction.WeightedSum,
							() => InputFunction = EInputFunction.WeightedSum);

		genericMenu.AddSeparator("");
		genericMenu.AddItem(new GUIContent("TransferFunction/Sigmoid"),
							TransferFunction == ETransferFunction.Sigmoid,
							() => TransferFunction = ETransferFunction.Sigmoid);
		genericMenu.AddItem(new GUIContent("TransferFunction/TanH"),
							TransferFunction == ETransferFunction.TanH,
							() => TransferFunction = ETransferFunction.TanH);
		genericMenu.AddItem(new GUIContent("TransferFunction/ReLu"),
							TransferFunction == ETransferFunction.ReLu,
							() => TransferFunction = ETransferFunction.ReLu);
		genericMenu.AddItem(new GUIContent("TransferFunction/Linear"),
							TransferFunction == ETransferFunction.Linear,
							() => TransferFunction = ETransferFunction.Linear);
		genericMenu.AddItem(new GUIContent("TransferFunction/ArcTan"),
							TransferFunction == ETransferFunction.ArcTan,
							() => TransferFunction = ETransferFunction.ArcTan);
		genericMenu.AddItem(new GUIContent("TransferFunction/ArSinH"),
							TransferFunction == ETransferFunction.ArSinH,
							() => TransferFunction = ETransferFunction.ArSinH);
		genericMenu.AddItem(new GUIContent("TransferFunction/SoftSign"),
							TransferFunction == ETransferFunction.SoftSign,
							() => TransferFunction = ETransferFunction.SoftSign);
		genericMenu.AddItem(new GUIContent("TransferFunction/Gaussian"),
							TransferFunction == ETransferFunction.Gaussian,
							() => TransferFunction = ETransferFunction.Gaussian);
		genericMenu.AddItem(new GUIContent("TransferFunction/Sin"),
							TransferFunction == ETransferFunction.Sin,
							() => TransferFunction = ETransferFunction.Sin);
		genericMenu.AddItem(new GUIContent("TransferFunction/Log"),
							TransferFunction == ETransferFunction.Log,
							() => TransferFunction = ETransferFunction.Log);


		genericMenu.ShowAsContext();
	}

	protected virtual void OnClickRemoveNode()
	{
		if (OnRemoveNode != null)
			OnRemoveNode(this);
	}
}