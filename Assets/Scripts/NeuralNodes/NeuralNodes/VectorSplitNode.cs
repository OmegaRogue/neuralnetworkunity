﻿using System;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
	public class VectorSplitNode : NeuralNode
	{
		private Vector4              _value;
		public event Action<Vector4> XOutput;
		public event Action<Vector4> YOutput;
		public event Action<Vector4> ZOutput;
		public event Action<Vector4> WOutput;

		public void Vector4Input(Vector4 value)
		{
			Debug.Log("FunctionOutputSplitNode SetValue " + value);
			_value = value;
			XOutput?.Invoke(_value);
			YOutput?.Invoke(_value);
			ZOutput?.Invoke(_value);
			WOutput?.Invoke(_value);
		}
	}
}