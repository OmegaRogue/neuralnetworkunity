﻿using System.Collections.Generic;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Editor.Ports;

using UnityEditor.Experimental.GraphView;

using UnityEngine;

namespace NeuralNodes.Editor
{
	public static class NodeUtils
	{
		// CollectNodesNodeFeedsInto looks at the current node and calculates
		// which child nodes it depends on for it's calculation.
		// Results are returned depth first so by processing each node in
		// order you can generate a valid code block.
		public enum IncludeSelf
		{
			Include,
			Exclude
		}

		public static NeuralSlot GetSlot(this Port port)
		{
			var slot = (port as NeuralPort)?.Slot;

			if (slot == null)
				Debug.Log("an edge is null");
			return slot;
		}

		public static NeuralNodeView GetNode(this Node node)
		{
			var neuralNode = node as NeuralNodeView;

			if (neuralNode == null)
				Debug.Log("an edge is null");
			return neuralNode;
		}


		public static void GetSlots(
			this Edge       edge,
			out  NeuralSlot leftNeuralSlot,
			out  NeuralSlot rightNeuralSlot
		)
		{
			leftNeuralSlot  = (edge.output as NeuralPort)?.Slot;
			rightNeuralSlot = (edge.input as NeuralPort)?.Slot;
			if (leftNeuralSlot == null || rightNeuralSlot == null)
				Debug.Log("an edge is null");
		}

		public static void CollectNodesNodeFeedsInto(
			List<AbstractNeuralNodeEditor> nodeList,
			AbstractNeuralNodeEditor       node,
			IncludeSelf                    includeSelf = IncludeSelf.Include
		)
		{
			if (node == null)
				return;

			if (nodeList.Contains(node))
				return;

			foreach (var slot in node.GetOutputSlots<NeuralSlot>())
			foreach (var edge in node.Owner.NeuralGraphEditorObject.NeuralGraphData.GetEdges(slot.MemberName,
																							 slot.Owner.NodeGuid))
			{
				var inputNode = node.Owner.GetNodeByGuid(edge.sourceNodeGuid);
				CollectNodesNodeFeedsInto(nodeList, inputNode.GetNode().NeuralNodeEditor);
			}

			if (includeSelf == IncludeSelf.Include)
				nodeList.Add(node);
		}
	}
}