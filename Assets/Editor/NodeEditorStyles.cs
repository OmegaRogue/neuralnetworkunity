﻿using UnityEditor;

using UnityEngine;

public static class NodeEditorStyles
{
	
	public static GUIStyle GetLabelStyle() =>
		new GUIStyle
		{
			border = new RectOffset(12, 12, 12, 12),
			alignment = TextAnchor.UpperCenter,
			padding = new RectOffset(3, 3,3,3),
			normal = new GUIStyleState {textColor = GUI.contentColor}
		};
	
	public static GUIStyle GetConnectionLabelStyle() =>
		new GUIStyle
		{
			border    = new RectOffset(12, 12, 12, 12),
			alignment = TextAnchor.MiddleCenter,
			padding   = new RectOffset(3, 3, 3, 3),
			normal    = new GUIStyleState {textColor = GUI.contentColor}
		};
	public static GUIStyle GetNodeStyle() =>
		new GUIStyle
		{
			normal =
			{
				background =
					EditorGUIUtility.Load($"builtin skins/{(EditorGUIUtility.isProSkin ? "dark" : "light" )}skin/images/node1.png") as Texture2D
			},
			border = new RectOffset(12, 12, 12, 12),
		};
	
	public static GUIStyle GetSelectedNodeStyle() =>
		new GUIStyle
		{
			normal =
			{
				background =
					EditorGUIUtility.Load($"builtin skins/{(EditorGUIUtility.isProSkin ? "dark" : "light" )}skin/images/node1 on.png") as
						Texture2D
			},
			border = new RectOffset(12, 12, 12, 12)
		};

	public static GUIStyle GetInPointStyle() =>
		new GUIStyle
		{
			normal =
			{
				background =
					EditorGUIUtility.Load($"builtin skins/{(EditorGUIUtility.isProSkin ? "dark" : "light" )}skin/images/btn left.png") as
						Texture2D
			},
			active =
			{
				background =
					EditorGUIUtility.Load($"builtin skins/{(EditorGUIUtility.isProSkin ? "dark" : "light" )}skin/images/btn left on.png") as
						Texture2D
			},
			border = new RectOffset(4, 4, 12, 12)
		};

	public static GUIStyle GetOutPointStyle() =>
		new GUIStyle
		{
			normal =
			{
				background =
					EditorGUIUtility.Load($"builtin skins/{(EditorGUIUtility.isProSkin ? "dark" : "light" )}skin/images/btn right.png") as
						Texture2D
			},
			active =
			{
				background =
					EditorGUIUtility.Load($"builtin skins/{(EditorGUIUtility.isProSkin ? "dark" : "light" )}skin/images/btn right on.png") as
						Texture2D
			},
			border = new RectOffset(4, 4, 12, 12)
		};
}