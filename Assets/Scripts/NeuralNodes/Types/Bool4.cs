﻿using System;

namespace NeuralNodes.Types
{
    [System.Serializable]
    public struct Bool4
    {
        public bool x;
        public bool y;
        public bool z;
        public bool w;

        public Bool4(bool x)
        {
            this.x = x;
            y = false;
            z = false;
            w = false;
        }
        
        public Bool4(bool x, bool y)
        {
            this.x = x;
            this.y = y;
            z = false;
            w = false;
        }

        public Bool4(bool x, bool y, bool z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            w = false;
        }

        public Bool4(bool x, bool y, bool z, bool w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        
        public bool this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;
                    case 1:
                        return y;
                    case 2:
                        return z;
                    case 3:
                        return w;
                    default:
                        throw new IndexOutOfRangeException("Invalid Vector4 index!");
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        break;
                    case 1:
                        y = value;
                        break;
                    case 2:
                        z = value;
                        break;
                    case 3:
                        w = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException("Invalid Vector4 index!");
                }
            }
        }
        
        public static implicit operator Bool4(Bool3 v)
        {
            return new Bool4(v.x, v.y, v.z, false);
        }

        public static implicit operator Bool3(Bool4 v)
        {
            return new Bool3(v.x, v.y, v.z);
        }

        public static implicit operator Bool4(Bool2 v)
        {
            return new Bool4(v.x, v.y, false, false);
        }

        public static implicit operator Bool2(Bool4 v)
        {
            return new Bool2(v.x, v.y);
        }
    }
}