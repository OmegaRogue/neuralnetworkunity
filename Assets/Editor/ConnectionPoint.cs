﻿using System;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

public enum ConnectionPointType { In, Out }

public class ConnectionPoint
{
	public Rect Rect;

	public ConnectionPointType Type;

	public NodeOld Node;

	public GUIStyle Style;

	public Action<ConnectionPoint> OnClickConnectionPoint;

	public readonly List<Connection> Connections;

	public ConnectionPoint(NodeOld node, ConnectionPointType type, GUIStyle style, Action<ConnectionPoint> onClickConnectionPoint)
	{
		Connections = new List<Connection>();
		this.Node                   = node;
		this.Type                   = type;
		this.Style                  = style;
		this.OnClickConnectionPoint = onClickConnectionPoint;
		Rect                        = new Rect(0, 0, 10f, 20f);
	}

	public void Draw()
	{
		Rect.y = Node.Rect.y + (Node.Rect.height * 0.5f) - Rect.height * 0.5f;

		switch (Type)
		{
			case ConnectionPointType.In:
				Rect.x = Node.Rect.x - Rect.width + 8f;
				break;

			case ConnectionPointType.Out:
				Rect.x = Node.Rect.x + Node.Rect.width - 8f;
				break;
		}

		GUI.Box(Rect, "", Style);
	}
	
	
	
	public virtual bool ProcessEvents(Event e)
	{
		if (!Rect.Contains(e.mousePosition))
			return false;
		if (e.type != EventType.MouseDown)
			return true;
		switch (e.button)
		{
			case 1:
				ProcessContextMenu();
				e.Use();
				return false;
			case 0:
				CreateConnection();
				e.Use();
				break;
		}

		return false;


	}

	protected virtual void ProcessContextMenu()
	{
		var genericMenu = new GenericMenu();
		genericMenu.AddItem(new GUIContent("Remove Connections"), false, ClearConnections);
		genericMenu.ShowAsContext();
	}

	private void ClearConnections()
	{
		for (var index = Connections.Count - 1; index >= 0; index--)
		{
			var connection = Connections[index];
			connection.OnClickRemoveConnection?.Invoke(connection);
		}
	}

	private void CreateConnection()
	{
		OnClickConnectionPoint?.Invoke(this);
	}
}