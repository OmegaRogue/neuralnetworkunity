﻿namespace NeuralNodes.Types
{
    [System.Serializable]
    public struct Bool2
    {
        public bool x;
        public bool y;

        public Bool2(bool x)
        {
            this.x = x;
            y = false;
        }

        public Bool2(bool x, bool y)
        {
            this.x = x;
            this.y = y;
        }
    }
}