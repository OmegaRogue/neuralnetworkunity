﻿using System;

using NeuralNodes.Editor.Nodes;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
    [Serializable]
    public class VectorNeuralSlot : NeuralSlot
    {        
        private readonly string[] _labels;
        private readonly Func<Vector4> _get;
        private readonly Action<Vector4> _set;
        
        public override SlotValueType ValueType
        {
            get { return SlotValueType.Vector3; }
        }

        public VectorNeuralSlot(int id,
            AbstractNeuralNodeEditor owner, 
            string memberName, 
            string displayName, 
            SlotDirection direction,
            string[] labels,
            Func<Vector4> get, 
            Action<Vector4> set) 
            : base(id, owner, memberName, displayName, direction)
        {
            _get = get;
            _set = set;
            _labels = labels;
        }

        public override bool IsCompatibleWithInputSlotType(SlotValueType inputType)
        {
            return inputType == SlotValueType.Vector4 || 
                   inputType == SlotValueType.Vector3 ||
                   inputType == SlotValueType.Vector2 ||
                   inputType == SlotValueType.Vector1;
        }
        
        public override VisualElement InstantiateControl()
        {
            return new MultiFloatSlotControlView(
                Owner,
                _labels,
                _get, 
                _set);
        }
    }
}
