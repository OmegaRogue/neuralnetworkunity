﻿using System;
using System.Reflection;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
    [AttributeUsage(AttributeTargets.Method)]
    public abstract class InputAttribute : Attribute
    {
        /// <summary>
        /// Gets called from OnEnable or OnValidate of NeuralGraphInstance, needs to hook up the InputAttribute
        /// so that it pipes the data from the serialized GraphInput object through the nodechain.
        /// </summary>
        /// <param name="node">The node to hooke up.</param>
        /// <param name="method">The method which the InputAttribute is.</param>
        /// <param name="graphInput">The GraphiInput holding the serialized data.</param>
        public abstract void HookUpMethodInvoke(NeuralNode node, MethodInfo method, GraphInput graphInput);

        public abstract Type InputType();
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class Vector1InputAttribute : InputAttribute
    {
        private float _priorFloatValueX;

        public override void HookUpMethodInvoke(NeuralNode node, MethodInfo method, GraphInput graphInput)
        {
            graphInput.validate = () => OnValidate(node, method, graphInput);
        }

        public override Type InputType()
        {
            return typeof(float);
        }
        
        private void OnValidate(NeuralNode node, MethodInfo method, GraphInput graphInput)
        {
            if (!Mathf.Approximately(graphInput.floatValueX, _priorFloatValueX))
            {
                method.Invoke(node, new object[] {graphInput.floatValueX});
                _priorFloatValueX = graphInput.floatValueX;
            }
        }
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    public class Vector3InputAttribute : InputAttribute
    {
        private float _priorFloatValueX;
        private float _priorFloatValueY;
        private float _priorFloatValueZ;

        public override void HookUpMethodInvoke(NeuralNode node, MethodInfo method, GraphInput graphInput)
        {
            graphInput.validate = () => OnValidate(node, method, graphInput);
        }

        public override Type InputType()
        {
            return typeof(float);
        }
        
        private void OnValidate(NeuralNode node, MethodInfo method, GraphInput graphInput)
        {
            if (!Mathf.Approximately(graphInput.floatValueX, _priorFloatValueX) ||
                !Mathf.Approximately(graphInput.floatValueY, _priorFloatValueY) ||
                !Mathf.Approximately(graphInput.floatValueZ, _priorFloatValueZ))
            {
                method.Invoke(node, new object[] {new Vector3(graphInput.floatValueX, graphInput.floatValueY, graphInput.floatValueZ)});
                _priorFloatValueX = graphInput.floatValueX;
                _priorFloatValueY = graphInput.floatValueY;
                _priorFloatValueZ = graphInput.floatValueZ;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Event)]
    public class OutputAttribute : Attribute
    {
    }
}