﻿using System;

using UnityEngine;

[Serializable]
public class NetworkTopology
{
	[Min(1)]
	public int   inputLayerSize  = 1;
	[Min(0)]
	public int   nHiddenLayers   = 1;
	public int[] hiddenLayerSize = {1};
	[Min(1)]
	public int   outputLayerSize = 1;

	public bool biases;

}