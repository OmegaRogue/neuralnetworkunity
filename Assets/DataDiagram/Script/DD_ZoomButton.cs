﻿using System;

using UnityEngine;

public class ZoomButtonClickEventArgs : EventArgs
{
}

public class DD_ZoomButton : MonoBehaviour
{
	public delegate void ZoomButtonClickHandle(object sender, ZoomButtonClickEventArgs args);

	private readonly RTParam[] RTparams = new RTParam[2];

	private DD_DataDiagram        m_DataDiagram;
	private int                   paramSN;
	public  ZoomButtonClickHandle ZoomButtonClickEvent;

	private void Awake()
	{
		m_DataDiagram = GetComponentInParent<DD_DataDiagram>();
		if (null == m_DataDiagram)
		{
			Debug.LogWarning(this + "Awake Error : null == m_DataDiagram");
		}
	}

	// Use this for initialization
	private void Start()
	{
		if (null == m_DataDiagram)
			return;

		RectTransform rt;

		RTparams[0].parent = m_DataDiagram.transform.parent;
		rt                 = m_DataDiagram.GetComponent<RectTransform>();

		RTparams[0].rect = DD_CalcRectTransformHelper.CalcLocalRect(rt.anchorMin,
																	rt.anchorMax,
																	RTparams[0]
																	   .parent.GetComponent<RectTransform>().rect.size,
																	rt.pivot,
																	rt.anchoredPosition,
																	rt.rect);


		RTparams[1].parent = GetComponentInParent<Canvas>().transform;
		RTparams[1].rect = new Rect(new Vector2(Screen.width     / 10, Screen.height     / 10),
									new Vector2(Screen.width * 8 / 10, Screen.height * 8 / 10));

		paramSN = 0;
	}

	// Update is called once per frame
	private void Update()
	{
	}

	public void OnZoomButton()
	{
		if (null == m_DataDiagram)
			return;

		paramSN = (paramSN + 1) % 2;

		m_DataDiagram.transform.SetParent(RTparams[paramSN].parent);
		m_DataDiagram.rect = RTparams[paramSN].rect;

		if (null != ZoomButtonClickEvent)
			ZoomButtonClickEvent(this, new ZoomButtonClickEventArgs());
	}

	private struct RTParam
	{
		public Transform parent;
		public Rect      rect;
	}
}