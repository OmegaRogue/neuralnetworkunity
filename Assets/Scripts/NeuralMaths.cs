﻿using System.Collections.Generic;
using System.Linq;

using NeuralNodes.Types;

using Unity.Mathematics;

public delegate FunctionOutput TransferFunction(float x);

public delegate float ErrorFunction(float predictedOutput, float targetOutput);

public delegate float InputFunction(IEnumerable<ConnectionData> connections);


public static class NeuralMaths
{
	public const float Epsilon = float.Epsilon;

	public static readonly TransferFunction[] Transfer =
	{
		Sigmoid, TanH, ReLu, Linear, ArcTan, ArSinH, SoftSign, Gaussian, Sin, Log
	};

	public static readonly ErrorFunction[] Error = {SquaredError, AbsoluteError};

	public static readonly InputFunction[] Input =
	{
		And, Difference, EuclideanRbf, Max, Min, Or, Product, Sum, SumSqr, WeightedSum
	};

	#region Transfer

	private static FunctionOutput Sigmoid(float x)
	{
		return new FunctionOutput
			   {
				   value = 1 / (1 + math.exp(-x)), derivative = 1 / (1 + math.exp(-x)) * (1 - 1 / (1 + math.exp(-x)))
			   };
	}

	private static FunctionOutput TanH(float x)
	{
		return new FunctionOutput {value = math.tanh(x), derivative = 1 - math.pow(math.tanh(x), 2)};
	}

	private static FunctionOutput ReLu(float x)
	{
		return new FunctionOutput {value = math.max(0, x), derivative = x < 0 ? 0 : 1};
	}

	private static FunctionOutput Linear(float x)
	{
		return new FunctionOutput {value = x, derivative = 1};
	}

	private static FunctionOutput ArcTan(float x)
	{
		return new FunctionOutput {value = math.atan(x), derivative = 1 / (math.pow(x, 2) + 1)};
	}

	private static FunctionOutput ArSinH(float x)
	{
		return new FunctionOutput
			   {
				   value = math.log(x + math.sqrt(math.pow(x, 2) + 1)), derivative = 1 / math.sqrt(math.pow(x, 2) + 1)
			   };
	}

	private static FunctionOutput SoftSign(float x)
	{
		return new FunctionOutput {value = x / (1 + math.abs(x)), derivative = 1 / math.pow(1 + math.abs(x), 2)};
	}

	private static FunctionOutput Gaussian(float x)
	{
		return new FunctionOutput {value = math.exp(-math.pow(x, 2)), derivative = -2 * x * math.exp(-math.pow(x, 2))};
	}

	private static FunctionOutput Sin(float x)
	{
		return new FunctionOutput {value = math.sin(x), derivative = math.cos(x)};
	}

	private static FunctionOutput Log(float x)
	{
		return new FunctionOutput {value = math.log(x), derivative = 1 / x};
	}

	private static FunctionOutput Sinc(float x)
	{
		return new FunctionOutput
			   {
				   value      = math.abs(x) < Epsilon ? 1 : math.sin(x) / x,
				   derivative = math.abs(x) < Epsilon ? 0 : math.cos(x) / x - math.sin(x) / math.pow(x, 2)
			   };
	}

	#endregion

	#region Error

	private static float SquaredError(float predictedOutput, float targetOutput)
	{
		return math.pow(predictedOutput - targetOutput, 2);
	}

	private static float AbsoluteError(float predictedOutput, float targetOutput)
	{
		return math.abs(predictedOutput - targetOutput);
	}

	#endregion

	#region Input

	private static float And(IEnumerable<ConnectionData> connections)
	{
		return connections.Aggregate(true, (current, data) => current && data.input >= 0.5f) ? 1 : 0;
	}

	private static float Difference(IEnumerable<ConnectionData> connections)
	{
		return math.sqrt(connections.Sum(data => math.pow(data.Difference, 2)));
	}

	private static float EuclideanRbf(IEnumerable<ConnectionData> connections)
	{
		return connections.Sum(data => math.pow(data.Difference, 2));
	}


	private static float Max(IEnumerable<ConnectionData> connections)
	{
		return connections.Select(data => data.WeightedInput).Max();
	}

	private static float Min(IEnumerable<ConnectionData> connections)
	{
		return connections.Select(data => data.WeightedInput).Min();
	}

	private static float Or(IEnumerable<ConnectionData> connections)
	{
		return connections.Aggregate(true, (current, data) => current || data.input >= 0.5f) ? 1 : 0;
	}

	private static float Product(IEnumerable<ConnectionData> connections)
	{
		return connections.Select(data => data.input).Aggregate((a, b) => a * b);
	}

	private static float Sum(IEnumerable<ConnectionData> connections)
	{
		return connections.Sum(data => data.input);
	}

	private static float SumSqr(IEnumerable<ConnectionData> connections)
	{
		return connections.Sum(data => math.pow(data.input, 2));
	}

	private static float WeightedSum(IEnumerable<ConnectionData> connections)
	{
		return connections.Sum(data => data.WeightedInput);
	}

	#endregion
}