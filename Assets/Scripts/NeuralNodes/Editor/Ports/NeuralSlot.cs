﻿using System.Linq;

using NeuralNodes.Editor.Nodes;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
    public abstract class NeuralSlot
    {
        private readonly string _memberName;
        private readonly string _displayName = "";
        private readonly SlotDirection _direction;
        
        
        [SerializeField]
        private int _id;
       
        
        public int Id
        {
            get { return _id; }
        }

        public AbstractNeuralNodeEditor Owner { get; }
        
        public NeuralSlot(int id, AbstractNeuralNodeEditor owner, string memberName, string displayName, SlotDirection direction)
        {
            _id = id;
            Owner = owner;
            _memberName = memberName;
            _displayName = displayName;
            _direction = direction;
        }

        public string DisplayName => _displayName + " " + ValueType;

        public string MemberName => _memberName;

        public bool IsInputSlot => _direction == SlotDirection.Input;

        public bool IsOutputSlot => _direction == SlotDirection.Output;

        public SlotDirection Direction => _direction;

        public abstract SlotValueType ValueType { get; }

        public abstract bool IsCompatibleWithInputSlotType(SlotValueType inputType);

        public bool IsCompatibleWith(NeuralSlot otherNeuralSlot)
        {
            return otherNeuralSlot != null
                   && otherNeuralSlot.Owner != Owner
                   && otherNeuralSlot.IsInputSlot != IsInputSlot
                   && ((IsInputSlot
                       ? otherNeuralSlot.IsCompatibleWithInputSlotType(ValueType)
                       : IsCompatibleWithInputSlotType(otherNeuralSlot.ValueType)));
        }


        public bool IsConnected
        {
            get
            {
                // node and graph respectivly
                if (Owner == null || Owner.Owner == null)
                    return false;

                var graph = Owner.Owner.NeuralGraphEditorObject.NeuralGraphData;
                var edges = graph.GetEdges(Owner.NodeGuid, MemberName);
                return edges.Any();
            }
        }
        
        public virtual VisualElement InstantiateControl()
        {
            return null;
        }
    }
}