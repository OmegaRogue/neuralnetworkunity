﻿using System;

using NeuralNodes.Extensions;

using UnityEditor.Experimental.GraphView;

using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
    public sealed class NeuralPort : Port
    {
        private NeuralPort(Orientation portOrientation, Direction portDirection, Capacity portCapacity, Type type)
            : base(portOrientation, portDirection, portCapacity, type)
        {
            this.LoadAndAddStyleSheet("Styles/NeuralPort");
        }

        private NeuralSlot _slot;

        public static Port Create(NeuralSlot neuralSlot, IEdgeConnectorListener connectorListener)
        {
            var port = new NeuralPort(Orientation.Horizontal, 
                neuralSlot.IsInputSlot ? Direction.Input : Direction.Output,
                neuralSlot.IsInputSlot ? Capacity.Single : Capacity.Multi,
                null)
            {
                m_EdgeConnector = new EdgeConnector<Edge>(connectorListener),
            };
            port.AddManipulator(port.m_EdgeConnector);
            port.Slot = neuralSlot;
            port.portName = neuralSlot.DisplayName;
            port.visualClass = neuralSlot.ValueType.ToClassName();
            return port;
        }

        public NeuralSlot Slot
        {
            get => _slot;
            set
            {
                if (ReferenceEquals(value, _slot))
                    return;
                if (value == null)
                    throw new NullReferenceException();
                if (_slot != null && value.IsInputSlot != _slot.IsInputSlot)
                    throw new ArgumentException("Cannot change direction of already created port");
                _slot = value;
                portName = Slot.DisplayName;
                visualClass = Slot.ValueType.ToString();
            }
        }
    }
}
