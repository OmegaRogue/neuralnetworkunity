﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Data;

using Enums;

using TMPro;

using Unity.Mathematics;

using UnityEngine;
using UnityEngine.UI;

public class DisplayNn : MonoBehaviour
{
	public static float MaximumError;

	[Min(0)] public int currentData;


	[Header("Data")] public DataSet    data;
	public                  InputField dataInput;

	public DD_DataDiagram diagram;

	public bool displayAllErrors;

	public float error;

	public EErrorFunction  errorFunction;
	public TextMeshProUGUI errorFunctionText;

	public EInputFunction inputFunction;


	public TextMeshProUGUI inputFunctionText;


	public int iterations;


	[Header("Prefabs")] public GameObject layerPanelPrefab;

	public Action learnEvent;

	[Header("Network Settings")] [Min(0)] public float learningRate = .5f;

	public float maxError;


	public Process    neuralProcess;
	public int        neuronCount;
	public GameObject neuronPrefab;

	public NeuralData saveData;
	public Gradient   synapseColorGradientNegative;


	public Gradient synapseColorGradientPositive;


	[Header("Network Information")] public int synapseCount;

	public GameObject synapsePrefab;

	public NetworkTopology topology;

	public bool training;

	public ETransferFunction transferFunction;
	public TextMeshProUGUI   transferFunctionText;


	public float x;
	public float y;

	public float LearningRate
	{
		get { return learningRate; }
	}


	public DataSetEntry CurrentData
	{
		get { return data.dataSets[currentData % data.nDataSet]; }
	}

	public void CalculateLearnStep()
	{
		CalculateStep();
	}

	// Start is called before the first frame update
	private void Start()
	{
		neuralProcess                 =  new Process(data.nDataSet);
		neuralProcess.Callback        += LearnSetCallback;
		neuralProcess.SpecialCallback += LearnCallback;
		neuralProcess.Work            += CalculateLearnStep;

		_errorLine = diagram.AddLine("Error");

		_errorText       = GameObject.FindWithTag("Error").GetComponent<TextMeshProUGUI>();
		_canvas          = GameObject.FindWithTag("Canvas").GetComponent<Canvas>();
		_layerPanelPanel = GameObject.FindWithTag("Panels").transform;
		_trainIterations = GameObject.FindWithTag("TrainNInput").GetComponent<InputField>();
		_iterationsText  = GameObject.FindWithTag("iterations").GetComponent<TextMeshProUGUI>();


		_inLayer      = new List<Neuron>();
		_outLayer     = new List<Neuron>();
		_hiddenLayers = new List<List<Neuron>>();
		_layerPanels  = new List<Transform>();
		_neurons      = new List<Neuron>();

		_synapses            = new List<Synapse>();
		_unconnectedSynapses = new Stack<Synapse>();


		for (var i = 0; i < 2 + topology.nHiddenLayers; i++)
		{
			var panel = Instantiate(layerPanelPrefab, _layerPanelPanel);
			panel.name = i == 0 ? "Input Layer" : i == topology.nHiddenLayers ? $"Hidden Layer {i}" : "OutputLayer";
			_layerPanels.Add(panel.transform);
		}

		for (var i = 0; i < topology.inputLayerSize; i++)
		{
			var neuronObject = Instantiate(neuronPrefab, _layerPanels[0]);
			var neuron       = neuronObject.GetComponent<Neuron>();
			_inLayer.Add(neuron);
			_neurons.Add(neuron);
			neuron.Initialize(ENeuronType.Input, $"X{i + 1}");
		}

		if (topology.biases)
		{
			var neuronObject = Instantiate(neuronPrefab, _layerPanels[0]);
			var neuron       = neuronObject.GetComponent<Neuron>();
			_inLayer.Add(neuron);
			_neurons.Add(neuron);
			neuron.Initialize(ENeuronType.Bias, "InputB");
		}

		for (var i = 0; i < topology.outputLayerSize; i++)
		{
			var neuronObject = Instantiate(neuronPrefab, _layerPanels.Last());
			var neuron       = neuronObject.GetComponent<Neuron>();
			_outLayer.Add(neuron);
			_neurons.Add(neuron);
			neuron.Initialize(ENeuronType.Output, $"Y{i + 1}");
		}

		for (var i = 0; i < topology.nHiddenLayers; i++)
		{
			_hiddenLayers.Add(new List<Neuron>());
			for (var j = 0; j < topology.hiddenLayerSize[i]; j++)
			{
				var neuronObject = Instantiate(neuronPrefab, _layerPanels[i + 1]);
				var neuron       = neuronObject.GetComponent<Neuron>();
				_hiddenLayers[i].Add(neuron);
				_neurons.Add(neuron);
				neuron.Initialize(ENeuronType.Hidden, $"H{i + 1}_{j + 1}");
			}

			if (topology.biases)
			{
				var neuronObject = Instantiate(neuronPrefab, _layerPanels[i + 1]);
				var neuron       = neuronObject.GetComponent<Neuron>();
				_hiddenLayers[i].Add(neuron);
				_neurons.Add(neuron);
				neuron.Initialize(ENeuronType.Bias, $"Hidden B {i + 1}");
			}
		}

		GetNeuronCount();
		GetSynapseCount();

		for (var i = 0; i < synapseCount; i++)
		{
			var synapseObject = Instantiate(synapsePrefab, _canvas.transform);
			var synapse       = synapseObject.GetComponent<Synapse>();
			_synapses.Add(synapse);
			_unconnectedSynapses.Push(synapse);
		}

		if (topology.nHiddenLayers == 0)
		{
			foreach (var neuron in _inLayer)
			foreach (var neuron1 in _outLayer.Where(neuron1 => neuron1.type != ENeuronType.Bias))
				_unconnectedSynapses.Pop().Initialize(neuron, neuron1);
		}
		else
		{
			foreach (var neuron in _inLayer)
			foreach (var neuron1 in _hiddenLayers[0].Where(neuron1 => neuron1.type != ENeuronType.Bias))
				_unconnectedSynapses.Pop().Initialize(neuron, neuron1);

			for (int i = 0, j = 1; j < topology.nHiddenLayers; i++, j++)
				foreach (var neuron in _hiddenLayers[i])
				foreach (var neuron1 in _hiddenLayers[j].Where(neuron1 => neuron1.type != ENeuronType.Bias))
					_unconnectedSynapses.Pop().Initialize(neuron, neuron1);

			foreach (var neuron in _hiddenLayers.Last())
			foreach (var neuron1 in _outLayer)
				_unconnectedSynapses.Pop().Initialize(neuron, neuron1);
		}
	}

	// Update is called once per frame
	private void Update()
	{
		if (!_learnOnce)
			neuralProcess.DoWork();

		_errorText.text      = $"{error}";
		_iterationsText.text = $"{iterations}";


		_maximumError = MaximumError;
		MaximumError  = math.max(MaximumError, error);
		diagram.InputPoint(_errorLine, new Vector2(x, error * y));
		errorFunctionText.text    = errorFunction.ToString();
		inputFunctionText.text    = inputFunction.ToString();
		transferFunctionText.text = transferFunction.ToString();
	}


	public void ChangeData()
	{
		currentData = Convert.ToInt32(dataInput.text);
		AdjustInputs();
		CalculateStep(false);
	}


	private void Awake()
	{
		Neuron.Nn  =  this;
		Synapse.Nn =  this;
		learnEvent += LearnCallback;
	}

	private void GetNeuronCount()
	{
		neuronCount = topology.inputLayerSize + topology.outputLayerSize + topology.hiddenLayerSize.Sum();
	}


	private void GetSynapseCount()
	{
		if (topology.nHiddenLayers == 0)
		{
			synapseCount = (topology.inputLayerSize + (topology.biases ? 1 : 0)) * topology.outputLayerSize;
			return;
		}

		synapseCount = (topology.inputLayerSize + (topology.biases ? 1 : 0)) * topology.hiddenLayerSize[0];
		for (int i = 0, j = 1; j < topology.nHiddenLayers; i++, j++)
			synapseCount += (topology.hiddenLayerSize[i] + (topology.biases ? 1 : 0)) * topology.hiddenLayerSize[j];
		synapseCount +=
			(topology.hiddenLayerSize[topology.hiddenLayerSize.GetUpperBound(0)] + (topology.biases ? 1 : 0)) *
			topology.outputLayerSize;
	}


	private float CalculateError()
	{
		return _outLayer.Sum(a => a.totalError);
	}


	private void LearnSetCallback()
	{
		currentData = (currentData + 1) % data.nDataSet;
		AdjustInputs();
	}

	private void AdjustInputs()
	{
		for (var i = 0; i < topology.inputLayerSize; i++)
			_inLayer[i].input = CurrentData.input[i];
	}

	private void LearnCallback()
	{
		if (!displayAllErrors)
			error = _error / data.nDataSet;
		_error = 0;
		iterations++;
		diagram.InputPoint(_errorLine, new Vector2(x, error * y));
	}

	private void CalculateStep(bool learn = true)
	{
		_inLayer.ForEach(neuron => neuron.Calculate());
		_hiddenLayers.ForEach(list => list.ForEach(neuron => neuron.Calculate()));
		_outLayer.ForEach(neuron => neuron.Calculate());
		if (learn)
			LearnStep();
	}


	private void LearnStep()
	{
		_outLayer.ForEach(neuron => neuron.Learn());
		_hiddenLayers.Reverse();
		_hiddenLayers.ForEach(list => list.ForEach(neuron => neuron.Learn()));
		_hiddenLayers.Reverse();
		_inLayer.ForEach(neuron => neuron.Learn());

		_synapses.ForEach(synapse => synapse.Learn());
		if (!displayAllErrors)
			_error += CalculateError();
		else
			error = CalculateError();
	}


	public void StartLearnOnce()
	{
		_learnOnce = true;
		StartCoroutine(LearnOnce());
	}

	public IEnumerator LearnOnce()
	{
		for (var i = 0; i < data.nDataSet; i++)
		{
			CalculateStep();
			LearnSetCallback();
			yield return new WaitForSeconds(1 / 4f);
		}

		CalculateStep();
		LearnSetCallback();
		LearnCallback();
		_learnOnce = false;
		yield return new WaitForSeconds(1 / 4f);
	}

	public void StartLearnMulti()
	{
		var n = Convert.ToInt32(_trainIterations.text);
		neuralProcess.Reset(data.nDataSet, true, n);
		neuralProcess.Start();
	}


	public void LearnUntilDone()
	{
		neuralProcess.Reset(data.nDataSet, false, 0, true);
		neuralProcess.CheckForDoneEvent += () => error < maxError;
		neuralProcess.CompleteCallback  += () => training = !training;
		neuralProcess.Start();
	}


	public void Save()
	{
		saveData.inWeights = _inLayer
							.Select(neuron => neuron.outputSynapses.Select(synapse => synapse.weight).ToArray())
							.ToArray();
		saveData.hiddenWeights = _hiddenLayers
								.Select(list => list
											   .Select(neuron => neuron
																.outputSynapses.Select(synapse => synapse.weight)
																.ToArray()).ToArray()).ToArray();
	}


	public void Randomize()
	{
		_synapses.ForEach(synapse => synapse.RandomizeWeights());
	}

	#region private fields

	private List<Transform>    _layerPanels;
	private List<Neuron>       _inLayer;
	private List<List<Neuron>> _hiddenLayers;
	private List<Neuron>       _outLayer;
	private List<Neuron>       _neurons;

	private List<Synapse>  _synapses;
	private Stack<Synapse> _unconnectedSynapses;

	private TextMeshProUGUI _errorText;
	private TextMeshProUGUI _iterationsText;
	private Canvas          _canvas;
	private Transform       _layerPanelPanel;

	private bool _learning;

	private InputField _trainIterations;

	private bool  _learnOnce;
	private float _error;

	private float _maximumError;

	private GameObject _errorLine;

	#endregion
}