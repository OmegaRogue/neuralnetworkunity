﻿using System.Collections.Generic;

using NeuralNodes.Editor.Ports;
using NeuralNodes.Extensions;

using UnityEditor.Experimental.GraphView;

using UnityEngine;
using UnityEngine.VFX.Utility;

namespace NeuralNodes.Editor
{
	/// <summary>
	/// Implementation of GraphView
	/// </summary>
	public class NeuralGraphView : GraphView
	{		
		public NeuralGraphEditorObject NeuralGraphEditorObject { get; private set; }
		public Blackboard Blackboard = new Blackboard();
		public List<ExposedProperty> ExposedProperties { get; private set; } = new List<ExposedProperty>();
		
		public NeuralGraphView()
		{
			this.LoadAndAddStyleSheet("Styles/NeuralGraphView");
			Debug.Log("NeuralGraphView Constructor");
		}
		
		public NeuralGraphView(NeuralGraphEditorObject neuralGraphEditorObject) : this()
		{
			NeuralGraphEditorObject = neuralGraphEditorObject;
		}
		
		public void ClearBlackBoardAndExposedProperties()
		{
			ExposedProperties.Clear();
			Blackboard.Clear();
		}
		
		
		
		public override List<Port> GetCompatiblePorts(Port startAnchor, NodeAdapter nodeAdapter)
		{
			var compatibleAnchors = new List<Port>();
			var startSlot = (startAnchor as NeuralPort).Slot;
			if (startSlot == null)
				return compatibleAnchors;

			foreach (var candidateAnchor in ports.ToList())
			{
				var candidateSlot = (candidateAnchor as NeuralPort).Slot;
				if (!startSlot.IsCompatibleWith(candidateSlot))
					continue;

				compatibleAnchors.Add(candidateAnchor);
			}
			return compatibleAnchors;
		}
	}
}