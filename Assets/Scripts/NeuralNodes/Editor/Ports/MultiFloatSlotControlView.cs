﻿using System;
using System.Globalization;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Extensions;

using UnityEditor;
using UnityEditor.UIElements;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
    public class MultiFloatSlotControlView : VisualElement
    {
        private readonly AbstractNeuralNodeEditor _nodeEditor;
        private readonly Func<Vector4> _get;
        private readonly Action<Vector4> _set;
        private int _undoGroup = -1;

        public MultiFloatSlotControlView(AbstractNeuralNodeEditor nodeEditor, string[] labels, Func<Vector4> get, Action<Vector4> set)
        {
           this.LoadAndAddStyleSheet("Styles/Controls/MultiFloatSlotControlView");
            _nodeEditor = nodeEditor;
            _get = get;
            _set = set;
            var initialValue = get();
            for (var i = 0; i < labels.Length; i++)
                AddField(initialValue, i, labels[i]);
        }

        private void AddField(Vector4 initialValue, int index, string subLabel)
        {
            var dummy = new VisualElement { name = "dummy" };
            var label = new Label(subLabel);
            dummy.Add(label);
            Add(dummy);
            var field = new FloatField { userData = index, value = initialValue[index] };
            var dragger = new FieldMouseDragger<float>(field);
            dragger.SetDragZone(label);
            field.RegisterValueChangedCallback(evt =>
                {
                    var value = _get();
                    value[index] = (float)evt.newValue;
                    _set(value);
                    _nodeEditor.SetDirty();
                    _undoGroup = -1;
                });
            field.RegisterCallback<InputEvent>(evt =>
                {
                    if (_undoGroup == -1)
                    {
                        _undoGroup = Undo.GetCurrentGroup();
                        _nodeEditor.Owner.NeuralGraphEditorObject.RegisterCompleteObjectUndo("Change " + _nodeEditor.NodeType());
                    }
                    if (!float.TryParse(evt.newData, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out var newValue))
                        newValue = 0f;
                    var value = _get();
                    if (Math.Abs(value[index] - newValue) > 1e-9)
                    {
                        value[index] = newValue;
                        _set(value);
                        _nodeEditor.SetDirty();
                    }
                });
            field.RegisterCallback<KeyDownEvent>(evt =>
                {
                    if (evt.keyCode == KeyCode.Escape && _undoGroup > -1)
                    {
                        Undo.RevertAllDownToGroup(_undoGroup);
                        _undoGroup = -1;
                        evt.StopPropagation();
                    }
                    MarkDirtyRepaint();
                });
            Add(field);
        }
    }
}
