﻿using System.Linq;

using NeuralNodes.Editor.Ports;
using NeuralNodes.NeuralNodes;

namespace NeuralNodes.Editor.Nodes
{
    [Title("Input", "Vector1")]
    [NodeEditorType(typeof(Vector1InputNeuralNode))]
    public class Vector1InputNeuralNodeEditor : AbstractNeuralNodeEditor, IInputNode
    {        
        private static readonly string[] Labels = {"X"};
        
        public override void ConstructNode()
        {
            var index = GetInputSlots<NeuralSlot>().Count() + 1;
            AddSlot(new VectorNeuralSlot(index,
                                         this,
                                         nameof(Vector1InputNeuralNode.Vector1Output),
                                         "V1Output",
                                         SlotDirection.Output,
                                         Labels,
                                         null,
                                         null));
        }
    }
}

