﻿using Unity.Mathematics;

using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;

public class Synapse : MonoBehaviour
{
	public static DisplayNn Nn;

	public float         angle;
	public RectTransform graphicTransform;

	public bool   initialized;
	public Neuron input;
	public Neuron output;

	public Image synapseGraphic;
	public float weight;

	public Text weightLabel;

	public float WeightDelta
	{
		get { return -Nn.LearningRate * output.delta * input.value; }
	}

	public ConnectionData Data
	{
		get { return new ConnectionData {input = input.value, weight = weight}; }
	}

	public float Value
	{
		get { return weight * input.value; }
	}


	private void Start()
	{
		RandomizeWeights();
	}

	public void Initialize(Neuron left, Neuron right)
	{
		left.outputSynapses.Add(this);
		right.inputSynapses.Add(this);
		input  = left;
		output = right;

		name = $"Synapse {left.gameObject.name} to {right.gameObject.name}";


		initialized = true;
	}

	private void Update()
	{
		if (!initialized)
			return;

		var position = input.transform.position;
		angle              = Vector2.SignedAngle(input.Right - position, output.Left - position);
		transform.position = Vector3.Lerp(input.Right, output.Left, .5f);
		transform.rotation = Quaternion.Euler(0, 0, angle);
		graphicTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
												   Vector2.Distance(input.Right, output.Left));


		weightLabel.text = $"{weight}";


		synapseGraphic.color = weight < 0
								   ? Nn.synapseColorGradientNegative.Evaluate(math.abs(weight))
								   : Nn.synapseColorGradientPositive.Evaluate(math.abs(weight));
	}

	public void Learn()
	{
		weight += WeightDelta;
	}

	public void RandomizeWeights()
	{
		weight = Random.Range(-0.5f, 0.5f);
	}
}