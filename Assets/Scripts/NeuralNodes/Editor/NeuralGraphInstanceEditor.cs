﻿using System;

using UnityEditor;

using UnityEngine;

namespace NeuralNodes.Editor
{
    [CustomEditor(typeof(NeuralGraphInstance))]
    public class NeuralGraphInstanceEditor : UnityEditor.Editor
    {
        private SerializedProperty _logicGraphObjectProperty;
        private SerializedProperty _inputsProperty;
        private SerializedProperty _outputsProperty;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
        
            var neuralGraphInstance = (NeuralGraphInstance) target;

            _logicGraphObjectProperty = serializedObject.FindProperty("_logicGraphObject");
            _inputsProperty           = serializedObject.FindProperty("_inputs");
            _outputsProperty          = serializedObject.FindProperty("_outputs");

            EditorGUILayout.PropertyField(_logicGraphObjectProperty);
            if (GUILayout.Button("Refresh"))
            {
                neuralGraphInstance.HookUpGraph();
            }
        
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Inputs");

            for (var i = 0; i < _inputsProperty.arraySize; ++i)
            {
                var displayName = _inputsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("DisplayName");       
            
                EditorGUI.indentLevel = 2;
                EditorGUILayout.LabelField(displayName.stringValue);

                if (neuralGraphInstance.Inputs[i].inputType == typeof(float))
                {
                    var floatValue =
                        _inputsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("FloatValueX");
                    EditorGUILayout.PropertyField(floatValue);
                }
                else
                {
                    var componentValue = _inputsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("ComponentValue");    
                    EditorGUILayout.ObjectField(componentValue, neuralGraphInstance.Inputs[i].inputType);
                }
                EditorGUILayout.Space();

            }
        
            EditorGUI.indentLevel = 0;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Outputs");
        
            for (var i = 0; i < _outputsProperty.arraySize; ++i)
            {
                var displayName = _outputsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("DisplayName");
                EditorGUI.indentLevel = 2;
                EditorGUILayout.LabelField(displayName.stringValue);

                if (neuralGraphInstance.Outputs[i].outputType == typeof(Single))
                {
                    var eventProperty =
                        _outputsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("_updatedFloat");
                    EditorGUILayout.PropertyField(eventProperty);
                }
                else
                {
                    var eventProperty = _outputsProperty.GetArrayElementAtIndex(i).FindPropertyRelative("_updatedObject");
                    EditorGUILayout.PropertyField(eventProperty);
                }
            }
        
            serializedObject.ApplyModifiedProperties();
        }
    }
}