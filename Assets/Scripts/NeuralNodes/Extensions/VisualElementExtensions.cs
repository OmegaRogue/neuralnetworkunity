﻿using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Extensions
{
    public static class VisualElementExtensions
    {
        public static void LoadAndAddStyleSheet(this VisualElement visualElement, string sheetPath)
        {
            var styleSheet = Resources.Load(sheetPath, typeof(StyleSheet)) as StyleSheet;
            if ((UnityEngine.Object) styleSheet == (UnityEngine.Object) null)
                Debug.LogWarning((object) string.Format("Style sheet not found for path \"{0}\"", (object) sheetPath));
            else
                visualElement.styleSheets.Add(styleSheet);
        }
    }
}