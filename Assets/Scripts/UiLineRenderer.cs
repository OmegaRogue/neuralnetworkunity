﻿using UnityEngine;
using UnityEngine.UI;

public class UiLineRenderer : MaskableGraphic
{
	public float     lineThickness = 2;
	public Vector2   margin;
	public Vector2[] points;
	public bool      useMargins;

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		if (points == null || points.Length < 2)
			points = new[] {new Vector2(0, 0), new Vector2(1, 1)};

		var rectTransform1 = rectTransform;
		var rect           = rectTransform1.rect;
		var sizeX          = rect.width;
		var sizeY          = rect.height;
		var offsetX        = -rectTransform1.pivot.x * rectTransform1.rect.width;
		var offsetY        = -rectTransform1.pivot.y * rectTransform1.rect.height;

		if (useMargins)
		{
			sizeX   -= margin.x;
			sizeY   -= margin.y;
			offsetX += margin.x / 2f;
			offsetY += margin.y / 2f;
		}

		vh.Clear();

		var prevV1 = Vector2.zero;
		var prevV2 = Vector2.zero;

		for (var i = 1; i < points.Length; i++)
		{
			var prev = points[i - 1];
			var cur  = points[i];
			prev = new Vector2(prev.x * sizeX + offsetX, prev.y * sizeY + offsetY);
			cur  = new Vector2(cur.x  * sizeX + offsetX, cur.y  * sizeY + offsetY);

			var angle = Mathf.Atan2(cur.y - prev.y, cur.x - prev.x) * 180f / Mathf.PI;

			var v1 = prev + new Vector2(0, -lineThickness / 2);
			var v2 = prev + new Vector2(0, +lineThickness / 2);
			var v3 = cur  + new Vector2(0, +lineThickness / 2);
			var v4 = cur  + new Vector2(0, -lineThickness / 2);

			v1 = RotatePointAroundPivot(v1, prev, new Vector3(0, 0, angle));
			v2 = RotatePointAroundPivot(v2, prev, new Vector3(0, 0, angle));
			v3 = RotatePointAroundPivot(v3, cur,  new Vector3(0, 0, angle));
			v4 = RotatePointAroundPivot(v4, cur,  new Vector3(0, 0, angle));

			if (i > 1)
				SetVbo(vh, new[] {prevV1, prevV2, v1, v2});

			SetVbo(vh, new[] {v1, v2, v3, v4});

			prevV1 = v3;
			prevV2 = v4;
		}
	}

	private void SetVbo(VertexHelper vh, Vector2[] vertices)
	{
		foreach (var t in vertices)
		{
			var vert = UIVertex.simpleVert;
			vert.color    = color;
			vert.position = t;
			vh.AddVert(vert);
		}
	}

	private static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
	{
		var dir = point - pivot;                // get point direction relative to pivot
		dir   = Quaternion.Euler(angles) * dir; // rotate it
		point = dir + pivot;                    // calculate rotated point
		return point;                           // return it
	}
}