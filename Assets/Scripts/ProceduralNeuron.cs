﻿using Unity.Mathematics;

using UnityEngine;

using Random = System.Random;

public class ProceduralNeuron : MonoBehaviour
{
	private int _nPrevious;

	[Range(float.Epsilon, 1)] public float max;


	[Range(float.Epsilon, 1)] public float min;

	[Min(1)] public int nPoints;

	public float[,] pixels;

	public float2[] points;

	[Min(1)] public int resolution = 32;

	public float scale = 1;

	public float sphereSize = 1;


	// Start is called before the first frame update
	private void Start()
	{
		var rand = new Random();
		var rng  = new Unity.Mathematics.Random((uint) rand.Next());
		points = new float2[nPoints];
		for (var i = 0; i < nPoints; i++)
			points[i] = rng.NextFloat2(float2.zero, new float2(resolution, resolution));
		pixels = new float[resolution, resolution];
		for (var i = 0; i < resolution; i++)
		for (var j = 0; j < resolution; j++)
		{
			var n = 0;
			for (var k = 0; k < nPoints; k++)
				if (math.distance(math.float2(i, j), points[k]) < math.distance(math.float2(i, j), points[n]))
					n = k;
			pixels[i, j] = math.distance(math.float2(i, j), points[n]);
		}
	}


	// Update is called once per frame
	private void Update()
	{
		pixels = new float[resolution, resolution];
		for (var i = 0; i < resolution; i++)
		for (var j = 0; j < resolution; j++)
		{
			var n = 0;
			for (var k = 0; k < nPoints; k++)
				if (math.distance(math.float2(i, j), points[k]) < math.distance(math.float2(i, j), points[n]))
					n = k;
			pixels[i, j] = math.distance(math.float2(i, j), points[n]) / resolution;
		}
	}


	private void OnDrawGizmos()
	{
		if (pixels == null)
			return;
		for (var i = 0; i < 1; i += 1 / resolution)
		for (var j = 0; j < 1; j += 1 / resolution)
		{
			var pixel = pixels[i * resolution, j * resolution];
			if (pixel > max || pixel < min)
				continue;
			Gizmos.color = new Color(pixel, 0, 0);
			Gizmos.DrawSphere(new Vector3(i, 0, j), sphereSize);
		}


		for (var i = 0; i < nPoints; i++)
		{
			Gizmos.color = Color.white;
			Gizmos.DrawSphere(new Vector3(points[i].x / resolution, 0, points[i].y / resolution), sphereSize);
		}
	}


	private void OnValidate()
	{
		if (_nPrevious == nPoints)
			return;
		var rand = new Random();
		var rng  = new Unity.Mathematics.Random((uint) rand.Next());
		points = new float2[nPoints];
		for (var i = 0; i < nPoints; i++)
			points[i] = rng.NextFloat2(float2.zero, new float2(resolution, resolution));
		_nPrevious = nPoints;
	}
}