﻿namespace Enums
{
	public enum ENeuronType
	{
		Input,
		Hidden,
		Output,
		Bias
	}
}