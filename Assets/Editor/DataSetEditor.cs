﻿using System;
using System.Collections.Generic;

using Data;

using UnityEditor;

using UnityEngine;

[CustomEditor(typeof(DataSet))]
public class DataSetEditor : Editor
{

	public List<bool> foldouts = new List<bool>();

	private const float DataFieldSize = 30f;
	
	public override void OnInspectorGUI()
	{
		
		var selected = (DataSet) target;
		selected.nDataSet = Mathf.Max(1, EditorGUILayout.IntField("Number of Data Sets", selected.nDataSet));
		selected.inputs = Mathf.Max(1, EditorGUILayout.IntField("Input Count", selected.inputs));
		selected.outputs = Mathf.Max(1, EditorGUILayout.IntField("Output Count", selected.outputs));
		selected.dataSets = selected.dataSets.Resize(selected.nDataSet, selected.inputs, selected.outputs);
		
		for (var j = 0; j < selected.dataSets.Length; j++)
		{
			if (foldouts.Count < j + 1)
				foldouts.Add(false);
			foldouts[j] = EditorGUILayout.BeginFoldoutHeaderGroup(foldouts[j], $"Data Set {j}");
			if (foldouts[j])
			{
				var splitInput     = EditorGUIUtility.currentViewWidth / selected.inputs  < 30;
				var splitOutput    = EditorGUIUtility.currentViewWidth / selected.outputs < 30;
				var columnCount    = (int) EditorGUIUtility.currentViewWidth / 30;
				var inputRowCount  = (float) selected.inputs                          / columnCount;
				var outputRowCount = (float) selected.outputs / columnCount;
				var inputBuffer    = (float[]) selected.dataSets[j].input.Clone();
				var outputBuffer   = (float[]) selected.dataSets[j].output.Clone();
				selected.dataSets[j].input  = inputBuffer.Resize(selected.inputs);
				selected.dataSets[j].output = outputBuffer.Resize(selected.outputs);
				

				EditorGUILayout.LabelField("Inputs");
				Rect rect;
				Rect posRect;
				if (splitInput)
				{
					EditorGUILayout.BeginVertical();
					for (int k = 0, l = 0, inputsLeft = selected.inputs;
						 k < Mathf.Ceil(inputRowCount);
						 k++, inputsLeft -= columnCount)
					{
						rect = EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField("");
						posRect = new Rect(rect.x, rect.y, 30, rect.height);
						for (var i = 0; i < Mathf.Min(inputsLeft, columnCount); i++, l++, posRect.x += posRect.width)
							selected.dataSets[j].input[l] =
								EditorGUI.FloatField(posRect, selected.dataSets[j].input[l]);

						EditorGUILayout.EndHorizontal();
					}

					EditorGUILayout.EndVertical();
				}
				else
				{
					rect = EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("");
					posRect = new Rect(rect.x, rect.y, Mathf.Min(60, rect.width / selected.inputs), rect.height);
					for (var i = 0; i < selected.inputs; i++, posRect.x += posRect.width)
						selected.dataSets[j].input[i] =
							EditorGUI.FloatField(posRect, selected.dataSets[j].input[i]);

					EditorGUILayout.EndHorizontal();
				}

				EditorGUILayout.LabelField("Outputs");
				if (splitOutput)
				{
					EditorGUILayout.BeginVertical();
					for (int k = 0, l = 0, outputsLeft = selected.outputs; k < Mathf.Ceil(outputRowCount); k++, outputsLeft -= columnCount)
					{
						rect = EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField("");
						posRect = new Rect(rect.x, rect.y, 30, rect.height);
						for (var i = 0; i < Mathf.Min(outputsLeft, columnCount); i++, l++, posRect.x += posRect.width)
							selected.dataSets[j].output[l] =
								EditorGUI.FloatField(posRect, selected.dataSets[j].output[l]);

						EditorGUILayout.EndHorizontal();
					}
					EditorGUILayout.EndVertical();
				}
				else
				{
					

					rect = EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("");
					posRect = new Rect(rect.x, rect.y, Mathf.Min(60, rect.width / selected.outputs), rect.height);
					for (var i = 0; i < selected.outputs; i++, posRect.x += posRect.width)
						selected.dataSets[j].output[i] = EditorGUI.FloatField(posRect, selected.dataSets[j].output[i]);
					EditorGUILayout.EndHorizontal();
				}
			}
			
			EditorGUILayout.EndFoldoutHeaderGroup();
		}
	}
}

public static class Extensions
{
	
	public static DataSetEntry[] Resize(this DataSetEntry[] source, int size, int nIn, int nOut)
	{
		DataSetEntry[] buffer;
		try
		{
			buffer = new DataSetEntry[source.Length];
			source.CopyTo(buffer, 0);
			var buffer2   = new DataSetEntry[size];
			var copyUntil = Mathf.Min(size, source.Length);
			for (var i = 0; i < copyUntil; i++)
				buffer2[i] = buffer[i];
			if (source.Length >= size)
				return buffer2;
			for (var i = copyUntil; i < Mathf.Max(size, source.Length); i++)
				buffer2[i] = new DataSetEntry {input = new float[nIn], output = new float[nOut]};
			return buffer2;
		}
		catch (Exception)
		{
			var buffer2   = new DataSetEntry[size];
			for (var i = 0; i < size; i++)
				buffer2[i] = new DataSetEntry {input = new float[nIn], output = new float[nOut]};
			return buffer2;
		}
		
	}
	public static float[] Resize(this float[] source, int size)
	{
		var buffer = new float[source.Length];
		source.CopyTo(buffer, 0);
		var buffer2 = new float[size];

		for (var i = 0; i < Mathf.Min(size,source.Length); i++)
			buffer2[i] = buffer[i];
		return buffer2;
	}
	
	public static int[] Resize(this int[] source, int size)
	{
		var buffer = new int[source.Length];
		source.CopyTo(buffer, 0);
		var buffer2 = new int[size];

		for (var i = 0; i < Mathf.Min(size, source.Length); i++)
			buffer2[i] = buffer[i];
		return buffer2;
	}
}