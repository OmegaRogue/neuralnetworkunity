﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace NeuralNodes
{
	[Serializable]
	public class NeuralGraphData 
	{
		[SerializeField]
		private List<SerializedNode> serializedInputNodes = new List<SerializedNode>();
		
		[SerializeField]
		private List<SerializedNode> serializedOutputNodes = new List<SerializedNode>();
		
		[SerializeField]
		private List<SerializedNode> serializedNodes = new List<SerializedNode>();
		
		[SerializeField]
		private List<SerializedEdge> serializedEdges = new List<SerializedEdge>();
		
		public List<SerializedEdge> SerializedEdges
		{
			get { return serializedEdges; }
		}

		public List<SerializedNode> SerializedNodes
		{
			get { return serializedNodes; }
		}

		public List<SerializedNode> SerializedInputNodes
		{
			get { return serializedInputNodes; }
		}

		public List<SerializedNode> SerializedOutputNodes
		{
			get { return serializedOutputNodes; }
		}

		public void GetEdges(string nodeGuid, string memberName, List<SerializedEdge> foundEdges)
		{
			foreach (var serializedEdge in serializedEdges)
			{
				if (serializedEdge.sourceNodeGuid == nodeGuid && serializedEdge.sourceMemberName == memberName)
					foundEdges.Add(serializedEdge);
				else if (serializedEdge.targetNodeGuid == nodeGuid && serializedEdge.targetMemberName == memberName)
					foundEdges.Add(serializedEdge);
			}
		}
		
		
		
		public IEnumerable<SerializedEdge> GetEdges(string nodeGuid, string memberName)
		{
			var edges = new List<SerializedEdge>();
			GetEdges(nodeGuid, memberName, edges);
			return edges;
		}
	
	}
	

	[Serializable]
	public class SerializedNode
	{
		public string nodeType;
		public string json;
	}
	
	[Serializable]
	public class SerializedEdge
	{
		public string sourceNodeGuid;
		public string sourceMemberName;
		public string targetNodeGuid;
		public string targetMemberName;
	}
}