﻿using System.Linq;

using NeuralNodes.Editor.Ports;
using NeuralNodes.NeuralNodes;

using UnityEngine;

namespace NeuralNodes.Editor.Nodes
{
	[Title("Transfer", "Linear")]
	[NodeEditorType(typeof(LinearTransferNode))]
	public class LinearTransferNodeEditor : AbstractNeuralNodeEditor
	{
		private static readonly string[] Labels       = {"X"};
		private static readonly string[] OutputLabels = {"Value", "Derivative"};

		[SerializeField] private float _value;


		public override void ConstructNode()
		{
			var index = GetInputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new VectorNeuralSlot(index,
										 this,
										 nameof(LinearTransferNode.Vector1Input),
										 "Input",
										 SlotDirection.Input,
										 Labels,
										 () => new Vector4(_value, 0, 0, 0),
										 newValue => _value = newValue.x));

			index = GetOutputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new VectorNeuralSlot(index,
										 this,
										 nameof(LinearTransferNode.FunctionOutput),
										 "Output",
										 SlotDirection.Output,
										 OutputLabels,
										 () => new Vector2(_value, 1),
										 null));
		}
	}
}