﻿using System;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
    public class Vector1NeuralNode : NeuralNode
    {
        public event Action<float> Vector1Output;

        private float _value;

        public void Vector1Input(float value)
        {
            Debug.Log("Vector1NeuralNode SetValue " + value);
            _value = value;
            if (Vector1Output != null) Vector1Output(_value);
        }
    }
}