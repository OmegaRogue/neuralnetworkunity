﻿using System;
using System.Collections.Generic;
using System.Linq;

using Enums;

using UnityEngine;
using UnityEngine.UI;

public class Neuron : MonoBehaviour
{
	public static DisplayNn Nn;

	private bool _initialized;
	public  Text biasLabel;

	public float delta;
	public float derivative;

	public float error;

	public float gradient;

	public float         input;
	public List<Synapse> inputSynapses;

	public Vector3 leftOffset = new Vector3(-50, 0);

	public List<Synapse> outputSynapses;

	public Vector3 rightOffset = new Vector3(50, 0);
	public float   totalError;


	public ENeuronType type;


	public float value;


	public Text valueLabel;

	public Vector3 Left
	{
		get { return transform.position + leftOffset; }
	}

	public Vector3 Right
	{
		get { return transform.position + rightOffset; }
	}


	public void Initialize(ENeuronType newType, string newName)
	{
		type = newType;
		name = newName;


		outputSynapses = new List<Synapse>();
		inputSynapses  = new List<Synapse>();

		Nn.learnEvent += Learn;

		_initialized = true;
	}


	public void Update()
	{
		valueLabel.text = $"{Math.Round(value, 4)}";
		biasLabel.text  = $"{delta}";
	}


	public void Calculate()
	{
		if (!_initialized)
			return;


		switch (type)
		{
			case ENeuronType.Hidden:
			case ENeuronType.Output:
				var result =
					NeuralMaths.Transfer[(int) Nn.transferFunction](NeuralMaths.Input
																		[
																		 (int) Nn
																			.inputFunction](inputSynapses
																							   .Select(synapse =>
																										   synapse
																											  .Data)));
				value      = result.value;
				derivative = result.derivative;
				break;
			case ENeuronType.Input:
				value = input;
				break;
			case ENeuronType.Bias:
				value = 1;
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	public void Learn()
	{
		switch (type)
		{
			case ENeuronType.Output:
			{
				totalError = NeuralMaths.Error[(int) Nn.errorFunction](value, Nn.CurrentData.output[0]);
				error      = value - Nn.CurrentData.output[0];
				delta      = error * derivative;
				break;
			}
			case ENeuronType.Bias:
			case ENeuronType.Input:
			case ENeuronType.Hidden:
				var sum = outputSynapses.Sum(synapse => synapse.output.delta * synapse.weight);
				delta = derivative * sum;
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}
}