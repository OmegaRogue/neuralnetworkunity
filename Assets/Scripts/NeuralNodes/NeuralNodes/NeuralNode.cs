﻿using System;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
	[Serializable]
	public class NeuralNode
	{
		[SerializeField] private string displayName = "";

		[SerializeField] private string nodeGuid = "";

		public string NodeGuid
		{
			get { return nodeGuid; }
		}

		public string DisplayName
		{
			get { return displayName; }
		}
	}
}