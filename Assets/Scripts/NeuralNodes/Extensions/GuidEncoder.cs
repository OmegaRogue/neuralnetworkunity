﻿using System;

namespace NeuralNodes.Extensions
{
	public static class GuidEncoder
	{
		public static string Encode(Guid guid)
		{
			var enc = Convert.ToBase64String(guid.ToByteArray());
			return String.Format("{0:X}", enc.GetHashCode());
		}
	}
}