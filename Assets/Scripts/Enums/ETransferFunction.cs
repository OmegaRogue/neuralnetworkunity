﻿namespace Enums
{
	public enum ETransferFunction
	{
		Sigmoid,
		TanH,
		ReLu,
		Linear,
		ArcTan,
		ArSinH,
		SoftSign,
		Gaussian,
		Sin,
		Log
	}
}