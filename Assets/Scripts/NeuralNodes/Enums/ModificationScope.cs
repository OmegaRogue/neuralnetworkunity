﻿namespace NeuralNodes
{
	public enum ModificationScope
	{
		Nothing,
		Node,
		Graph,
		Topological,
		Layout,
	}
}