﻿using System;

namespace NeuralNodes
{
    /// <summary>
    /// Implement on a component that should be able to serve as input to a logic graph.
    /// </summary>
    public interface IInputComponent
    {
        event Action<IInputComponent, long> Changed;

        void OnChange();

    }
}