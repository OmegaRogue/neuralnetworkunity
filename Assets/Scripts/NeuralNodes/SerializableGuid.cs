﻿using System;

using UnityEngine;

namespace NeuralNodes
{
	[Serializable]
	public class SerializableGuid : ISerializationCallbackReceiver
	{
		public SerializableGuid()
		{
			_mGuid = Guid.NewGuid();
		}

		public SerializableGuid(Guid guid)
		{
			_mGuid = guid;
		}

		[NonSerialized]
		private Guid _mGuid;

		[SerializeField]
		private string mGuidSerialized;

		public Guid Guid
		{
			get { return _mGuid; }
		}

		public virtual void OnBeforeSerialize()
		{
			mGuidSerialized = _mGuid.ToString();
		}

		public virtual void OnAfterDeserialize()
		{
			if (!string.IsNullOrEmpty(mGuidSerialized))
				_mGuid = new Guid(mGuidSerialized);
			else
				_mGuid = Guid.NewGuid();
		}
	}
}