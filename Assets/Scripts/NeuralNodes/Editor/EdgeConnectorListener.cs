﻿using NeuralNodes.Editor.Ports;

using UnityEditor.Experimental.GraphView;

using UnityEngine;

namespace NeuralNodes.Editor
{
    public class EdgeConnectorListener : IEdgeConnectorListener
    {
        private readonly NeuralGraphEditorView _neuralGraphEditorView;
        private readonly SearchWindowProvider _searchWindowProvider;

        public EdgeConnectorListener(NeuralGraphEditorView neuralGraphEditorView, SearchWindowProvider searchWindowProvider)
        {
            _neuralGraphEditorView = neuralGraphEditorView;
            _searchWindowProvider = searchWindowProvider;
        }

        public void OnDropOutsidePort(Edge edge, Vector2 position)
        {
            var draggedPort = (edge.output != null ? edge.output.edgeConnector.edgeDragHelper.draggedPort : null) ??
                              (edge.input != null ? edge.input.edgeConnector.edgeDragHelper.draggedPort : null);
            _searchWindowProvider.ConnectedNeuralPort = (NeuralPort) draggedPort;
            SearchWindow.Open(new SearchWindowContext(GUIUtility.GUIToScreenPoint(Event.current.mousePosition)),
                _searchWindowProvider);
        }

        public void OnDrop(GraphView graphView, Edge edge)
        {
            _neuralGraphEditorView.AddEdge(edge);
        }
    }
}