﻿using System.Linq;

using NeuralNodes.Editor.Ports;
using NeuralNodes.NeuralNodes;

using UnityEngine;

namespace NeuralNodes.Editor.Nodes
{
    [Title("Output", "Vector1")]
    [NodeEditorType(typeof(Vector1OutputNeuralNode))]
    public class Vector1OutputNeuralNodeEditor : AbstractNeuralNodeEditor, IOutputNode
    {
        [SerializeField] private float _value;

        private static readonly string[] Labels = {"X"};

        public override void ConstructNode()
        {
            var index = GetInputSlots<NeuralSlot>().Count() + 1;
            AddSlot(new VectorNeuralSlot(index,
                this,
                nameof(Vector1OutputNeuralNode.Vector1Input),
                "Vector1Input",
                SlotDirection.Input,
                Labels,
                () => new Vector4(_value, 0, 0, 0),
                (newValue) => _value = newValue.x));
        }
    }
}