﻿﻿using UnityEditor;

using UnityEngine;

[CustomPropertyDrawer(typeof(NetworkTopology))]
public class TopologyDrawer : PropertyDrawer
{
	private float _yOffset = 0f;
	public override void OnGUI(
		Rect               position,
		SerializedProperty property,
		GUIContent         label
	)
	{
		
		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		// Draw label
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), GUIContent.none);

		// Don't make child fields be indented
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		_yOffset = 0f;

		// Calculate rects
		var inputRect = new Rect(position.x,      position.y + _yOffset, position.width,                  20);
		_yOffset += inputRect.height;
		var hiddenLayerCountRect = new Rect(position.x, position.y + _yOffset, position.width, 20);
		_yOffset += hiddenLayerCountRect.height;
		
		// Draw fields - passs GUIContent.none to each so they are drawn without labels
		
		
		EditorGUI.PropertyField(inputRect, property.FindPropertyRelative("inputLayerSize"), new GUIContent("Inputs"));

		
		EditorGUI.PropertyField(hiddenLayerCountRect, property.FindPropertyRelative("nHiddenLayers"), new GUIContent("Hidden Layers"));
		

		var hiddenLayerArray = property.FindPropertyRelative("hiddenLayerSize");
		hiddenLayerArray.arraySize = property.FindPropertyRelative("nHiddenLayers").intValue;

		EditorGUI.indentLevel = 1;
		var rect = new Rect(position.x, position.y + _yOffset, position.width, 20);
		for (int i = 0; i < hiddenLayerArray.arraySize; i++)
		{
			rect.y = position.y + _yOffset;
			EditorGUI.PropertyField(rect,
									hiddenLayerArray.GetArrayElementAtIndex(i),
									new GUIContent($"Hidden Layer {i + 1}"));
			_yOffset += rect.height;
		}
		
		EditorGUI.indentLevel = 0;
		
		var outputRect = new Rect(position.x, position.y + _yOffset, position.width, 20);
		_yOffset += outputRect.height;
		var biasRect = new Rect(position.x, position.y + _yOffset, position.width, 20);
		_yOffset += biasRect.height;
		
	
		EditorGUI.PropertyField(outputRect, property.FindPropertyRelative("outputLayerSize"), new GUIContent("Outputs"));
		EditorGUI.PropertyField(biasRect, property.FindPropertyRelative("biases"), new GUIContent("Biases"));

		// Set indent back to what it was
		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty();
	}
	
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return _yOffset;
	}
}