﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using NeuralNodes.Editor.Controls;
using NeuralNodes.Editor.Ports;
using NeuralNodes.Extensions;

using UnityEditor.Experimental.GraphView;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Nodes
{
	/// <summary>
	///     Actual visual nodes which gets added to the graph UI.
	/// </summary>
	public class NeuralNodeView : Node
	{
		private readonly List<SerializedEdge>   _foundEdges = new List<SerializedEdge>();
		private          IEdgeConnectorListener _connectorListener;
		private          VisualElement          _controlItems;
		private          VisualElement          _controlsDivider;
		private          VisualElement          _portInputContainer;

		public AbstractNeuralNodeEditor NeuralNodeEditor { get; private set; }

		public override bool expanded
		{
			get { return base.expanded; }
			set
			{
				Debug.Log(value);
				if (base.expanded != value)
					base.expanded = value;

				NeuralNodeEditor.Expanded = value;
				RefreshExpandedState(); //This should not be needed. GraphView needs to improve the extension api here
				UpdatePortInputVisibilities();
			}
		}

		public void Initialize(AbstractNeuralNodeEditor neuralNodeEditor, IEdgeConnectorListener connectorListener)
		{
			this.LoadAndAddStyleSheet("Styles/NeuralNodeView");

			switch (neuralNodeEditor)
			{
				case IInputNode _:
					AddToClassList("input");
					break;
				case IOutputNode _:
					AddToClassList("output");
					break;
			}

			_connectorListener = connectorListener;
			NeuralNodeEditor   = neuralNodeEditor;
			title              = NeuralNodeEditor.NodeType();

			var contents = this.Q("contents");

			var controlsContainer = new VisualElement {name = "controls"};

			_controlsDivider = new VisualElement {name = "divider"};
			_controlsDivider.AddToClassList("horizontal");
			controlsContainer.Add(_controlsDivider);
			_controlItems = new VisualElement {name = "items"};
			controlsContainer.Add(_controlItems);

			foreach (var propertyInfo in
				neuralNodeEditor.GetType().GetProperties(BindingFlags.Instance |
														 BindingFlags.Public   |
														 BindingFlags.NonPublic))
			foreach (INodeControlAttribute attribute in
				propertyInfo.GetCustomAttributes(typeof(INodeControlAttribute), false))
				_controlItems.Add(attribute.InstantiateControl(neuralNodeEditor, propertyInfo));
			contents.Add(controlsContainer);

			// Add port input container, which acts as a pixel cache for all port inputs
			_portInputContainer = new VisualElement
								  {
									  name = "portInputContainer",

//                clippingOptions = ClippingOptions.ClipAndCacheContents,
									  pickingMode = PickingMode.Ignore
								  };
			Add(_portInputContainer);

			var foundSlots = new List<NeuralSlot>();
			neuralNodeEditor.GetSlots(foundSlots);
			AddPorts(foundSlots);

			SetPosition(new Rect(neuralNodeEditor.Position.x, neuralNodeEditor.Position.y, 0, 0));
			UpdatePortInputs();
			base.expanded = neuralNodeEditor.Expanded;
			RefreshExpandedState();
			UpdatePortInputVisibilities();
		}

		private void AddPorts(IEnumerable<NeuralSlot> slots)
		{
			foreach (var slot in slots)
			{
				var port = NeuralPort.Create(slot, _connectorListener);
				if (slot.IsOutputSlot)
					outputContainer.Add(port);
				else
					inputContainer.Add(port);
			}
		}

		private void UpdatePortInputs()
		{
			foreach (var port in inputContainer.Children().OfType<NeuralPort>())
			{
				if (port.Slot.IsConnected)
					continue;

				if (_portInputContainer.Children().OfType<PortInputView>().Any(a => Equals(a.Description, port.Slot)))
					continue;
				var portInputView = new PortInputView(port.Slot) {style = {position = Position.Absolute}};
				_portInputContainer.Add(portInputView);
				if (float.IsNaN(port.layout.width))
					port.RegisterCallback<GeometryChangedEvent>(UpdatePortInput);
				else
					SetPortInputPosition(port, portInputView);
			}
		}


		private void SetPortInputPosition(NeuralPort port, PortInputView inputView)
		{
			inputView.style.top           = port.layout.y;
			inputView.parent.style.height = inputContainer.layout.height;
		}


		private void UpdatePortInput(GeometryChangedEvent evt)
		{
			var port = (NeuralPort) evt.target;
			var inputView = _portInputContainer.Children().OfType<PortInputView>()
											   .First(x => Equals(x.Description, port.Slot));

			SetPortInputPosition(port, inputView);
			port.UnregisterCallback<GeometryChangedEvent>(UpdatePortInput);
		}

		public void UpdatePortInputVisibilities()
		{
			foreach (var portInputView in _portInputContainer.Children().OfType<PortInputView>())
			{
				var slot          = portInputView.Description;
				var oldVisibility = portInputView.visible;
				_foundEdges.Clear();
				NeuralNodeEditor.Owner.NeuralGraphEditorObject.NeuralGraphData.GetEdges(NeuralNodeEditor.NodeGuid,
																						portInputView
																						   .Description.MemberName,
																						_foundEdges);
				portInputView.visible = expanded && _foundEdges.Count == 0;
				if (portInputView.visible != oldVisibility)
					_portInputContainer.MarkDirtyRepaint();
			}
		}


		public void OnModified(ModificationScope scope)
		{
			if (scope == ModificationScope.Topological)
			{
				var slots = NeuralNodeEditor.GetSlots<NeuralSlot>().ToList();

				var inputPorts = inputContainer.Children().OfType<NeuralPort>().ToList();
				foreach (var port in inputPorts)
				{
					var currentSlot = port.Slot;
					var newSlot     = slots.FirstOrDefault(s => s.Id == currentSlot.Id);
					if (newSlot == null)
					{
						// Slot doesn't exist anymore, remove it
						inputContainer.Remove(port);

						// We also need to remove the inline input
						var portInputView = inputContainer
										   .Children().OfType<PortInputView>()
										   .FirstOrDefault(v => Equals(v.Description, port.Slot));
						if (portInputView != null)
							portInputView.RemoveFromHierarchy();
					}
					else
					{
						port.Slot = newSlot;
						var portInputView = inputContainer
										   .Children().OfType<PortInputView>()
										   .FirstOrDefault(x => x.Description.Id == currentSlot.Id);
						if (newSlot.IsConnected)
							portInputView?.RemoveFromHierarchy();
						else
							portInputView?.UpdateSlot(newSlot);

						slots.Remove(newSlot);
					}
				}

				var outputPorts = outputContainer.Children().OfType<NeuralPort>().ToList();
				foreach (var port in outputPorts)
				{
					var currentSlot = port.Slot;
					var newSlot     = slots.FirstOrDefault(s => s.Id == currentSlot.Id);
					if (newSlot == null)
					{
						outputContainer.Remove(port);
					}
					else
					{
						port.Slot = newSlot;
						slots.Remove(newSlot);
					}
				}

				AddPorts(slots);

				slots.Clear();
				slots.AddRange(NeuralNodeEditor.GetSlots<NeuralSlot>());


				if (inputContainer.childCount > 0)
					inputContainer.Sort((x, y) => slots.IndexOf(((NeuralPort) x).Slot) -
												  slots.IndexOf(((NeuralPort) y).Slot));
				if (outputContainer.childCount > 0)
					outputContainer.Sort((x, y) => slots.IndexOf(((NeuralPort) x).Slot) -
												   slots.IndexOf(((NeuralPort) y).Slot));

				UpdatePortInputs();
				UpdatePortInputVisibilities();
			}

			RefreshExpandedState(); //This should not be needed. GraphView needs to improve the extension api here
		}

//        public void UpdatePortInputTypes()
//        {
//            foreach (var anchor in inputContainer.Concat(outputContainer).OfType<NeuralPort>())
//            {
//                var slot = anchor.Description;
//                anchor.portName = slot._displayName;
//                anchor.visualClass = slot.ValueType.ToString();
//            }
//
//            foreach (var portInputView in _portInputContainer.OfType<PortInputView>())
//                portInputView.UpdateSlotType();
//
//            foreach (var control in _controlItems)
//            {
//                var listener = control as INodeModificationListener;
//                if (listener != null)
//                    listener.OnNodeModified(ModificationScope.Graph);
//            }
//        }
	}
}