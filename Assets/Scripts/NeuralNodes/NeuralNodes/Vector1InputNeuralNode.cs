﻿using System;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
    public class Vector1InputNeuralNode : NeuralNode
    {
        public event Action<float> Vector1Output;
       
        [Vector1Input]
        public void Vector1Input(float value)
        {
            Debug.Log("Vector1InputNeuralNode Vector1Input " + value);
            if (Vector1Output != null) Vector1Output(value);
        }
    }
}