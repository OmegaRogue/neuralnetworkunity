﻿using System;
using System.Collections.Generic;
using System.Linq;

using NeuralNodes.Editor.Ports;

using UnityEngine;

namespace NeuralNodes.Editor.Nodes
{
	/// <summary>
	///     Describes how to draw a node, paired with GenericNodeview
	/// </summary>
	[Serializable]
	public abstract class AbstractNeuralNodeEditor
	{
		public const string DocUrl =
			"https://github.com/Unity-Technologies/ScriptableRenderPipeline/tree/master/com.unity.shadergraph/Documentation%7E/";

		[NonSerialized] private List<NeuralSlot> _slots = new List<NeuralSlot>();

		[SerializeField] private string displayName;

		[SerializeField] private bool expanded = true;

		[SerializeField] private string nodeGuid;

		[SerializeField] private Vector3 position;

		public AbstractNeuralNodeEditor()
		{
			nodeGuid = Guid.NewGuid().ToString();
			ConstructNode();
		}

		public NeuralGraphView Owner          { get; set; }
		public SerializedNode  SerializedNode { get; set; }


		public string Name
		{
			get { return displayName; }
			set { displayName = value; }
		}

		public virtual string DocumentationUrl
		{
			get { return GetDocumentationString(); }
		}

		public Vector3 Position
		{
			get { return position; }
			set { position = value; }
		}

		public bool Expanded
		{
			get { return expanded; }
			set { expanded = value; }
		}

		public string NodeGuid
		{
			get { return nodeGuid; }
		}

		public string GetDocumentationString()
		{
			return $"{DocUrl}{displayName.Replace(" ", "-")}" + "-Node.md";
		}

		public abstract void ConstructNode();

		public string NodeType()
		{
			var attrs = GetType().GetCustomAttributes(typeof(NodeEditorType), false) as NodeEditorType[];
			if (attrs != null && attrs.Length > 0)
			{
				return attrs[0].NodeType.Name;
			}

			Debug.LogWarning(GetType() + " requires a NodeType attribute");
			return "";
		}

		public void GetInputSlots<T>(List<T> foundSlots) where T : NeuralSlot
		{
			foundSlots.AddRange(_slots.Where(slot => slot.IsInputSlot && slot is T).Cast<T>());
		}

		public void GetOutputSlots<T>(List<T> foundSlots) where T : NeuralSlot
		{
			foundSlots.AddRange(_slots.Where(slot => slot.IsOutputSlot && slot is T).Cast<T>());
		}

		public void GetSlots<T>(List<T> foundSlots) where T : NeuralSlot
		{
			foundSlots.AddRange(_slots.OfType<T>());
		}


		public IEnumerable<T> GetSlots<T>() where T : NeuralSlot
		{
			var slots = new List<T>();
			GetSlots(slots);
			return slots;
		}

		public IEnumerable<T> GetInputSlots<T>() where T : NeuralSlot
		{
			var slots = new List<T>();
			GetInputSlots(slots);
			return slots;
		}

		public IEnumerable<T> GetOutputSlots<T>() where T : NeuralSlot
		{
			var slots = new List<T>();
			GetOutputSlots(slots);
			return slots;
		}

		public void SetDirty()
		{
			SerializedNode.json = JsonUtility.ToJson(this);
		}

		public void AddSlot(NeuralSlot neuralSlot)
		{
			if (!(neuralSlot is NeuralSlot))
				throw new
					ArgumentException(string.Format("Trying to add slot {0} to Material node {1}, but it is not a {2}",
													neuralSlot,
													this,
													typeof(NeuralSlot)));

			_slots.Add(neuralSlot);
		}

		public T FindSlot<T>(string memberName) where T : NeuralSlot
		{
			return _slots.Where(slot => slot.MemberName == memberName && slot is T).Cast<T>().FirstOrDefault();
		}

		public T FindInputSlot<T>(string memberName) where T : NeuralSlot
		{
			return _slots.Where(slot => slot.IsInputSlot && slot.MemberName == memberName && slot is T).Cast<T>()
						 .FirstOrDefault();
		}

		public T FindOutputSlot<T>(string memberName) where T : NeuralSlot
		{
			return _slots.Where(slot => slot.IsOutputSlot && slot.MemberName == memberName && slot is T).Cast<T>()
						 .FirstOrDefault();
		}
	}
}