﻿using System;

using UnityEngine;

namespace NeuralNodes.NeuralNodes
{
    public class Vector1OutputNeuralNode : NeuralNode
    {
        [Output]
        public event Action<float> Vector1Output;

        [SerializeField]
        private float _value;

        public void Vector1Input(float value)
        {
            Debug.Log("Vector1OutputNeuralNode SetValue " + value);
            _value = value;
            Vector1Output?.Invoke(_value);
        }
    }
}