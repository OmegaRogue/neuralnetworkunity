﻿using System;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Types;

using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
    [Serializable]
    public class BooleanSlot : NeuralSlot
    {
        private readonly string[] _labels;
        private readonly Func<Bool4> _get;
        private readonly Action<Bool4> _set;
        
        public override SlotValueType ValueType { get { return SlotValueType.Boolean; } }

        public BooleanSlot(int id, AbstractNeuralNodeEditor owner, 
            string memberName, 
            string displayName,
            SlotDirection direction,
            string[] labels,
            Func<Bool4> get,
            Action<Bool4> set) : base(id, owner, memberName, displayName, direction)
        {
            _labels = labels;
            _get = get;
            _set = set;
        }

        public override bool IsCompatibleWithInputSlotType(SlotValueType inputType)
        {
            return inputType == SlotValueType.Boolean;
        }
        
        public override VisualElement InstantiateControl()
        {
            return new BooleanSlotControlView(
                this,
                _labels,
                _get, 
                _set);
        }
    }
}