﻿using System;
using System.Reflection;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Extensions;

using UnityEditor;
using UnityEditor.UIElements;

using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Controls
{
    [AttributeUsage(AttributeTargets.Property)]
    public class EnumControlAttribute : Attribute, INodeControlAttribute
    {
        private readonly string _label;

        public EnumControlAttribute(string label = null)
        {
            _label = label;
        }

        public VisualElement InstantiateControl(AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo)
        {
            return new EnumControlView(_label, neuralNodeEditor, propertyInfo);
        }
    }

    public class EnumControlView : VisualElement
    {
        private readonly AbstractNeuralNodeEditor _neuralNodeEditor;
        private readonly PropertyInfo _propertyInfo;

        public EnumControlView(string label, AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo)
        {
            this.LoadAndAddStyleSheet("Styles/Controls/EnumControlView");
            _neuralNodeEditor = neuralNodeEditor;
            _propertyInfo = propertyInfo;
            if (!propertyInfo.PropertyType.IsEnum)
                throw new ArgumentException("Property must be an enum.", nameof(propertyInfo));
            Add(new Label(label ?? ObjectNames.NicifyVariableName(propertyInfo.Name)));
            var enumField = new EnumField((Enum)_propertyInfo.GetValue(_neuralNodeEditor, null));
            enumField.RegisterValueChangedCallback(OnValueChanged);
            Add(enumField);
        }

        private void OnValueChanged(ChangeEvent<Enum> evt)
        {
            var value = (Enum)_propertyInfo.GetValue(_neuralNodeEditor, null);
            if (!evt.newValue.Equals(value))
            {
                _neuralNodeEditor.Owner.NeuralGraphEditorObject.RegisterCompleteObjectUndo("Change " + _neuralNodeEditor.NodeType());
                _propertyInfo.SetValue(_neuralNodeEditor, evt.newValue, null);
            }
        }
    }
}
