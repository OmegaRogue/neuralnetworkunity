﻿using System.Linq;

using NeuralNodes.Editor.Ports;
using NeuralNodes.NeuralNodes;

using UnityEngine;

namespace NeuralNodes.Editor.Nodes
{
	[Title("Utility", "Split")]
	[NodeEditorType(typeof(VectorSplitNode))]
	public class VectorSplitNodeEditor : AbstractNeuralNodeEditor
	{
		private static readonly string[] Labels = {"X", "Y", "Z", "W"};

		[SerializeField] private Vector4 _value;


		public override void ConstructNode()
		{
			var index = GetInputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new VectorNeuralSlot(index,
										 this,
										 nameof(VectorSplitNode.Vector4Input),
										 "Input",
										 SlotDirection.Input,
										 Labels,
										 () => _value,
										 newValue => _value = newValue));
			index = GetInputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new FloatSlot(index,
								  this,
								  nameof(VectorSplitNode.XOutput),
								  "X",
								  SlotDirection.Output,
								  Labels,
								  () => _value.x,
								  null));
			index = GetInputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new FloatSlot(index,
								  this,
								  nameof(VectorSplitNode.YOutput),
								  "Y",
								  SlotDirection.Output,
								  Labels,
								  () => _value.y,
								  null));
			index = GetInputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new FloatSlot(index,
								  this,
								  nameof(VectorSplitNode.ZOutput),
								  "Z",
								  SlotDirection.Output,
								  Labels,
								  () => _value.z,
								  null));
			index = GetInputSlots<NeuralSlot>().Count() + 1;
			AddSlot(new FloatSlot(index,
								  this,
								  nameof(VectorSplitNode.WOutput),
								  "W",
								  SlotDirection.Output,
								  Labels,
								  () => _value.w,
								  null));
		}
	}
}