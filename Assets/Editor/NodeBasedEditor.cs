﻿using System;

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class NodeBasedEditor : EditorWindow
{
    private List<NodeOld> _nodes;
    private List<InputNode> _inputNodes;
    private List<HiddenNode> _hiddenNodes;
    private List<OutputNode> _outputNodes;
    private List<BiasNode> _biasNodes;
    private List<Connection> _connections;

    private GUIStyle _nodeStyle;
    private GUIStyle _selectedNodeStyle;
    private GUIStyle _inPointStyle;
    private GUIStyle _outPointStyle;

    private ConnectionPoint _selectedInPoint;
    private ConnectionPoint _selectedOutPoint;

    private Vector2 _offset;
    private Vector2 _drag;
    private readonly float _zoom = 1;
    
    
   

    [MenuItem("Window/Neural Network Editor")]
    private static void OpenWindow()
    {
        var window = GetWindow<NodeBasedEditor>();
        window.titleContent = new GUIContent("Neural Network");
    }

    private void OnEnable()
    {
        _nodeStyle = NodeEditorStyles.GetNodeStyle();

        _selectedNodeStyle = NodeEditorStyles.GetSelectedNodeStyle();

        _inPointStyle = NodeEditorStyles.GetInPointStyle();

        _outPointStyle = NodeEditorStyles.GetOutPointStyle();
        
        
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Forward Propagate"))
        {
            ForwardPropagate();
        }
        GUILayout.EndHorizontal();
        DrawGrid(20, 0.2f);
        DrawGrid(100, 0.4f);

        DrawNodes();
        DrawConnections();

        DrawConnectionLine(Event.current);

        ProcessConnectionPointEvents(Event.current);
        ProcessNodeEvents(Event.current);
        ProcessEvents(Event.current);

        if (GUI.changed) Repaint();
    }

    private void ProcessConnectionPointEvents(Event current)
    {
        if (_nodes == null)
            return;
        for (var i = _nodes.Count - 1; i >= 0; i--)
        {
            
            var guiChanged = _nodes[i].InPoint.ProcessEvents(current);
            guiChanged |= _nodes[i].OutPoint.ProcessEvents(current);
            if (guiChanged)
                GUI.changed = true;
        }
    }

    private void DrawGrid(float gridSpacing, float gridOpacity)
    {
        var spacing = gridSpacing * _zoom;
        var widthDivs = Mathf.CeilToInt(position.width / spacing);
        var heightDivs = Mathf.CeilToInt(position.height / spacing);

        Handles.BeginGUI();
        Handles.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, gridOpacity);

        _offset += _drag * 0.5f;
        var newOffset = new Vector3(_offset.x % spacing, _offset.y % spacing, 0);

        for (var i = 0; i < widthDivs; i++)
            Handles.DrawLine(new Vector3(spacing * i, -spacing,        0)  + newOffset,
                             new Vector3(spacing * i, position.height, 0f) + newOffset);

        for (var j = 0; j < heightDivs; j++)
            Handles.DrawLine(new Vector3(-spacing,       spacing * j, 0)  + newOffset,
                             new Vector3(position.width, spacing * j, 0f) + newOffset);

        Handles.color = GUI.backgroundColor;
        Handles.EndGUI();
    }

    private void DrawNodes()
    {
        if (_nodes == null)
            return;
        foreach (var t in _nodes)
            t.Draw();
    }

    private void DrawConnections()
    {
        if (_connections == null)
            return;
        foreach (var t in _connections)
            t.Draw();
    }

    private void ProcessEvents(Event e)
    {
        _drag = Vector2.zero;

        switch (e.type)
        {
            case EventType.MouseDown:
                switch (e.button)
                {
                    case 0:
                        ClearConnectionSelection();
                        break;
                    case 1:
                        ProcessContextMenu(e.mousePosition);
                        break;
                }
                break;

            case EventType.MouseDrag:
                if (e.button == 2)
                    OnDrag(e.delta);
                break;
            case EventType.MouseUp:
                break;
            case EventType.MouseMove:
                break;
            case EventType.KeyDown:
                // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
                switch (e.keyCode)
                {
                    case KeyCode.F1:
                        OnClickAddInputNode(e.mousePosition); 
                        break;
                    case KeyCode.F2:
                        OnClickAddHiddenNode(e.mousePosition);
                        break;
                    case KeyCode.F3:
                        OnClickAddBiasNode(e.mousePosition); 
                        break;
                    case KeyCode.F4:
                        OnClickAddOutputNode(e.mousePosition);
                        break;
                    case KeyCode.F5:
                        ForwardPropagate();
                        break;
                }
                
                break;
            case EventType.KeyUp:
                break;
            case EventType.ScrollWheel:
                break;
            case EventType.Repaint:
                break;
            case EventType.Layout:
                break;
            case EventType.DragUpdated:
                break;
            case EventType.DragPerform:
                break;
            case EventType.DragExited:
                break;
            case EventType.Ignore:
                break;
            case EventType.Used:
                break;
            case EventType.ValidateCommand:
                break;
            case EventType.ExecuteCommand:
                break;
            case EventType.ContextClick:
                break;
            case EventType.MouseEnterWindow:
                break;
            case EventType.MouseLeaveWindow:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void ProcessNodeEvents(Event e)
    {
        if (_nodes == null)
            return;
        for (var i = _nodes.Count - 1; i >= 0; i--)
        {
            var guiChanged = _nodes[i].ProcessEvents(e);
            if (guiChanged)
                GUI.changed = true;
        }
    }

    private void DrawConnectionLine(Event e)
    {
        if (_selectedInPoint != null && _selectedOutPoint == null)
        {
            Handles.DrawBezier(
                _selectedInPoint.Rect.center,
                e.mousePosition,
                _selectedInPoint.Rect.center + Vector2.left * 50f,
                e.mousePosition - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        if (_selectedOutPoint != null && _selectedInPoint == null)
        {
            Handles.DrawBezier(
                _selectedOutPoint.Rect.center,
                e.mousePosition,
                _selectedOutPoint.Rect.center - Vector2.left * 50f,
                e.mousePosition + Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }
    }
    

    private void ProcessContextMenu(Vector2 mousePosition)
    {
        var genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Add Input Node"), false, () => OnClickAddInputNode(mousePosition)); 
        genericMenu.AddItem(new GUIContent("Add Hidden Node"), false, () => OnClickAddHiddenNode(mousePosition)); 
        genericMenu.AddItem(new GUIContent("Add Bias Node"), false, () => OnClickAddBiasNode(mousePosition)); 
        genericMenu.AddItem(new GUIContent("Add Output Node"), false, () => OnClickAddOutputNode(mousePosition)); 
        genericMenu.ShowAsContext();
    }

    private void OnDrag(Vector2 delta)
    {
        _drag = delta;

        if (_nodes != null)
            foreach (var t in _nodes)
                t.Drag(delta);

        GUI.changed = true;
    }


    private void OnClickAddInputNode(Vector2 mousePosition)
    {
        if (_nodes == null)
            _nodes = new List<NodeOld>();
        if(_inputNodes == null)
            _inputNodes = new List<InputNode>();

        var node = new InputNode(mousePosition,
                                 100,
                                 75,
                                 _nodeStyle,
                                 _selectedNodeStyle,
                                 _inPointStyle,
                                 _outPointStyle,
                                 OnClickInPoint,
                                 OnClickOutPoint,
                                 OnClickRemoveNode);

        _nodes.Add(node);
        _inputNodes.Add(node);
    }
   private void OnClickAddHiddenNode(Vector2 mousePosition)
    {
        if (_nodes == null)
            _nodes = new List<NodeOld>();
        if(_hiddenNodes == null)
            _hiddenNodes = new List<HiddenNode>();

        var node = new HiddenNode(mousePosition,
                                  100,
                                  75,
                                  _nodeStyle,
                                  _selectedNodeStyle,
                                  _inPointStyle,
                                  _outPointStyle,
                                  OnClickInPoint,
                                  OnClickOutPoint,
                                  OnClickRemoveNode);

        _nodes.Add(node);
        _hiddenNodes.Add(node);
    }
   private void OnClickAddBiasNode(Vector2 mousePosition)
    {
        if (_nodes == null)
            _nodes = new List<NodeOld>();
        if(_biasNodes == null)
            _biasNodes = new List<BiasNode>();

        var node = new BiasNode(mousePosition,
                                100,
                                75,
                                _nodeStyle,
                                _selectedNodeStyle,
                                _inPointStyle,
                                _outPointStyle,
                                OnClickInPoint,
                                OnClickOutPoint,
                                OnClickRemoveNode);

        _nodes.Add(node);
        _biasNodes.Add(node);
    }
   private void OnClickAddOutputNode(Vector2 mousePosition)
    {
        if (_nodes == null)
            _nodes = new List<NodeOld>();
        if(_outputNodes == null)
            _outputNodes = new List<OutputNode>();

        var node = new OutputNode(mousePosition,
                                  100,
                                  75,
                                  _nodeStyle,
                                  _selectedNodeStyle,
                                  _inPointStyle,
                                  _outPointStyle,
                                  OnClickInPoint,
                                  OnClickOutPoint,
                                  OnClickRemoveNode);

        _nodes.Add(node);
        _outputNodes.Add(node);
    }
   

    private void OnClickInPoint(ConnectionPoint inPoint)
    {
        _selectedInPoint = inPoint;

        if (_selectedOutPoint == null)
            return;
        if (_selectedOutPoint.Node != _selectedInPoint.Node)
        {
            CreateConnection();
            ClearConnectionSelection(); 
        }
        else
            ClearConnectionSelection();
    }

    private void OnClickOutPoint(ConnectionPoint outPoint)
    {
        _selectedOutPoint = outPoint;

        if (_selectedInPoint == null)
            return;
        if (_selectedOutPoint.Node != _selectedInPoint.Node)
        {
            CreateConnection();
            ClearConnectionSelection();
        }
        else
            ClearConnectionSelection();
    }

    private void OnClickRemoveNode(NodeOld node)
    {
        if (_connections != null)
        {
            var connectionsToRemove = _connections.Where(t => t.InPoint == node.InPoint || t.OutPoint == node.OutPoint).ToList();

            foreach (var t in connectionsToRemove)
                _connections.Remove(t);

            connectionsToRemove = null;
        }

        _nodes.Remove(node);
    }

    private void OnClickRemoveConnection(Connection connection)
    {
        connection.InPoint.Connections.Remove(connection);
        connection.OutPoint.Connections.Remove(connection);
        _connections.Remove(connection);
    }

    private void CreateConnection()
    {
        if (_connections == null)
        {
            _connections = new List<Connection>();
        }

        _connections.Add(new Connection(_selectedInPoint, _selectedOutPoint, OnClickRemoveConnection));
    }

    private void ClearConnectionSelection()
    {
        _selectedInPoint = null;
        _selectedOutPoint = null;
    }

    public void ForwardPropagate()
    {
        if (_biasNodes != null)
            foreach (var node in _biasNodes)
                node.Value = node.CalculateValue();
        if (_inputNodes != null)
            foreach (var node in _inputNodes)
                node.Value = node.CalculateValue();
        if (_hiddenNodes != null)
            foreach (var node in _hiddenNodes)
                node.Value = node.CalculateValue();
        if (_outputNodes != null)
            foreach (var node in _outputNodes)
                node.Value = node.CalculateValue();
       
    }
}