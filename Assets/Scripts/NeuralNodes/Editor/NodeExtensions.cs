﻿using System.Collections.Generic;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Editor.Ports;

namespace NeuralNodes.Editor
{
	public static class NodeExtensions
	{
		public static IEnumerable<T> GetSlots<T>(this AbstractNeuralNodeEditor node) where T : NeuralSlot
		{
			var slots = new List<T>();
			node.GetSlots(slots);
			return slots;
		}

		public static IEnumerable<T> GetInputSlots<T>(this AbstractNeuralNodeEditor node) where T : NeuralSlot
		{
			var slots = new List<T>();
			node.GetInputSlots(slots);
			return slots;
		}

		public static IEnumerable<T> GetOutputSlots<T>(this AbstractNeuralNodeEditor node) where T : NeuralSlot
		{
			var slots = new List<T>();
			node.GetOutputSlots(slots);
			return slots;
		}
	}
}