﻿using System;
using System.Collections.Generic;
using System.Reflection;

using NeuralNodes.NeuralNodes;

using UnityEngine;
using UnityEngine.Events;

namespace NeuralNodes
{
	[ExecuteInEditMode]
	public class NeuralGraphInstance : MonoBehaviour
	{
		private readonly List<NeuralNode> _inputNodes  = new List<NeuralNode>();
		private readonly List<NeuralNode> _nodes       = new List<NeuralNode>();
		private readonly List<NeuralNode> _outputNodes = new List<NeuralNode>();

		private                  int               _editorInstanceId;
		[SerializeField] private List<GraphInput>  inputs = new List<GraphInput>();
		[SerializeField] private NeuralGraphObject neuralGraphObject;
		[SerializeField] private List<GraphOutput> outputs = new List<GraphOutput>();

		public List<GraphInput> Inputs
		{
			get { return inputs; }
		}

		public List<GraphOutput> Outputs
		{
			get { return outputs; }
		}

		private void Awake()
		{
			Debug.Log("Awake");
		}

		public void OnEnable()
		{
			// Debug.Log("OnEnable");
			//is this needed? Probably in a build
//            if (neuralGraphObject != null)
//            {
//                neuralGraphObject.LoadLogicNodeGraph(_nodes, _inputNodes, _outputNodes);
//                UpdateInputsAndOutputs();
//            }
			HookUpGraph();
		}

		public void OnDisable()
		{
			// Debug.Log("OnDisable");

			for (var i = 0; i < Inputs.Count; ++i)
				Inputs[i].ValidateEventRegistered = false;
		}

		private void Reset()
		{
			_inputNodes.Clear();
			_outputNodes.Clear();
			neuralGraphObject = null;
		}

		public void OnValidate()
		{
			// Debug.Log("OnValidate");
			if (neuralGraphObject != null)
				for (var i = 0; i < Inputs.Count; ++i)
					if (Inputs[i].validate != null)
						Inputs[i].validate();
		}

		public void HookUpGraph()
		{
			if (neuralGraphObject != null)
			{
				neuralGraphObject.LoadLogicNodeGraph(_nodes, _inputNodes, _outputNodes);
				UpdateInputsAndOutputs();

				for (var i = 0; i < Inputs.Count; ++i)
					if (Inputs[i].componentValue != null)
					{
						var component = Inputs[i].componentValue as IInputComponent;
						if (component != null)
							Inputs[i].RegisterValidateEvent(component);
					}
					else if (Inputs[i].validate != null)
					{
						Inputs[i].validate();
					}
			}
		}

		private void UpdateInputsAndOutputs()
		{
			// Debug.Log("GraphLogic OnEnable");

			if (_inputNodes.Count != 0)
			{
				var loadedInputs = new List<GraphInput>();
				LoadInputs(loadedInputs);

				//add nodes
				for (var i = 0; i < loadedInputs.Count; ++i)
					if (Inputs.Find(n => n.memberName == loadedInputs[i].memberName &&
										 n.nodeGuid   == loadedInputs[i].nodeGuid) == null)
						Inputs.Add(loadedInputs[i]);

				//remove nodes and hook up
				for (var i = Inputs.Count - 1; i > -1; --i)
					if (loadedInputs.Find(n => n.memberName == Inputs[i].memberName &&
											   n.nodeGuid   == Inputs[i].nodeGuid) == null)
						Inputs.RemoveAt(i);
					else
						HookUpInput(Inputs[i]);
			}

			if (_outputNodes.Count != 0)
			{
				var loadedOutputs = new List<GraphOutput>();
				LoadOutputs(loadedOutputs);

				//add nodes
				for (var i = 0; i < loadedOutputs.Count; ++i)
					if (Outputs.Find(n => n.memberName == loadedOutputs[i].memberName &&
										  n.nodeGuid   == loadedOutputs[i].nodeGuid) == null)
						Outputs.Add(loadedOutputs[i]);

				//remove nodes and hook up
				for (var i = Outputs.Count - 1; i > -1; --i)
					if (loadedOutputs.Find(n => n.memberName == Outputs[i].memberName &&
												n.nodeGuid   == Outputs[i].nodeGuid) == null)
						Outputs.RemoveAt(i);
					else
						HookUpOutput(Outputs[i]);
			}
		}

		private void LoadInputs(List<GraphInput> inputs)
		{
			foreach (var node in _inputNodes)
			{
				var methods = node.GetType().GetMethods(BindingFlags.Public    |
														BindingFlags.NonPublic |
														BindingFlags.Instance);
				foreach (var method in methods)
				{
					var attrs = method.GetCustomAttributes(typeof(InputAttribute), false) as InputAttribute[];
					if (attrs.Length > 0)
					{
						var input = new GraphInput
									{
										memberName  = method.Name,
										nodeGuid    = node.NodeGuid,
										displayName = node.DisplayName + " " + method.Name,
										inputType   = attrs[0].InputType()
									};
						inputs.Add(input);
						if (attrs.Length > 1)
							Debug.LogWarning(method.Name + " on  " + node + "has multiple input attributes.");
					}
				}
			}
		}

		private void HookUpInput(GraphInput graphInput)
		{
			var node = _inputNodes.Find(n => n.NodeGuid == graphInput.nodeGuid);
			var methods = node.GetType().GetMethods(BindingFlags.Public    |
													BindingFlags.NonPublic |
													BindingFlags.Instance);
			foreach (var method in methods)
			{
				var attrs = method.GetCustomAttributes(typeof(InputAttribute), false) as InputAttribute[];
				if (attrs.Length > 0)
				{
					if (method.Name == graphInput.memberName)
					{
						graphInput.inputType = attrs[0].InputType();
						attrs[0].HookUpMethodInvoke(node, method, graphInput);
					}

					if (attrs.Length > 1)
						Debug.LogWarning(method.Name + " on  " + node + "has multiple input attributes.");
				}
			}
		}

		private void LoadOutputs(List<GraphOutput> outputs)
		{
			foreach (var node in _outputNodes)
			{
				var events = node.GetType().GetEvents(BindingFlags.Public    |
													  BindingFlags.NonPublic |
													  BindingFlags.Instance);
				foreach (var eventInfo in events)
				{
					var attrs = eventInfo.GetCustomAttributes(typeof(OutputAttribute), false) as OutputAttribute[];
					if (attrs.Length > 0)
					{
						// Debug.Log($"Output attribute found{attrs[0]}");
						var types = eventInfo.EventHandlerType.GetGenericArguments();
						if (types.Length > 1)
							Debug.LogError(eventInfo +
										   " has more than one generic type, only one currently supported.");
						else if (types.Length == 0)
							Debug.LogError(eventInfo + " has more than no generic types.");

						var graphOutput = new GraphOutput();
						graphOutput.memberName  = eventInfo.Name;
						graphOutput.nodeGuid    = node.NodeGuid;
						graphOutput.displayName = node.DisplayName + " " + eventInfo.Name;
						graphOutput.outputType  = types[0];
						outputs.Add(graphOutput);

						if (attrs.Length > 1)
							Debug.LogWarning(eventInfo.Name + " on  " + node + "has multiple input attributes.");
					}
				}
			}
		}

		private void HookUpOutput(GraphOutput graphOutput)
		{
			var node = _outputNodes.Find(n => n.NodeGuid == graphOutput.nodeGuid);
			var events = node.GetType().GetEvents(BindingFlags.Public    |
												  BindingFlags.NonPublic |
												  BindingFlags.Instance);
			foreach (var eventInfo in events)
			{
				var attrs = eventInfo.GetCustomAttributes(typeof(OutputAttribute), false) as OutputAttribute[];
				if (attrs.Length > 0)
				{
					if (eventInfo.Name == graphOutput.memberName)
					{
						var types = eventInfo.EventHandlerType.GetGenericArguments();
						graphOutput.outputType = types[0];
						graphOutput.SubscribeRaiseUpdate(eventInfo, node);
					}

					if (attrs.Length > 1)
						Debug.LogWarning(eventInfo.Name + " on  " + node + "has multiple input attributes.");
				}
			}
		}
	}

	[Serializable]
	public class GraphInput
	{
		public Component componentValue;
		public string    displayName;
		public float     floatValueW;
		public float     floatValueX;
		public float     floatValueY;
		public float     floatValueZ;
		public Type      inputType;
		public string    memberName;
		public string    nodeGuid;
		public Action    validate;

		public bool ValidateEventRegistered { get; set; }

		public void RegisterValidateEvent(IInputComponent inputComponent)
		{
			//the object may not be reloaded, and lose prior references, everytime, this checks
			//to ensure change doesn't get subscribed to multiple times
			if (!ValidateEventRegistered)
			{
				ValidateEventRegistered =  true;
				inputComponent.Changed  += OnValidate;
			}

			inputComponent.OnChange(); //call right after subscribing so it sends it's data through
		}

		public void OnValidate(IInputComponent component, long timestamp)
		{
			// Debug.Log($"OnValidate {component} {timestamp}");
			validate?.Invoke();
		}
	}


	[Serializable]
	public class GraphOutput
	{
		public                   string          displayName;
		public                   string          memberName;
		public                   string          nodeGuid;
		public                   Type            outputType;
		[SerializeField] private FloatUnityEvent updatedFloat = new FloatUnityEvent();

		[SerializeField] private ObjectUnityEvent updatedObject = new ObjectUnityEvent();

		[SerializeField] private Vector3UnityEvent updatedVector3 = new Vector3UnityEvent();

		private void RaiseUpdatedFloat(float value)
		{
//            Debug.Log("RaiseUpdatedFloat " + value);
			updatedFloat.Invoke(value);
		}

		private void RaiseUpdatedVector3(Vector3 value)
		{
//            Debug.Log("RaiseUpdatedVector3 " + value);
			updatedVector3.Invoke(value);
		}

		private void RaiseUpdatedObject(object value)
		{
//            Debug.Log("RaiseUpdatedObject " + value);
			var objectEvent = new ObjectEvent(value, outputType);
			updatedObject.Invoke(objectEvent);
		}

		public void SubscribeRaiseUpdate(EventInfo eventInfo, NeuralNode node)
		{
			var type         = eventInfo.EventHandlerType;
			var passingTypes = eventInfo.EventHandlerType.GetGenericArguments();
			var methodName   = "";
			if (passingTypes.Length == 0)
				Debug.LogError(eventInfo + " has no passing type.");
			else if (passingTypes[0] == typeof(float))
				methodName = "RaiseUpdatedFloat";
			else if (passingTypes[0] == typeof(Vector3))
				methodName = "RaiseUpdatedVector3";
			else
				methodName = "RaiseUpdatedObject";

			var method = typeof(GraphOutput).GetMethod(methodName,
													   BindingFlags.Public   |
													   BindingFlags.Instance |
													   BindingFlags.NonPublic);
			var handler = Delegate.CreateDelegate(type, this, method);
			eventInfo.AddEventHandler(node, handler);
		}
	}

	[Serializable]
	public struct ObjectEvent
	{
		public readonly object ObjectValue;
		public readonly Type   ObjectType;

		public ObjectEvent(object objectValue, Type objectType)
		{
			ObjectValue = objectValue;
			ObjectType  = objectType;
		}

		public T TypedValue<T>()
		{
			return (T) Convert.ChangeType(ObjectValue, ObjectType);
		}
	}

	[Serializable]
	public class ObjectUnityEvent : UnityEvent<ObjectEvent>
	{
	}

	[Serializable]
	public class FloatUnityEvent : UnityEvent<float>
	{
	}

	[Serializable]
	public class Vector3UnityEvent : UnityEvent<Vector3>
	{
	}
}