﻿using System;
using System.Collections.Generic;
using System.Linq;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Editor.Ports;
using NeuralNodes.Extensions;

using UnityEditor;
using UnityEditor.Experimental.GraphView;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor
{
	/// <summary>
	///     Root Visual element which takes up entire editor window. Every other visual element is a child
	///     of this.
	/// </summary>
	public class NeuralGraphEditorView : VisualElement
	{
		private readonly EdgeConnectorListener _edgeConnectorListener;

		private readonly List<SerializedEdge>    _foundEdges = new List<SerializedEdge>();
		private readonly NeuralGraphEditorObject _neuralGraphEditorObject;
		private readonly SearchWindowProvider    _searchWindowProvider;
		private          NeuralGraphEditorWindow _editorWindow;
		private          bool                    _reloadGraph;

		public NeuralGraphEditorView(
			NeuralGraphEditorWindow editorWindow,
			NeuralGraphEditorObject neuralGraphEditorObject
		)
		{
			Debug.Log(neuralGraphEditorObject.GetInstanceID());
			_editorWindow                         =  editorWindow;
			_neuralGraphEditorObject              =  neuralGraphEditorObject;
			_neuralGraphEditorObject.Deserialized += NeuralGraphEditorDataOnDeserialized;

			this.LoadAndAddStyleSheet("Styles/NeuralGraphEditorView");

			var toolbar = new IMGUIContainer(() =>
											 {
												 GUILayout.BeginHorizontal(EditorStyles.toolbar);
												 if (GUILayout.Button("Save Asset", EditorStyles.toolbarButton))
													 SaveRequested?.Invoke();

												 GUILayout.Space(6);
												 if (GUILayout.Button("Show In Project", EditorStyles.toolbarButton))
													 ShowInProjectRequested?.Invoke();

												 GUILayout.FlexibleSpace();
												 GUILayout.EndHorizontal();
											 });
			Add(toolbar);

			var content = new VisualElement {name = "content"};
			{
				NeuralGraphView = new NeuralGraphView(_neuralGraphEditorObject)
								  {
									  name = "GraphView"

//                    persistenceKey = "NeuralGraphView",
								  };

				NeuralGraphView.SetupZoom(0.05f, ContentZoomer.DefaultMaxScale);
				NeuralGraphView.AddManipulator(new ContentDragger());
				NeuralGraphView.AddManipulator(new SelectionDragger());
				NeuralGraphView.AddManipulator(new RectangleSelector());
				NeuralGraphView.AddManipulator(new ClickSelector());
				NeuralGraphView.RegisterCallback<KeyDownEvent>(KeyDown);
				content.Add(NeuralGraphView);

				NeuralGraphView.graphViewChanged = GraphViewChanged;
			}

			_searchWindowProvider = ScriptableObject.CreateInstance<SearchWindowProvider>();
			_searchWindowProvider.Initialize(editorWindow, this, NeuralGraphView);

			_edgeConnectorListener = new EdgeConnectorListener(this, _searchWindowProvider);

			NeuralGraphView.nodeCreationRequest = c =>
												  {
													  _searchWindowProvider.ConnectedNeuralPort = null;
													  SearchWindow.Open(new SearchWindowContext(c.screenMousePosition),
																		_searchWindowProvider);
												  };

			LoadElements();

			Add(content);
		}

		public Action SaveRequested { get; set; }

		public Action ShowInProjectRequested { get; set; }

		public NeuralGraphView NeuralGraphView { get; }

		private void LoadElements()
		{
			var errorCorrected = false;

			for (var i = _neuralGraphEditorObject.NeuralGraphData.SerializedNodes.Count - 1; i > -1; --i)
				AddNode(_neuralGraphEditorObject.NeuralGraphData.SerializedNodes[i]);

			for (var i = _neuralGraphEditorObject.NeuralGraphData.SerializedInputNodes.Count - 1; i > -1; --i)
				AddNode(_neuralGraphEditorObject.NeuralGraphData.SerializedInputNodes[i]);

			for (var i = _neuralGraphEditorObject.NeuralGraphData.SerializedOutputNodes.Count - 1; i > -1; --i)
				AddNode(_neuralGraphEditorObject.NeuralGraphData.SerializedOutputNodes[i]);

			for (var i = _neuralGraphEditorObject.NeuralGraphData.SerializedEdges.Count - 1; i > -1; --i)
				if (!AddEdge(_neuralGraphEditorObject.NeuralGraphData.SerializedEdges[i]))
				{
					Debug.Log("Removing erroneous edge.");
					_neuralGraphEditorObject.NeuralGraphData.SerializedEdges.RemoveAt(i);
					errorCorrected = true;
				}

			if (errorCorrected)
				SaveRequested?.Invoke();
		}


		public void HandleGraphChanges()
		{
			foreach (var nodeView in NeuralGraphView.nodes.ToList().OfType<NeuralNodeView>())
				nodeView.OnModified(ModificationScope.Topological);
			if (_reloadGraph)
			{
				_reloadGraph = false;

				foreach (var nodeView in NeuralGraphView.nodes.ToList())
				{
					Debug.Log("removing node " + nodeView);
					NeuralGraphView.RemoveElement(nodeView);
				}

				foreach (var edge in NeuralGraphView.edges.ToList())
				{
					Debug.Log("removing edge " + edge);
					NeuralGraphView.RemoveElement(edge);
				}

				LoadElements();
			}
		}

		private void OnNodeChanged(AbstractNeuralNodeEditor inNode, ModificationScope scope)
		{
			if (NeuralGraphView == null)
				return;

			var dependentNodes = new List<AbstractNeuralNodeEditor>();
			NodeUtils.CollectNodesNodeFeedsInto(dependentNodes, inNode);
			foreach (var node in dependentNodes)
			{
				var theViews   = NeuralGraphView.nodes.ToList().OfType<NeuralNodeView>();
				var viewsFound = theViews.Where(x => x.NeuralNodeEditor.NodeGuid == node.NodeGuid).ToList();
				foreach (var drawableNodeData in viewsFound)
					drawableNodeData.OnModified(scope);
			}
		}


		private void NeuralGraphEditorDataOnDeserialized()
		{
			Debug.Log("GraphOnDeserialized");

			//comes after GraphData was undone, so reload graph
			_reloadGraph = true;
		}

		private GraphViewChange GraphViewChanged(GraphViewChange graphViewChange)
		{
			Debug.Log($"GraphViewChanged {graphViewChange}");

			if (graphViewChange.edgesToCreate != null)
				Debug.Log("EDGES TO CREATE " + graphViewChange.edgesToCreate.Count);

			if (graphViewChange.movedElements != null)
			{
				Debug.Log("Moved elements " + graphViewChange.movedElements.Count);
				_neuralGraphEditorObject.RegisterCompleteObjectUndo("Graph Element Moved.");
				foreach (var element in graphViewChange.movedElements)
				{
					var neuralNodeEditor = element.userData as AbstractNeuralNodeEditor;
					neuralNodeEditor.Position            = element.GetPosition().position;
					neuralNodeEditor.SerializedNode.json = JsonUtility.ToJson(neuralNodeEditor);
				}
			}

			if (graphViewChange.elementsToRemove != null)
			{
				Debug.Log("Elements to remove" + graphViewChange.elementsToRemove.Count);
				_neuralGraphEditorObject.RegisterCompleteObjectUndo("Deleted Graph Elements.");

				foreach (var nodeView in graphViewChange.elementsToRemove.OfType<NeuralNodeView>())
				{
					_neuralGraphEditorObject.NeuralGraphData.SerializedNodes.Remove(nodeView.NeuralNodeEditor
																							.SerializedNode);
					_neuralGraphEditorObject.NeuralGraphData.SerializedInputNodes.Remove(nodeView.NeuralNodeEditor
																								 .SerializedNode);
					_neuralGraphEditorObject.NeuralGraphData.SerializedOutputNodes.Remove(nodeView.NeuralNodeEditor
																								  .SerializedNode);
				}

				foreach (var edge in graphViewChange.elementsToRemove.OfType<Edge>())
					_neuralGraphEditorObject.NeuralGraphData.SerializedEdges.Remove(edge.userData as SerializedEdge);
			}

			return graphViewChange;
		}


		private void KeyDown(KeyDownEvent evt)
		{
			if (evt.keyCode == KeyCode.Space && !evt.shiftKey && !evt.altKey && !evt.ctrlKey && !evt.commandKey)
			{
			}
			else if (evt.keyCode == KeyCode.F1)
			{
			}
		}

		public SerializedNode AddNode(AbstractNeuralNodeEditor neuralNodeEditor)
		{
			_neuralGraphEditorObject.RegisterCompleteObjectUndo("Add Node " + neuralNodeEditor.NodeType());

			var serializedNode = new SerializedNode
								 {
									 nodeType = neuralNodeEditor.NodeType(), json = JsonUtility.ToJson(neuralNodeEditor)
								 };

			neuralNodeEditor.SerializedNode = serializedNode;
			if (neuralNodeEditor is IInputNode)
				_neuralGraphEditorObject.NeuralGraphData.SerializedInputNodes.Add(serializedNode);
			else if (neuralNodeEditor is IOutputNode)
				_neuralGraphEditorObject.NeuralGraphData.SerializedOutputNodes.Add(serializedNode);
			else
				_neuralGraphEditorObject.NeuralGraphData.SerializedNodes.Add(serializedNode);

			neuralNodeEditor.Owner = NeuralGraphView;
			var nodeView = new NeuralNodeView {userData = neuralNodeEditor};
			NeuralGraphView.AddElement(nodeView);
			nodeView.Initialize(neuralNodeEditor, _edgeConnectorListener);
			nodeView.MarkDirtyRepaint();
			return serializedNode;
		}

		private void AddNode(SerializedNode serializedNode)
		{
			AbstractNeuralNodeEditor neuralNodeEditor = null;
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			foreach (var type in assembly.GetTypes())
				if (type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(AbstractNeuralNodeEditor)))
				{
					var attrs = type.GetCustomAttributes(typeof(NodeEditorType), false) as NodeEditorType[];
					if (attrs != null && attrs.Length > 0)
						if (attrs[0].NodeType.Name == serializedNode.nodeType)
							neuralNodeEditor = (AbstractNeuralNodeEditor) Activator.CreateInstance(type);
				}

			if (neuralNodeEditor != null)
			{
				JsonUtility.FromJsonOverwrite(serializedNode.json, neuralNodeEditor);
				neuralNodeEditor.SerializedNode = serializedNode;
				neuralNodeEditor.Owner          = NeuralGraphView;
				var nodeView = new NeuralNodeView {userData = neuralNodeEditor};
				NeuralGraphView.AddElement(nodeView);
				nodeView.Initialize(neuralNodeEditor, _edgeConnectorListener);
				nodeView.MarkDirtyRepaint();
			}
			else
			{
				Debug.LogWarning("No NodeEditor found for " + serializedNode);
			}
		}

		public void RemoveEdgesConnectedTo(NeuralPort neuralPort)
		{
			_foundEdges.Clear();
			_neuralGraphEditorObject.NeuralGraphData.GetEdges(neuralPort.Slot.Owner.NodeGuid,
															  neuralPort.Slot.MemberName,
															  _foundEdges);
			for (var i = 0; i < _foundEdges.Count; ++i)
				RemoveEdge(_foundEdges[i]);
		}

		public void RemoveEdge(SerializedEdge serializedEdge)
		{
			_neuralGraphEditorObject.NeuralGraphData.SerializedEdges.Remove(serializedEdge);
			foreach (var edge in NeuralGraphView.edges.ToList())
				if (edge.userData == serializedEdge)
				{
					Debug.Log("removing edge " + edge);
					NeuralGraphView.RemoveElement(edge);
				}
		}

		public void AddEdge(Edge edgeView)
		{
			edgeView.GetSlots(out var leftNeuralSlot, out var rightNeuralSlot);

			_neuralGraphEditorObject.RegisterCompleteObjectUndo("Connect Edge");
			var serializedEdge = new SerializedEdge
								 {
									 sourceNodeGuid   = leftNeuralSlot.Owner.NodeGuid,
									 sourceMemberName = leftNeuralSlot.MemberName,
									 targetNodeGuid   = rightNeuralSlot.Owner.NodeGuid,
									 targetMemberName = rightNeuralSlot.MemberName
								 };

			_neuralGraphEditorObject.NeuralGraphData.SerializedEdges.Add(serializedEdge);

			edgeView.userData = serializedEdge;
			edgeView.output.Connect(edgeView);
			edgeView.input.Connect(edgeView);
			NeuralGraphView.AddElement(edgeView);
		}

		public void AddEdge(NeuralSlot leftNeuralSlot, NeuralSlot rightNeuralSlot)
		{
			var serializedEdge = new SerializedEdge
								 {
									 sourceNodeGuid   = leftNeuralSlot.Owner.NodeGuid,
									 sourceMemberName = leftNeuralSlot.MemberName,
									 targetNodeGuid   = rightNeuralSlot.Owner.NodeGuid,
									 targetMemberName = rightNeuralSlot.MemberName
								 };

			_neuralGraphEditorObject.NeuralGraphData.SerializedEdges.Add(serializedEdge);

			AddEdge(serializedEdge);
		}

		public bool AddEdge(SerializedEdge serializedEdge)
		{
			var sourceNodeView = NeuralGraphView.nodes.ToList().OfType<NeuralNodeView>()
												.FirstOrDefault(x => x.NeuralNodeEditor.NodeGuid ==
																	 serializedEdge.sourceNodeGuid);
			if (sourceNodeView == null)
			{
				Debug.LogWarning($"Source NodeGUID not found {serializedEdge.sourceNodeGuid}");
				return false;
			}

			var sourceAnchor = sourceNodeView.outputContainer.Children().OfType<NeuralPort>()
											 .FirstOrDefault(x => x.Slot.MemberName == serializedEdge.sourceMemberName);
			if (sourceAnchor == null)
			{
				Debug.LogError($"Source anchor null {serializedEdge.sourceMemberName} {serializedEdge.sourceNodeGuid}");
				return false;
			}

			var targetNodeView = NeuralGraphView.nodes.ToList().OfType<NeuralNodeView>()
												.FirstOrDefault(x => x.NeuralNodeEditor.NodeGuid ==
																	 serializedEdge.targetNodeGuid);
			if (targetNodeView == null)
			{
				Debug.LogWarning($"Target NodeGUID not found {serializedEdge.targetNodeGuid}");
				return false;
			}

			var targetAnchor = targetNodeView.inputContainer.Children().OfType<NeuralPort>()
											 .FirstOrDefault(x => x.Slot.MemberName == serializedEdge.targetMemberName);
			if (targetAnchor == null)
			{
				Debug.LogError($"Target anchor null {serializedEdge.sourceMemberName} {serializedEdge.targetNodeGuid}");
				return false;
			}

			var edgeView = new Edge {userData = serializedEdge, output = sourceAnchor, input = targetAnchor};
			edgeView.output.Connect(edgeView);
			edgeView.input.Connect(edgeView);
			NeuralGraphView.AddElement(edgeView);
			targetNodeView.UpdatePortInputVisibilities();
			sourceNodeView.UpdatePortInputVisibilities();

			return true;
		}
	}
}