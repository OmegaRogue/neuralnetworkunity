﻿using UnityEngine;

namespace Data
{
	[CreateAssetMenu(fileName = "NeuralData", menuName = "Neural Data Storage", order = 0)]
	public class NeuralData : ScriptableObject
	{
		public float[][]   inWeights;
		public float[][][] hiddenWeights;
	}
}