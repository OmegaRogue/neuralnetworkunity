﻿using System;
using System.Collections.Generic;
using System.Reflection;

using NeuralNodes.NeuralNodes;

using UnityEngine;

namespace NeuralNodes
{
    public class NeuralGraphObject : ScriptableObject
    {
        [SerializeField] private NeuralGraphData neuralGraphData;

        public NeuralGraphData GraphData
        {
            get { return neuralGraphData; }
        }

        public void Initialize(NeuralGraphData neuralGraphData)
        {
            this.neuralGraphData = neuralGraphData;
        }

        /// <summary>
        /// Creates instances, and connects together the instances, of a LogicGraph.
        /// Will flush the previously saved instances in the process. TODO investigate if any previous instances can be recycled
        /// </summary>
        public void LoadLogicNodeGraph(List<NeuralNode> nodes, List<NeuralNode> inputNodes, List<NeuralNode> outputNodes)
        {
            if (GraphData == null)
                return;

            nodes.Clear();
            for (var i = 0; i < GraphData.SerializedNodes.Count; ++i)
            {
                var node = CreateLogicNodeFromSerializedNode(GraphData.SerializedNodes[i]);
                Debug.Log("Adding node " + node);
                if (node != null)
                {
                    nodes.Add(node);
                }
            }

            inputNodes.Clear();
            Debug.Log(GraphData.SerializedInputNodes.Count);
            for (var i = 0; i < GraphData.SerializedInputNodes.Count; ++i)
            {
                var node = CreateLogicNodeFromSerializedNode(GraphData.SerializedInputNodes[i]);
                Debug.Log("Adding node " + node);
                if (node != null)
                {
                    inputNodes.Add(node);
                }
            }

            outputNodes.Clear();
            for (var i = 0; i < GraphData.SerializedOutputNodes.Count; ++i)
            {
                var node = CreateLogicNodeFromSerializedNode(GraphData.SerializedOutputNodes[i]);
                Debug.Log("Adding node " + node);
                if (node != null)
                {
                    outputNodes.Add(node);
                }
            }

            foreach (var serializedEdge in GraphData.SerializedEdges)
            {
                var sourceNode = FindNodeByGuid(serializedEdge.sourceNodeGuid, nodes, inputNodes, outputNodes);
                if (sourceNode == null)
                {
                    Debug.LogWarning($"Source node is null for edge. SourceNodeGuid: {serializedEdge.sourceNodeGuid} SourceMemberName: {serializedEdge.sourceMemberName}");
                    continue;
                }

                var targetNode = FindNodeByGuid(serializedEdge.targetNodeGuid, nodes, inputNodes, outputNodes);
                if (targetNode == null)
                {
                    Debug.LogWarning($"Target node is null for edge. TargetNodeGuid: {serializedEdge.targetNodeGuid} TargetMemberName: {serializedEdge.targetMemberName}");
                    continue;
                }

                var targetMethodInfo = MethodInfoByName(targetNode, serializedEdge.targetMemberName);
                if (targetMethodInfo == null)
                {
                    Debug.LogWarning($"Target method is null for edge. TargetNodeGuid: {serializedEdge.targetNodeGuid} TargetMemberName: {serializedEdge.targetMemberName}");
                    continue;
                }

                SubscribeToEventByName(sourceNode, serializedEdge.sourceMemberName, targetNode, targetMethodInfo);
            }
        }

        private MethodInfo MethodInfoByName(NeuralNode node, string memberName)
        {
//            Debug.Log($"Finding method {memberName} on {node.GetType()}");
            var methods = node.GetType()
                .GetMethods(BindingFlags.Public |
                            BindingFlags.NonPublic |
                            BindingFlags.Instance);
            foreach (var method in methods)
            {
//                    Debug.Log(method.Name);
                if (method.Name == memberName)
                {
                    return method;
                }
            }

            return null;
        }

        private void SubscribeToEventByName(
            NeuralNode sourceNode,
            string memberName,
            NeuralNode targetNode,
            MethodInfo targetMethodInfo)
        {
            try
            {
                var events = sourceNode.GetType()
                    .GetEvents(BindingFlags.Public |
                               BindingFlags.NonPublic |
                               BindingFlags.Instance);
                foreach (var eventInfo in events)
                {
                    if (eventInfo.Name == memberName)
                    {
                        var type = eventInfo.EventHandlerType;
                        var handler = Delegate.CreateDelegate(type, targetNode, targetMethodInfo);
                        eventInfo.AddEventHandler(sourceNode, handler);
                        return;
                    }
                }
            }
            catch (ArgumentException e)
            {
                Debug.LogError($"Subscribing to event {memberName} on {sourceNode.GetType()} failed. {e.Message}");
            }
        }

        private NeuralNode FindNodeByGuid(string guid, params List<NeuralNode>[] lists)
        {
            foreach (var list in lists)
            {
                var node = list.Find(n => n.NodeGuid == guid);
                if (node != null) return node;
            }

            return null;
        }

        private NeuralNode CreateLogicNodeFromSerializedNode(SerializedNode serializedNode)
        {
            Debug.Log("Created node " + serializedNode.nodeType);
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(NeuralNode)))
                    {
                        if (type.Name == serializedNode.nodeType)
                        {
                            return JsonUtility.FromJson(serializedNode.json, type) as NeuralNode;
                        }
                    }
                }
            }

            Debug.LogError("Failed to create node " + serializedNode.nodeType);
            return null;
        }
    }
}