﻿using System;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Extensions;

using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
	[Serializable]
	public class FloatSlot : NeuralSlot
	{
		private readonly Func<float>   _get;
		private readonly string[]      _labels;
		private readonly Action<float> _set;

		public FloatSlot(
			int                      id,
			AbstractNeuralNodeEditor owner,
			string                   memberName,
			string                   displayName,
			SlotDirection            direction,
			string[]                 labels,
			Func<float>              get,
			Action<float>            set
		) : base(id, owner, memberName, displayName, direction)
		{
			_labels = labels;
			_get    = get;
			_set    = set;
		}

		public override SlotValueType ValueType
		{
			get { return SlotValueType.Vector1; }
		}

		public override bool IsCompatibleWithInputSlotType(SlotValueType inputType)
		{
			return inputType == SlotValueType.Vector1;
		}

		public override VisualElement InstantiateControl()
		{
			return new MultiFloatSlotControlView(Owner,
												 _labels,
												 _get.AsVector4(),
												 _set.AsVector4());
		}
	}
}