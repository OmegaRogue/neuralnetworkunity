﻿using System;

using UnityEngine;

namespace NeuralNodes.Extensions
{
	public static class FuncExtensions
	{
		public static Func<Vector4> AsVector4(this Func<float> func)
		{
			return () => new Vector4(func.Invoke(), 0);
		}

		public static Action<Vector4> AsVector4(this Action<float> action)
		{
			return vector4 => action.Invoke(vector4.x);
		}
	}
}