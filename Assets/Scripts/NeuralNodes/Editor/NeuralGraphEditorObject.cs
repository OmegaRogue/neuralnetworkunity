﻿using System;

using UnityEngine;

namespace NeuralNodes.Editor
{
	/// <summary>
	/// ScriptableObject to hold NeuralGraphData to recieve serialize callbacks and undo.
	/// </summary>
	public class NeuralGraphEditorObject : ScriptableObject, ISerializationCallbackReceiver
	{
		[SerializeField]
		private NeuralGraphData neuralGraphData;
		
		public event Action Deserialized;
		
		public NeuralGraphData NeuralGraphData
		{
			get { return neuralGraphData; }
		}

		public void Initialize(NeuralGraphData neuralGraphData)
		{
			this.neuralGraphData = neuralGraphData;
			if (this.neuralGraphData == null)
			{
				this.neuralGraphData = new NeuralGraphData();
			}
		}
		
		public void RegisterCompleteObjectUndo(string name)
		{
#if UNITY_EDITOR
			UnityEditor.Undo.RegisterCompleteObjectUndo(this, name);
#endif
		}
		
		public void OnBeforeSerialize()
		{
//			Debug.Log("OnBeforeSerialize");
		}

		public void OnAfterDeserialize()
		{
//			Debug.Log("OnAfterDeserialize");
			if (Deserialized != null) Deserialized();
		}
	}
}
