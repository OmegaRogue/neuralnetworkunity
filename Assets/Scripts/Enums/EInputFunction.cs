﻿namespace Enums
{
	public enum EInputFunction
	{
		And, 
		Difference, 
		EuclideanRbf, 
		Max, 
		Min, 
		Or, 
		Product, 
		Sum, 
		SumSqr, 
		WeightedSum
	}
}