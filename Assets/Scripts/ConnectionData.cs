﻿using System;

/// <summary>
///     Serializable compact Form of the Data of a Synapse
/// </summary>
[Serializable]
public struct ConnectionData
{
	public float input;
	public float weight;

	public float WeightedInput
	{
		get { return weight * input; }
	}

	public float Difference
	{
		get { return input - weight; }
	}
}