﻿namespace NeuralNodes.Types
{
    [System.Serializable]
    public struct Bool3
    {
        public bool x;
        public bool y;
        public bool z;

        public Bool3(bool x)
        {
            this.x = x;
            y = false;
            z = false;
        }

        public Bool3(bool x, bool y)
        {
            this.x = x;
            this.y = y;
            z = false;
        }

        public Bool3(bool x, bool y, bool z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}