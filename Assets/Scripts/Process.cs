﻿using System;

using UnityEngine;

public delegate bool CheckForDone();

public class Process
{
	private bool _done;
	private bool _doneEvent;
	private int  _limit;

	private bool _limited;

	private int          _limitTimer;
	private int          _specialTimer;
	private bool         _started;
	public  Action       Callback;
	public  CheckForDone CheckForDoneEvent;

	public Action CompleteCallback;
	public Action SpecialCallback;

	public int    SpecialInterval;
	public float  Started;
	public float  TimeSinceStarted;
	public Action Work;

	public Process(int specialInterval, bool limited = false, int limit = 0, bool doneEvent = false)
	{
		_limit          = limit;
		_limited        = limited;
		_limitTimer     = 0;
		SpecialInterval = specialInterval;
		Started         = Time.time;
		_specialTimer   = 0;
		_done           = false;
		_doneEvent      = doneEvent;
	}

	public void Reset(int specialInterval, bool limited = false, int limit = 0, bool doneEvent = false)
	{
		_limit          = limit;
		_limited        = limited;
		_limitTimer     = 0;
		SpecialInterval = specialInterval;
		Started         = Time.time;
		_specialTimer   = 0;
		_done           = false;
		_doneEvent      = doneEvent;
		_started        = false;
	}

	public void Start()
	{
		_started = true;
	}

	private void CallbackWrapper()
	{
		if (_specialTimer >= SpecialInterval)
		{
			SpecialCallback.Invoke();
			_specialTimer = 0;
			_limitTimer++;
			if (_limited && _limitTimer == _limit)
			{
				_done = true;
				CompleteCallback.Invoke();
			}

			if (_doneEvent && CheckForDoneEvent.Invoke())
			{
				_done = true;
				CompleteCallback.Invoke();
			}
		}

		_specialTimer++;
		Callback.Invoke();
	}

	public void DoWork()
	{
		if (!_started)
			return;
		if (_done)
			return;
		TimeSinceStarted = Time.time - Started;
		try
		{
			Work.Invoke();
			CallbackWrapper();
		}
		catch
		{
			// ignored
		}
	}
}