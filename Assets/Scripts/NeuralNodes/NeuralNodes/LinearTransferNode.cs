﻿using System;

namespace NeuralNodes.NeuralNodes
{
	public class LinearTransferNode : NeuralNode
	{
		private float              _value;
		public event Action<float> FunctionOutput;

		public void Vector1Input(float value)
		{
			// Debug.Log("LinearTransferNode SetValue " + value);
			_value = value;
			FunctionOutput?.Invoke(_value);
		}
	}
}