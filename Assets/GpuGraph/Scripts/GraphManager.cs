﻿// © 2016 BRANISLV GRUJIC ALL RIGHTS RESERVED
// Provided AS IS
// For any official support, please use the contact on the unity asset store

using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

namespace GpuGraph.Scripts
{
    public class GraphManager : MonoBehaviour
    {
        public static  GraphManagerInstance Graph;
        private static GameObject           _graphManagerUi;
        private static GameObject           _graphsUi;

        private Material _mGraphMaterial;

        private static bool IsInVr()
        {
            return UnityEngine.XR.XRDevice.isPresent;
        }

        public class Matrix4X4Wrapper
        {
            public Matrix4X4Wrapper()
            {
                Rotation = Quaternion.Euler(0, 0, 0);
                Trs      = Matrix4x4.TRS(Translation, Rotation, Scale);
            }

            public Matrix4X4Wrapper(Vector3 translation, Quaternion rotation, Vector3 scale)
            {
                Translation = translation;
                Rotation    = rotation;
                Scale       = scale;

                Trs = Matrix4x4.TRS(translation, rotation, scale);
            }

            public Vector3    Translation;
            public Quaternion Rotation;
            public Vector3    Scale;

            public Matrix4x4 Trs;
        }

        private static void DrawRectangle(Vector4 rect, bool isWorldSpace, Matrix4x4 trs, bool invertCoordinate)
        {
            GL.Color(Color.white);

            // input rectangle 
            // the coordinates are top left is 0,0
            var topL = new Vector3(rect.x,          rect.y, 0.0f);
            var topR = new Vector3(rect.x + rect.z, rect.y, 0.0f);

            var bottomL = new Vector3(rect.x, rect.y + rect.w, 0.0f);
            var bottomR = new Vector3(rect.x         + rect.z, rect.y + rect.w, 0.0f);

            if (isWorldSpace)
            {
                topL = trs.MultiplyPoint(topL);
                topR = trs.MultiplyPoint(topR);

                bottomL = trs.MultiplyPoint(bottomL);
                bottomR = trs.MultiplyPoint(bottomR);
            }

            if (isWorldSpace)
            {
                invertCoordinate = false;
            }

            DrawLine(topL,    topR,    invertCoordinate);
            DrawLine(bottomL, bottomR, invertCoordinate);

            DrawLine(topL, bottomL, invertCoordinate);
            DrawLine(topR, bottomR, invertCoordinate);
        }

        private static void DrawLine(Vector3 a, Vector3 b, bool invertCoordinate)
        {
            if (invertCoordinate)
            {
                // flip the y coordinate
                // opengl is bottom left is 0,0
                a.y = 1.0f - a.y;
                b.y = 1.0f - b.y;
            }

            GL.Vertex(a);
            GL.Vertex(b);
        }

        private static void StartRender(Material material, bool isWorldSpace)
        {
            material.SetPass(0);

            if (isWorldSpace == false)
                GL.LoadOrtho();

            GL.Begin(GL.LINES);
        }

        private static void EndRender()
        {
            GL.End();
        }

        private class GpuDataPair
        {
            public GpuDataPair()
            {
                Value = 0.0f;
                Color = Color.red;
            }

            public GpuDataPair(float value)
            {
                Value = value;
                Color = Color.red;
            }

            public GpuDataPair(float value, Color color)
            {
                Value = value;
                Color = color;
            }

            public float Value { get; }
            public Color Color { get; }
        }

        // Single graph container
        public class GpuGraphData
        {
            private GameObject _parentUi;

            private readonly List<GpuDataPair> _dataPairs;

            public string Name;
            // UI

            // WS
            private GameObject    _canvasUi;
            private Canvas        _canvas;
            private RectTransform _canvasRect;

            // Display Current Value
            private GameObject _maxUi;
            private GameObject _minUi;
            private GameObject _avgUi;

            private Text _maxText;
            private Text _minText;
            private Text _avgText;

            private RectTransform _maxTransform;
            private RectTransform _avgTransform;
            private RectTransform _minTransform;

            private bool             IsWorldSpace { get; set; }
            private Matrix4X4Wrapper Trs          { get; set; }

            private int MaxNumPoints { get; }
            private int _prevMaxNumPoints;

            private float MinValue     { get; set; }
            private float MaxValue     { get; set; }
            private float AverageValue { get; set; }
            private float CurrentRange { get; set; }

            private Vector4 Rectangle { get; set; }

            public GpuGraphData()
            {
                _dataPairs        = new List<GpuDataPair>();
                MaxNumPoints      = 400;
                _prevMaxNumPoints = 0;
                Rectangle         = new Vector4(0, 0, 1.0f, 0.2f);
                Name              = "";
                _parentUi         = null;

                ResetDefault();
            }

            private void ResetDefault()
            {
                AverageValue = 0.0f;

                MinValue = float.MaxValue;
                MaxValue = float.MinValue;

                CurrentRange = MaxValue - MinValue;

                IsWorldSpace = false;
            }

            private void ResetUi()
            {
                if (_canvasUi)
                {
                    Destroy(_canvasUi);
                    _canvasUi = null;

                    _canvas     = null;
                    _canvasRect = null;
                }

                if (_maxUi)
                {
                    Destroy(_maxUi);
                    _maxUi   = null;
                    _maxText = null;
                }

                if (_minUi)
                {
                    Destroy(_minUi);
                    _minUi   = null;
                    _minText = null;
                }

                if (_avgUi)
                {
                    Destroy(_avgUi);
                    _avgUi   = null;
                    _avgText = null;
                }
            }

            public void UpdateStats(float dataPoint)
            {
                ResetDefault();

                foreach (var currentDataPoint in _dataPairs.Select(t => t.Value))
                {
                    MinValue = Mathf.Min(MinValue, currentDataPoint);
                    MaxValue = Mathf.Max(MaxValue, currentDataPoint);

                    AverageValue += currentDataPoint;
                }

                AverageValue /= _dataPairs.Count;

                CurrentRange = MaxValue - MinValue;
            }

            private void InitializeParentUi()
            {
                if (_parentUi != null)
                    return;
                if (_graphManagerUi == null || _graphsUi == null)
                    CreateGlobalObjects();

                if (!_graphManagerUi)
                    return;
                foreach (Transform transform in _graphManagerUi.transform)
                    if (string.Equals(transform.gameObject.name, "Graphs_UI"))
                    {
                        _parentUi = transform.gameObject;
                        break;
                    }
            }

            public void UpdateUi()
            {
                InitializeParentUi();

                if (!_parentUi)
                    return;
                if (_canvasUi == null)
                {
                    // we need to ensure this doesnt exist already because script compilation
                    var canvasObjectName = $"{(IsWorldSpace ? "WS" : "SS")}_Canvas_{Name}";

                    _canvasUi = GameObject.Find(canvasObjectName);

                    if (_canvasUi == null)
                    {
                        _canvasUi                  = new GameObject();
                        _canvasUi.transform.parent = _parentUi.transform;
                        _canvasUi.gameObject.name  = canvasObjectName;

                        _canvas = _canvasUi.AddComponent<Canvas>();

                        if (IsWorldSpace)
                            _canvas.renderMode = RenderMode.WorldSpace;
                        else if (IsInVr())
                        {
                            _canvas.renderMode    = RenderMode.ScreenSpaceCamera;
                            _canvas.worldCamera   = Camera.current;
                            _canvas.planeDistance = 126.0f;
                        }
                        else
                            _canvas.renderMode = RenderMode.ScreenSpaceOverlay;

                        _canvasRect           = _canvas.GetComponent<RectTransform>();
                        _canvasRect.anchorMin = new Vector2(0, 0);
                        _canvasRect.anchorMax = new Vector2(0, 0);

                        var canvasScaler = _canvasUi.AddComponent<CanvasScaler>();
                        canvasScaler.dynamicPixelsPerUnit = 5000.0f;

                        _canvasUi.AddComponent<GraphicRaycaster>();
                    }
                    else
                    {
                        _canvas     = _canvasUi.GetComponent<Canvas>();
                        _canvasRect = _canvas.GetComponent<RectTransform>();
                    }
                }

                // Update the position
                if (_canvasRect)
                {
                    _canvasRect.position  = Trs.Translation;
                    _canvasRect.rotation  = Trs.Rotation;
                    _canvasRect.sizeDelta = new Vector2(Trs.Scale.x, Trs.Scale.y);
                }

                Vector2 uiSize;

                uiSize = IsWorldSpace ? new Vector2(0.8f, 0.2f) : new Vector2(100.0f, 20.0f);

                if (_canvasUi == null)
                    return;
                if (_maxUi == null)
                {
                    var maxObjectName = $"{_canvasUi.gameObject.name}_{"Max"}";
                    _maxUi = GameObject.Find(maxObjectName);

                    if (_maxUi == null)
                    {
                        _maxUi                  = new GameObject();
                        _maxUi.transform.parent = _canvasUi.transform;
                        _maxUi.gameObject.name  = maxObjectName;

                        _maxText           = _maxUi.AddComponent<Text>();
                        _maxText.text      = "20.0";
                        _maxText.color     = Color.white;
                        _maxText.font      = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
                        _maxText.alignment = TextAnchor.UpperLeft;
                    }
                    else
                    {
                        _maxText = _maxUi.GetComponent<Text>();
                    }

                    _maxTransform            = _maxText.GetComponent<RectTransform>();
                    _maxTransform.sizeDelta  = uiSize;
                    _maxTransform.localScale = new Vector3(1, 1, 1);
                }

                if(_maxTransform)
                {
                    _maxTransform.localPosition = IsWorldSpace ? new Vector3(0.0f - Trs.Scale.x / 2.0f + _maxTransform.sizeDelta.x / 2.0f, 0.0f + Trs.Scale.y / 2.0f - _maxTransform.sizeDelta.y / 2.0f, 0.0f) : new Vector3(0.0f - Screen.width / 2.0f + _maxTransform.sizeDelta.x / 2.0f + Rectangle.x * Screen.width, Screen.height / 2.0f - _maxTransform.sizeDelta.y / 2.0f - Rectangle.y * Screen.height, 0.0f);
                }

                if (_avgUi == null)
                {
                    var avgObjectName = $"{_canvasUi.gameObject.name}_Avg"; ;
                    _avgUi = GameObject.Find(avgObjectName);

                    if (_avgUi == null)
                    {
                        _avgUi                  = new GameObject();
                        _avgUi.transform.parent = _canvasUi.transform;
                        _avgUi.gameObject.name  = $"{_canvasUi.gameObject.name}_Avg";

                        _avgText           = _avgUi.AddComponent<Text>();
                        _avgText.text      = "0.0f";
                        _avgText.color     = Color.white;
                        _avgText.font      = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
                        _avgText.alignment = TextAnchor.MiddleLeft;
                    }
                    else
                        _avgText = _avgUi.GetComponent<Text>();

                    _avgTransform            = _avgText.GetComponent<RectTransform>();
                    _avgTransform.sizeDelta  = uiSize;
                    _avgTransform.localScale = new Vector3(1, 1, 1);
                }

                if(_avgTransform)
                {
                    _avgTransform.localPosition = IsWorldSpace ? new Vector3(0.0f - Trs.Scale.x / 2.0f + _avgTransform.sizeDelta.x / 2.0f, 0, 0.0f) : new Vector3(0.0f - Screen.width / 2.0f + _avgTransform.sizeDelta.x / 2.0f + Rectangle.x * Screen.width, Screen.height / 2.0f - Rectangle.y * Screen.height - 0.5f * Rectangle.w * Screen.height, 0.0f);
                }

                if (_minUi == null)
                {
                    var minObjectName = $"{_canvasUi.gameObject.name}_Min";
                    _minUi = GameObject.Find(minObjectName);

                    if (_minUi == null)
                    {
                        _minUi                  = new GameObject();
                        _minUi.transform.parent = _canvasUi.transform;
                        _minUi.gameObject.name  = $"{_canvasUi.gameObject.name}_Min";

                        _minText           = _minUi.AddComponent<Text>();
                        _minText.text      = "20.0";
                        _minText.color     = Color.white;
                        _minText.font      = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
                        _minText.alignment = TextAnchor.LowerLeft;
                    }
                    else
                        _minText = _minUi.GetComponent<Text>();

                    _minTransform            = _minText.GetComponent<RectTransform>();
                    _minTransform.sizeDelta  = uiSize;
                    _minTransform.localScale = new Vector3(1, 1, 1);
                }

                if(_minTransform)
                    _minTransform.localPosition = IsWorldSpace
                                                      ? new Vector3(0.0f - Trs.Scale.x / 2.0f +
                                                                    _minTransform.sizeDelta.x / 2.0f,
                                                                    0.0f - Trs.Scale.y / 2.0f +
                                                                    _minTransform.sizeDelta.y / 2.0f,
                                                                    0.0f)
                                                      : new Vector3(0.0f - Screen.width / 2.0f       +
                                                                    _minTransform.sizeDelta.x / 2.0f +
                                                                    Rectangle.x               * Screen.width,
                                                                    Screen.height             / 2.0f +
                                                                    _minTransform.sizeDelta.y / 2.0f   -
                                                                    Rectangle.y        * Screen.height -
                                                                    1.0f * Rectangle.w * Screen.height,
                                                                    0.0f);
            }

            // screens space methods
            public void AddPair(float dataPoint)
            {
                _dataPairs.Add(new GpuDataPair(dataPoint));
                Trs = new Matrix4X4Wrapper();
                Shrink();
                UpdateStats(dataPoint);
                UpdateUi();
            }

            public void AddPair(float dataPoint, Color color)
            {
                _dataPairs.Add(new GpuDataPair(dataPoint, color));
                Trs = new Matrix4X4Wrapper();
                Shrink();
                UpdateStats(dataPoint);
            }

            public void AddPair(float dataPoint, Color color, Rect rectangle)
            {
                // each graph is located in a specify position 
                Rectangle = new Vector4(rectangle.x / Screen.width, rectangle.y / Screen.height, rectangle.width / Screen.width, rectangle.height / Screen.height);

                _dataPairs.Add(new GpuDataPair(dataPoint, color));
                Trs = new Matrix4X4Wrapper();

                Shrink();
                UpdateStats(dataPoint);
                UpdateUi();
            }

            public void AddPair(float dataPoint, Matrix4X4Wrapper trs)
            {
                AddPair(dataPoint);
                IsWorldSpace = true;
                Trs          = trs;
                Rectangle    = new Vector4(0, 0, 1, 1);

                UpdateUi();
            }

            public void AddPair(float dataPoint, Color color, Matrix4X4Wrapper trs)
            {
                AddPair(dataPoint, color);
                IsWorldSpace = true;
                Trs          = trs;
                Rectangle    = new Vector4(0, 0, 1, 1);

                UpdateUi();
            }

            public void Shrink()
            {
                // insert one, remove one, keep count at 100
                if (_dataPairs.Count > MaxNumPoints)
                {
                    _dataPairs.RemoveAt(0);
                }
            }

            public void Reset()
            {
                _dataPairs.RemoveRange(0, _dataPairs.Count - 1);

                ResetDefault();
                ResetUi();
            }

            public void Update()
            {
                if (_prevMaxNumPoints != MaxNumPoints)
                {
                    _prevMaxNumPoints = MaxNumPoints;

                    if (_dataPairs.Count > 0)
                    {
                        Reset();
                    }
                }

                if (_dataPairs.Count > 0)
                {
                }
            }

            public float GetX(int index, float offset)
            {
                var fIndex        = (float)index;
                var fMaxNumPoints = (float)MaxNumPoints - 1.0f;

                return fIndex / fMaxNumPoints + offset;
            }

            public float GetY(int index)
            {
                var currentValue = _dataPairs[index].Value;
                return (currentValue - MinValue) / CurrentRange;
            }

            public void Render(Material graphMaterial)
            {
                if (CurrentRange == 0.0f || _dataPairs.Count <= 1.0f)
                    return;

                StartRender(graphMaterial, IsWorldSpace);

                // This is so that we can see the graph scrolling from right to left
                var screenOffset = _dataPairs.Count != MaxNumPoints ? 1.0f - (float)_dataPairs.Count / (float)MaxNumPoints : 0.0f;

                for (var i = 1; i < _dataPairs.Count; ++i)
                {
                    // Set the color of the specific data pair
                    GL.Color(_dataPairs[i].Color);

                    // Calculate positions
                    var pPos = new Vector3(GetX(i - 1, screenOffset), GetY(i - 1), 0.0f);
                    var cPos = new Vector3(GetX(i - 0, screenOffset), GetY(i - 0), 0.0f);

                    if (IsWorldSpace)
                    {
                        pPos.x -= 0.5f;
                        cPos.x -= 0.5f;

                        pPos.y -= 0.5f;
                        cPos.y -= 0.5f;

                        pPos = Trs.Trs.MultiplyPoint(pPos);
                        cPos = Trs.Trs.MultiplyPoint(cPos);
                    }
                    else
                    {
                        //Clamp to 1.0f, 1.0f
                        cPos = Vector3.Min(Vector3.one, cPos);
                        pPos = Vector3.Min(Vector3.one, pPos);

                        // convert into rectangle space thats provided by user
                        pPos.x *= Rectangle.z;
                        pPos.y *= Rectangle.w;

                        pPos.x += Rectangle.x;
                        pPos.y =  1.0f - Rectangle.y - Rectangle.w + pPos.y;

                        cPos.x *= Rectangle.z;
                        cPos.y *= Rectangle.w;

                        cPos.x += Rectangle.x;
                        cPos.y =  1.0f - Rectangle.y - Rectangle.w + cPos.y;
                    }

                    // Draw the line pair
                    DrawLine(pPos, cPos, false);
                }

                // coordinates here are 0,0 at the bottom, so we need to invert so the square is in the right place
                if (IsWorldSpace)
                {
                    var shiftedRectangle = Rectangle;
                    shiftedRectangle.x -= 0.5f;
                    shiftedRectangle.y -= 0.5f;
                    DrawRectangle(shiftedRectangle, IsWorldSpace, Trs.Trs, true);
                }
                else
                {
                    DrawRectangle(Rectangle, IsWorldSpace, Trs.Trs, true);
                }

                //// Draw the grid for the corresponding graph
                //{
                //    float deltaX = Rectangle.z / 20.0f;

                //    for (float startX = Rectangle.x; startX <= Rectangle.x + Rectangle.z; startX += deltaX)
                //    {
                //        Vector3 t = new Vector3(startX, Rectangle.y, 0.0f);
                //        Vector3 b = new Vector3(startX, Rectangle.y + Rectangle.w, 0.0f);
                //        DrawLine(t, b, true);
                //    }
                //}

                EndRender();

                UpdateText();
            }
            public void UpdateText()
            {
                if (_maxText)
                {
                    _maxText.text = $"{MaxValue}";
                }

                if (_minText)
                {
                    _minText.text = $"{MinValue}";
                }

                if (_avgText)
                {
                    _avgText.text = $"{AverageValue}";
                }
            }
        }

        // Contains the wrapper with all graph
        public class GraphManagerInstance
        {
            public GraphManagerInstance()
            {
                Graphs = new Dictionary<string, GpuGraphData>();
            }

            public void Update()
            {
                foreach (var graph in Graphs)
                {
                    graph.Value.Update();
                }
            }

            public GpuGraphData Retrieve(string key, float value)
            {
                // If we don't have data with that key
                if (!Graphs.TryGetValue(key, out var graphData))
                {
                    // Allocate this graph
                    graphData      = new GpuGraphData();
                    graphData.Name = key;

                    // insert this new graph into dictionary
                    Graphs.Add(key, graphData);
                }

                return graphData;
            }

            public void ResetAll()
            {
                foreach (var graph in Graphs)
                {
                    graph.Value.Reset();
                }

                Graphs.Clear();
            }

            public bool Reset(string key)
            {
                var          wasReset = Graphs.TryGetValue(key, out var graphData);

                // If we don't have data with that key
                if (wasReset)
                {
                    graphData.Reset();
                    Graphs.Remove(key);
                }

                return wasReset;
            }

            // screen space plot
            public void Plot(string key, float value)
            {
                Retrieve(key, value).AddPair(value);
            }

            // world space
            public void Plot(string key, float value, Matrix4X4Wrapper trs)
            {
                Retrieve(key, value).AddPair(value, trs);
            }

            public void Plot(string key, float value, Color color)
            {
                Retrieve(key, value).AddPair(value, color);
            }

            // screen space plot
            public void Plot(string key, float value, Color color, Rect rectangle)
            {
                Retrieve(key, value).AddPair(value, color, rectangle);
            }

            // world space plot
            public void Plot(string key, float value, Color color, Matrix4X4Wrapper trs)
            {
                Retrieve(key, value).AddPair(value, color, trs);
            }

            public Dictionary<string, GpuGraphData> Graphs;
        }

        private static GameObject CreateGlobalObjects()
        {
            if (_graphManagerUi == null)
            {
                _graphManagerUi = GameObject.Find("GraphManager_UI");

                if (_graphManagerUi == null)
                {
                    _graphManagerUi                    = new GameObject();
                    _graphManagerUi.transform.position = new Vector3(0, 0, 0);
                    _graphManagerUi.name               = "GraphManager_UI";
                }
            }

            if (_graphManagerUi && _graphsUi == null)
            {
                _graphsUi = GameObject.Find("Graphs_UI");

                if (_graphsUi == null)
                {
                    // create two children objects as well
                    _graphsUi                  = new GameObject();
                    _graphsUi.transform.parent = _graphManagerUi.transform;
                    _graphsUi.name             = "Graphs_UI";
                }
            }

            return _graphManagerUi;
        }

        // Use this for initialization
        private void Start()
        {
            // Create static instance
            Graph = new GraphManagerInstance();

            // Initialize the default material/shader used for the rendering
            if (!_mGraphMaterial)
            {
                _mGraphMaterial           = new Material(Shader.Find("GPUGraph/Graph"));
                _mGraphMaterial.hideFlags = HideFlags.HideAndDontSave;
            }

            CreateGlobalObjects();
        }

        // Update is called once per frame
        private void Update()
        {
            // Update the graph, this updates min/max/current/avg
            if (Graph != null)
            {
                Graph.Update();
            }
            else
            {
                Graph = new GraphManagerInstance();
                CreateGlobalObjects();
            }
        }

        private void RenderGraph()
        {
            if (Graph != null)
            {
                //GL.PushMatrix();

                foreach (var graph in Graph.Graphs)
                {
                    graph.Value.Render(_mGraphMaterial);
                }

                //GL.PopMatrix();
            }
        }

#if UNITY_EDITOR
        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            if (Graph != null)
            {
                Graph.ResetAll();
            }
        }
#endif

        // Render the graph in viewport
        public void OnPostRender()
        {
            RenderGraph();
        }
    }
}
