﻿using System.IO;

using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.Experimental.AssetImporters;

using UnityEngine;

namespace NeuralNodes.Editor
{
	[CustomEditor(typeof(NeuralGraphImporter))]
	public class NeuralGraphImporterEditor : ScriptedImporterEditor
	{
		public override void OnInspectorGUI()
		{
			if (GUILayout.Button("Open Generic Graph Editor"))
			{
				var importer = target as AssetImporter;
				ShowGraphEditWindow(importer.assetPath);
			}

			DrawDefaultInspector();
			ApplyRevertGUI();
		}

		private static bool ShowGraphEditWindow(string path)
		{
			var guid = AssetDatabase.AssetPathToGUID(path);
			var extension = Path.GetExtension(path);
			if (extension != ".NeuralNetwork" && extension != ".NeuralNetwork")
				return false;

			var foundWindow = false;
			foreach (var w in Resources.FindObjectsOfTypeAll<NeuralGraphEditorWindow>())
			{
				if (w.SelectedGuid == guid)
				{
					foundWindow = true;
					w.Focus();
				}
			}

			if (!foundWindow)
			{
				var window = CreateInstance<NeuralGraphEditorWindow>();
				window.Show();
				window.Initialize(guid);
			}

			return true;
		}

		[OnOpenAsset(0)]
		public static bool OnOpenAsset(int instanceId, int line)
		{
			var path = AssetDatabase.GetAssetPath(instanceId);
			return ShowGraphEditWindow(path);
		}
	}
}