﻿using System;

namespace NeuralNodes
{
	[Serializable]
	public enum SlotValueType
	{
		//TODO should these be strings?
		Vector4,
		Vector3,
		Vector2,
		Vector1,
		Boolean,
		CurvePrimitive,
		Mesh,
		VertexList,
		FunctionOutput
	}
}