﻿using System;

namespace NeuralNodes.Types
{
	[Serializable]
	public struct FunctionOutput
	{
		public float value;
		public float derivative;

		public FunctionOutput(float value, float derivative)
		{
			this.value      = value;
			this.derivative = derivative;
		}

		public FunctionOutput(float value)
		{
			this.value = value;
			derivative = 1;
		}
	}
}