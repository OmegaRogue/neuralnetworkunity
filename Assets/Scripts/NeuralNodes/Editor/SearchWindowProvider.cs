﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using NeuralNodes.Editor.Nodes;
using NeuralNodes.Editor.Ports;

using UnityEditor.Experimental.GraphView;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor
{
    public class SearchWindowProvider : ScriptableObject, ISearchWindowProvider
    {
        private NeuralGraphEditorWindow _editorWindow;
        private NeuralGraphEditorView _neuralGraphEditorView;
        private NeuralGraphView _graphView;
        private Texture2D _mIcon;
        public NeuralPort ConnectedNeuralPort { get; set; }
        public bool NodeNeedsRepositioning { get; set; }
        public Vector2 TargetPosition { get; private set; }

        public void Initialize(NeuralGraphEditorWindow editorWindow, 
            NeuralGraphEditorView neuralGraphEditorView, 
            NeuralGraphView graphView)
        {
            _editorWindow = editorWindow;
            _neuralGraphEditorView = neuralGraphEditorView;
            _graphView = graphView;
            
            // Transparent icon to trick search window into indenting items
            _mIcon = new Texture2D(1, 1);
            _mIcon.SetPixel(0, 0, new Color(0, 0, 0, 0));
            _mIcon.Apply();
        }

        private void OnDestroy()
        {
            if (_mIcon != null)
            {
                DestroyImmediate(_mIcon);
                _mIcon = null;
            }
        }

        private struct NodeEntry
        {
            public string[] Title;
            public AbstractNeuralNodeEditor NeuralNodeEditor;
            public string CompatibleSlotId;
        }

        private List<int> _mIds;
        private readonly List<NeuralSlot> _mSlots = new List<NeuralSlot>();

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            // First build up temporary data structure containing group & title as an array of strings (the last one is the actual title) and associated node type.
            var nodeEntries = new List<NodeEntry>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in GetTypesOrNothing(assembly))
                {
                    if (type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(AbstractNeuralNodeEditor)))
                    {
                        var attrs = type.GetCustomAttributes(typeof(TitleAttribute), false) as TitleAttribute[];
                        if (attrs != null && attrs.Length > 0)
                        {
                            var node = (AbstractNeuralNodeEditor)Activator.CreateInstance(type);
                            AddEntries(node, attrs[0].Title, nodeEntries);
                        }
                    }
                }
            }

            // Sort the entries lexicographically by group then title with the requirement that items always comes before sub-groups in the same group.
            // Example result:
            // - Art/BlendMode
            // - Art/Adjustments/ColorBalance
            // - Art/Adjustments/Contrast
            nodeEntries.Sort((entry1, entry2) =>
            {
                for (var i = 0; i < entry1.Title.Length; i++)
                {
                    if (i >= entry2.Title.Length)
                        return 1;
                    var value = entry1.Title[i].CompareTo(entry2.Title[i]);
                    if (value != 0)
                    {
                        // Make sure that leaves go before nodes
                        if (entry1.Title.Length != entry2.Title.Length && (i == entry1.Title.Length - 1 || i == entry2.Title.Length - 1))
                            return entry1.Title.Length < entry2.Title.Length ? -1 : 1;
                        return value;
                    }
                }
                return 0;
            });

            //* Build up the data structure needed by SearchWindow.

            // `groups` contains the current group path we're in.
            var groups = new List<string>();

            // First item in the tree is the title of the window.
            var tree = new List<SearchTreeEntry>
            {
                new SearchTreeGroupEntry(new GUIContent("Create Node"), 0),
            };

            foreach (var nodeEntry in nodeEntries)
            {
                // `createIndex` represents from where we should add new group entries from the current entry's group path.
                var createIndex = int.MaxValue;

                // Compare the group path of the current entry to the current group path.
                for (var i = 0; i < nodeEntry.Title.Length - 1; i++)
                {
                    var group = nodeEntry.Title[i];
                    if (i >= groups.Count)
                    {
                        // The current group path matches a prefix of the current entry's group path, so we add the
                        // rest of the group path from the currrent entry.
                        createIndex = i;
                        break;
                    }
                    if (groups[i] != group)
                    {
                        // A prefix of the current group path matches a prefix of the current entry's group path,
                        // so we remove everyfrom from the point where it doesn't match anymore, and then add the rest
                        // of the group path from the current entry.
                        groups.RemoveRange(i, groups.Count - i);
                        createIndex = i;
                        break;
                    }
                }

                // Create new group entries as needed.
                // If we don't need to modify the group path, `createIndex` will be `int.MaxValue` and thus the loop won't run.
                for (var i = createIndex; i < nodeEntry.Title.Length - 1; i++)
                {
                    var group = nodeEntry.Title[i];
                    groups.Add(group);
                    tree.Add(new SearchTreeGroupEntry(new GUIContent(group)) { level = i + 1 });
                }

                // Finally, add the actual entry.
                tree.Add(new SearchTreeEntry(new GUIContent(nodeEntry.Title.Last(), _mIcon)) { level = nodeEntry.Title.Length, userData = nodeEntry });
            }

            return tree;
        }

        public static IEnumerable<Type> GetTypesOrNothing(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch
            {
                return Enumerable.Empty<Type>();
            }
        }

        private void AddEntries(AbstractNeuralNodeEditor neuralNodeEditor, string[] title, List<NodeEntry> nodeEntries)
        {
            if (ConnectedNeuralPort == null)
            {
                nodeEntries.Add(new NodeEntry
                {
                    NeuralNodeEditor = neuralNodeEditor,
                    Title = title,
                    CompatibleSlotId = ""
                });
                return;
            }

            var connectedSlot = ConnectedNeuralPort.Slot;
            _mSlots.Clear();
            neuralNodeEditor.GetSlots(_mSlots);
            var hasSingleSlot = _mSlots.Count(s => s.IsOutputSlot != connectedSlot.IsOutputSlot) == 1;
            _mSlots.RemoveAll(slot =>
            {
                var logicSlot = (NeuralSlot)slot;
                return !logicSlot.IsCompatibleWith(connectedSlot);
            });

            if (hasSingleSlot && _mSlots.Count == 1)
            {
                nodeEntries.Add(new NodeEntry
                {
                    NeuralNodeEditor = neuralNodeEditor,
                    Title = title,
                    CompatibleSlotId = _mSlots.First().MemberName
                });
                return;
            }

            foreach (var slot in _mSlots)
            {
                var entryTitle = new string[title.Length];
                title.CopyTo(entryTitle, 0);
                entryTitle[entryTitle.Length - 1] += ": " + slot.DisplayName;
                nodeEntries.Add(new NodeEntry
                {
                    Title = entryTitle,
                    NeuralNodeEditor = neuralNodeEditor,
                    CompatibleSlotId = slot.MemberName
                });
            }
        }

        public bool OnSelectEntry(SearchTreeEntry entry, SearchWindowContext context)
        {
            var nodeEntry = (NodeEntry)entry.userData;
            var nodeEditor = nodeEntry.NeuralNodeEditor;
            
            var windowMousePosition = _editorWindow.rootVisualElement.ChangeCoordinatesTo(_editorWindow.rootVisualElement.parent, context.screenMousePosition - _editorWindow.position.position);
            var graphMousePosition = _graphView.contentViewContainer.WorldToLocal(windowMousePosition);
            nodeEditor.Position = new Vector3(graphMousePosition.x, graphMousePosition.y, 0);
            
            _neuralGraphEditorView.AddNode(nodeEditor);

            if (ConnectedNeuralPort != null)
            {
                var connectedSlotReference = ConnectedNeuralPort.Slot;
                var compatibleSlotReference = nodeEditor.FindOutputSlot<NeuralSlot>(nodeEntry.CompatibleSlotId);

                var fromReference = ConnectedNeuralPort.Slot.IsOutputSlot ? connectedSlotReference : compatibleSlotReference;
                var toReference = ConnectedNeuralPort.Slot.IsOutputSlot ? compatibleSlotReference : connectedSlotReference;

                _neuralGraphEditorView.RemoveEdgesConnectedTo(ConnectedNeuralPort);
                
                _neuralGraphEditorView.AddEdge(fromReference, toReference);
            }

            return true;
        }
    }
}
