﻿using System;
using System.Globalization;

using UnityEditor;
using UnityEngine;

public class Connection
{
	public static float LearningRate;
	public ConnectionPoint    InPoint;
	public ConnectionPoint    OutPoint;
	public Action<Connection> OnClickRemoveConnection;
	
	public float Weight;
	
	public float WeightDelta => -LearningRate * OutPoint.Node.Delta * InPoint.Node.Value.value;

	public float Value => Weight * InPoint.Node.Value.value;
	
	public ConnectionData Data => new ConnectionData {input = InPoint.Node.Value.value, weight = Weight};


	public Connection(ConnectionPoint inPoint, ConnectionPoint outPoint, Action<Connection> onClickRemoveConnection)
	{
		this.InPoint                 = inPoint;
		this.OutPoint                = outPoint;
		this.OnClickRemoveConnection = onClickRemoveConnection;
		InPoint.Connections.Add(this);
		OutPoint.Connections.Add(this);
		Weight = 1;
	}

	public void Draw()
	{
		Handles.DrawBezier(
						   InPoint.Rect.center,
						   OutPoint.Rect.center,
						   InPoint.Rect.center  + Vector2.left * 50f,
						   OutPoint.Rect.center - Vector2.left * 50f,
						   Color.white,
						   null,
						   2f
						  );

		var pos = (InPoint.Rect.center + OutPoint.Rect.center) * 0.5f;
		
		Handles.Label(pos,Weight.ToString(CultureInfo.CurrentCulture), NodeEditorStyles.GetConnectionLabelStyle());
		//
		// if (Handles.Button(pos + new Vector2(4, 4), Quaternion.identity, 8, 12, Handles.RectangleHandleCap))
		// {
		// 	OnClickRemoveConnection?.Invoke(this);
		// }
	}
}