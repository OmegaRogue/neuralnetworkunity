﻿using UnityEngine;

namespace Data
{
	[CreateAssetMenu(fileName = "Dataset", menuName = "Dataset", order = 0)]
	public class DataSet : ScriptableObject
	{
		public int inputs;
		public int outputs;
		public int nDataSet;
		public DataSetEntry[] dataSets;
	}
}