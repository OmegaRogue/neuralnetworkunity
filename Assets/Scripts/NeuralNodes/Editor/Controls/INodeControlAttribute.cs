﻿using System.Reflection;

using NeuralNodes.Editor.Nodes;

using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Controls
{
	public interface INodeControlAttribute
	{
		VisualElement InstantiateControl(AbstractNeuralNodeEditor neuralNodeEditor, PropertyInfo propertyInfo);
	}
}
