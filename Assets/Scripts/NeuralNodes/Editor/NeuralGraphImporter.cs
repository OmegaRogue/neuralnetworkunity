﻿using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEditor;
using UnityEditor.Experimental.AssetImporters;

using UnityEngine;

namespace NeuralNodes.Editor
{
    [ScriptedImporter(13, NeuralGraphImporter.NeuralGraphExtension)]
    public class NeuralGraphImporter : ScriptedImporter
    {
        public const string NeuralGraphExtension = "NeuralNetwork";
        
        public override void OnImportAsset(AssetImportContext ctx)
        {
            var objectList = new List<Object>();
            Debug.Log("MAIN OBJECT  " + ctx.mainObject);
            ctx.GetObjects(objectList);
            Debug.Log(" OBJECTS  " + objectList.Count);

        
            var               textGraph         = File.ReadAllText(ctx.assetPath, Encoding.UTF8);
            var loadedGraphObject = AssetDatabase.LoadAssetAtPath<NeuralGraphObject>(ctx.assetPath);
            if (loadedGraphObject == null)
            {
                Debug.Log("Generating new");
                var               graph             = JsonUtility.FromJson<NeuralGraphData>(textGraph);
                var neuralGraphObject = ScriptableObject.CreateInstance<NeuralGraphObject>();
                neuralGraphObject.Initialize(graph);
                ctx.AddObjectToAsset("MainAsset", neuralGraphObject);
                ctx.SetMainObject(neuralGraphObject);
            }
            else
            {
                Debug.Log("Updating Old");
                JsonUtility.FromJsonOverwrite(textGraph, loadedGraphObject.GraphData);
                ctx.AddObjectToAsset("MainAsset", loadedGraphObject);
                ctx.SetMainObject(loadedGraphObject);
            }
        
            Debug.Log(loadedGraphObject);

//        AssetDatabase.SaveAssets();
//        EditorSceneManager.SaveOpenScenes();
        


            Debug.Log("Set Asset");

//        AssetDatabase.Refresh();
        }
    }
}