﻿using System.Linq;

using NeuralNodes.Editor.Ports;
using NeuralNodes.NeuralNodes;

namespace NeuralNodes.Editor.Nodes
{
    [Title("Input", "Vector3")]
    [NodeEditorType(typeof(Vector3InputNeuralNode))]
    public class Vector3InputNeuralNodeEditor : AbstractNeuralNodeEditor, IInputNode
    {
        private static readonly string[] Labels = {"X", "Y", "Z"};

        public override void ConstructNode()
        {
            var index = GetInputSlots<NeuralSlot>().Count() + 1;
            AddSlot(new VectorNeuralSlot(index,
                                         this,
                                         "Vector3Output",
                                         "Out",
                                         SlotDirection.Output,
                                         Labels,
                                         null,
                                         null));
        }
    }
}