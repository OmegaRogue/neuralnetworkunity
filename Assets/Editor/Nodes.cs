﻿using System;
using System.Globalization;
using System.Linq;

using Enums;

using NeuralNodes.Types;

using UnityEditor;

using UnityEngine;

public class InputNode : NodeOld
{
	public float Input;

	public InputNode(
		Vector2                 position,
		float                   width,
		float                   height,
		GUIStyle                nodeStyle,
		GUIStyle                selectedStyle,
		GUIStyle                inPointStyle,
		GUIStyle                outPointStyle,
		Action<ConnectionPoint> onClickInPoint,
		Action<ConnectionPoint> onClickOutPoint,
		Action<NodeOld>         onClickRemoveNode
	) : base(position,
			 width,
			 height,
			 nodeStyle,
			 selectedStyle,
			 inPointStyle,
			 outPointStyle,
			 onClickInPoint,
			 onClickOutPoint,
			 onClickRemoveNode)
	{
		Title = "Input";
		Type  = ENeuronType.Input;
	}

	public override FunctionOutput CalculateValue()
	{
		return new FunctionOutput {value = Input};
	}

	public override void Draw()
	{
		OutPoint.Draw();
		GUILayout.BeginArea(Rect, Style);
		GUILayout.Label(Title, NodeEditorStyles.GetLabelStyle());
		GUILayout.BeginHorizontal();
		GUILayout.Space(10f);
		Input = EditorGUILayout.FloatField(Input);
		GUILayout.Space(5f);
		GUILayout.EndHorizontal();
		GUILayout.Label(TransferFunction.ToString(), NodeEditorStyles.GetLabelStyle());
		GUILayout.EndArea();
	}
}

public class OutputNode : NodeOld
{
	public OutputNode(
		Vector2                 position,
		float                   width,
		float                   height,
		GUIStyle                nodeStyle,
		GUIStyle                selectedStyle,
		GUIStyle                inPointStyle,
		GUIStyle                outPointStyle,
		Action<ConnectionPoint> onClickInPoint,
		Action<ConnectionPoint> onClickOutPoint,
		Action<NodeOld>         onClickRemoveNode
	) : base(position,
			 width,
			 height,
			 nodeStyle,
			 selectedStyle,
			 inPointStyle,
			 outPointStyle,
			 onClickInPoint,
			 onClickOutPoint,
			 onClickRemoveNode)
	{
		Title = "Output";
		Type  = ENeuronType.Output;
	}

	public override FunctionOutput CalculateValue()
	{
		return NeuralMaths.Transfer[(int) TransferFunction](NeuralMaths.Input
																[
																 (int) InputFunction](InPoint.Connections
																							 .Select(synapse =>
																										 synapse
																											.Data)));
	}

	public override void Draw()
	{
		InPoint.Draw();
		GUILayout.BeginArea(Rect, Style);
		GUILayout.Label(Title,                    NodeEditorStyles.GetLabelStyle());
		GUILayout.Label(InputFunction.ToString(), NodeEditorStyles.GetLabelStyle());

		GUILayout.Space(1f);
		GUILayout.Label(Value.value.ToString(CultureInfo.CurrentCulture), NodeEditorStyles.GetLabelStyle());
		GUILayout.EndArea();
	}
}

public class HiddenNode : NodeOld
{
	public HiddenNode(
		Vector2                 position,
		float                   width,
		float                   height,
		GUIStyle                nodeStyle,
		GUIStyle                selectedStyle,
		GUIStyle                inPointStyle,
		GUIStyle                outPointStyle,
		Action<ConnectionPoint> onClickInPoint,
		Action<ConnectionPoint> onClickOutPoint,
		Action<NodeOld>         onClickRemoveNode
	) : base(position,
			 width,
			 height,
			 nodeStyle,
			 selectedStyle,
			 inPointStyle,
			 outPointStyle,
			 onClickInPoint,
			 onClickOutPoint,
			 onClickRemoveNode)
	{
		Title = "Hidden";
		Type  = ENeuronType.Hidden;
	}


	public override FunctionOutput CalculateValue()
	{
		return NeuralMaths.Transfer[(int) TransferFunction](NeuralMaths.Input
																[
																 (int) InputFunction](InPoint.Connections
																							 .Select(synapse =>
																										 synapse
																											.Data)));
	}

	public override void Draw()
	{
		InPoint.Draw();
		OutPoint.Draw();
		GUILayout.BeginArea(Rect, Style);
		GUILayout.Label(Title,                    NodeEditorStyles.GetLabelStyle());
		GUILayout.Label(InputFunction.ToString(), NodeEditorStyles.GetLabelStyle());

		GUILayout.Label(Value.value.ToString(CultureInfo.CurrentCulture), NodeEditorStyles.GetLabelStyle());
		GUILayout.Label(TransferFunction.ToString(),                      NodeEditorStyles.GetLabelStyle());
		GUILayout.EndArea();
	}
}

public class BiasNode : NodeOld
{
	public BiasNode(
		Vector2                 position,
		float                   width,
		float                   height,
		GUIStyle                nodeStyle,
		GUIStyle                selectedStyle,
		GUIStyle                inPointStyle,
		GUIStyle                outPointStyle,
		Action<ConnectionPoint> onClickInPoint,
		Action<ConnectionPoint> onClickOutPoint,
		Action<NodeOld>         onClickRemoveNode
	) : base(position,
			 width,
			 height,
			 nodeStyle,
			 selectedStyle,
			 inPointStyle,
			 outPointStyle,
			 onClickInPoint,
			 onClickOutPoint,
			 onClickRemoveNode)
	{
		Title = "Bias";
		Type  = ENeuronType.Bias;
	}

	public override FunctionOutput CalculateValue()
	{
		return new FunctionOutput {value = 1};
	}

	public override void Draw()
	{
		OutPoint.Draw();
		GUILayout.BeginArea(Rect, Style);
		GUILayout.Label(Title, NodeEditorStyles.GetLabelStyle());
		GUILayout.Space(1f);
		GUILayout.Label("1",                         NodeEditorStyles.GetLabelStyle());
		GUILayout.Label(TransferFunction.ToString(), NodeEditorStyles.GetLabelStyle());

		GUILayout.EndArea();
	}
}