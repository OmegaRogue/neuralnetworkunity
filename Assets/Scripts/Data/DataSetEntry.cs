﻿using System;

namespace Data
{
	[Serializable]
	public class DataSetEntry
	{
		
		public float[] input = new float[1];
		public float[] output = new float[1];
	}
}