﻿using System;
using System.IO;
using System.Text;

using UnityEditor;

using UnityEngine;
using UnityEngine.UIElements;

using Object = UnityEngine.Object;

namespace NeuralNodes.Editor
{
    /// <summary>
    /// Root Editor Window. Entire system is a subset of this class.
    /// </summary>
    public class NeuralGraphEditorWindow : EditorWindow
    {
        private NeuralGraphEditorObject _neuralGraphEditorObject;

        private NeuralGraphEditorView _graphEditorView;

        private NeuralGraphEditorView NeuralGraphEditorView
        {
            get { return _graphEditorView; }
            set
            {
                if (_graphEditorView != null)
                {
                    _graphEditorView.RemoveFromHierarchy();
                }

                _graphEditorView = value;

                if (_graphEditorView != null)
                {
                    _graphEditorView.SaveRequested += UpdateAsset;
                    _graphEditorView.ShowInProjectRequested += PingAsset;
                    this.rootVisualElement.Add(_graphEditorView);
                }
            }
        }

        public string SelectedGuid { get; private set; }

        public void Initialize(string guid)
        {
            try
            {
                SelectedGuid = guid;
                var asset = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(guid));
                var path = AssetDatabase.GetAssetPath(asset);
                var textGraph = File.ReadAllText(path, Encoding.UTF8);

                _neuralGraphEditorObject = CreateInstance<NeuralGraphEditorObject>();
                var neuralGraphData = JsonUtility.FromJson<NeuralGraphData>(textGraph);
                _neuralGraphEditorObject.Initialize(neuralGraphData);
                NeuralGraphEditorView = new NeuralGraphEditorView(this, _neuralGraphEditorObject)
                {
//                    persistenceKey = _neuralGraphEditorObject.GetInstanceID().ToString()
                };
                NeuralGraphEditorView.RegisterCallback<GeometryChangedEvent>(OnPostLayout);

                titleContent =
                    new
                        GUIContent($"Neural Graph{(_neuralGraphEditorObject.name.Equals("") ? "" : $" - {_neuralGraphEditorObject.name}")}");

                Repaint();
            }
            catch (Exception)
            {
                _graphEditorView = null;
                _neuralGraphEditorObject = null;
                throw;
            }
        }

        private void OnDisable()
        {
            NeuralGraphEditorView = null;
        }

        private void OnDestroy()
        {
            NeuralGraphEditorView = null;
        }

        private void Update()
        {
            NeuralGraphEditorView.HandleGraphChanges();
        }

        private static void PingAsset()
        {
//            if (selectedGuid != null)
//            {
//                var path = AssetDatabase.GUIDToAssetPath(selectedGuid);
//                var asset = AssetDatabase.LoadAssetAtPath<Object>(path);
//                EditorGUIUtility.PingObject(asset);
//            }
        }

        public void UpdateAsset()
        {
            if (SelectedGuid == null || _neuralGraphEditorObject == null)
                return;
            var path = AssetDatabase.GUIDToAssetPath(SelectedGuid);
            if (string.IsNullOrEmpty(path) && _neuralGraphEditorObject.NeuralGraphData != null)
                return;

            var importer = AssetImporter.GetAtPath(path) as NeuralGraphImporter;
            if (importer == null)
                return;
                
            File.WriteAllText(path, EditorJsonUtility.ToJson(_neuralGraphEditorObject.NeuralGraphData, true));
            importer.SaveAndReimport();
//                AssetDatabase.ImportAsset(path);
        }

        private void OnPostLayout(GeometryChangedEvent evt)
        {
            NeuralGraphEditorView.UnregisterCallback<GeometryChangedEvent>(OnPostLayout);
            NeuralGraphEditorView.NeuralGraphView.FrameAll();
        }
    }
}