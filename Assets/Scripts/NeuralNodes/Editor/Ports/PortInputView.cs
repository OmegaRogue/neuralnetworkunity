﻿using System;

using NeuralNodes.Extensions;

using UnityEditor.Experimental.GraphView;

using UnityEngine;
using UnityEngine.UIElements;

namespace NeuralNodes.Editor.Ports
{
    public class PortInputView : GraphElement, IDisposable
    {
        private readonly CustomStyleProperty<Color> _kEdgeColorProperty = new CustomStyleProperty<Color>("--edge-color");

        private Color _edgeColor = Color.red;
       
        public Color EdgeColor
        {
            get { return _edgeColor; }
        }

        public NeuralSlot Description
        {
            get { return _description; }
        }

        private NeuralSlot _description;
        private SlotValueType _valueType;
        private VisualElement _control;
        private readonly VisualElement _container;
        private readonly EdgeControl _edgeControl;

        public PortInputView(NeuralSlot description) 
        {
            this.LoadAndAddStyleSheet("Styles/PortInputView");
            pickingMode = PickingMode.Ignore;
            ClearClassList();
            _description = description;
            _valueType = description.ValueType;
            AddToClassList("type" + _valueType);

            _edgeControl = new EdgeControl
            {
                @from = new Vector2(232f - 21f, 11.5f),
                to    = new Vector2(232f,       11.5f),
                edgeWidth = 2,
                pickingMode = PickingMode.Ignore
            };
            
            Add(_edgeControl);

            _container = new VisualElement { name = "container" };
            {
                _control = this.Description.InstantiateControl();
                if (_control != null)
                    _container.Add(_control);

                var slotElement = new VisualElement { name = "slot" };
                {
                    slotElement.Add(new VisualElement { name = "dot" });
                }
                _container.Add(slotElement);
            }
            Add(_container);

            _container.visible = _edgeControl.visible = _control != null;
            RegisterCallback<CustomStyleResolvedEvent>(OnCustomStyleResolved);
        }

        private void OnCustomStyleResolved(CustomStyleResolvedEvent e)
        {
            if (e.customStyle.TryGetValue(_kEdgeColorProperty, out var colorValue))
                _edgeColor = colorValue;
            
            _edgeControl.UpdateLayout();
            _edgeControl.inputColor = EdgeColor;
            _edgeControl.outputColor = EdgeColor;
        }

        public void UpdateSlot(NeuralSlot newNeuralSlot)
        {
            _description = newNeuralSlot;
            Recreate();
        }

        public void UpdateSlotType()
        {
            if (Description.ValueType != _valueType)
                Recreate();
        }

        private void Recreate()
        {
            RemoveFromClassList("type" + _valueType);
            _valueType = Description.ValueType;
            AddToClassList("type" + _valueType);
            if (_control != null)
            {
                var disposable = _control as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
                _container.Remove(_control);
            }
            _control = Description.InstantiateControl();
            if (_control != null)
                _container.Insert(0, _control);

            _container.visible = _edgeControl.visible = _control != null;
        }

        public void Dispose()
        {
            var disposable = _control as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
    }
}
