﻿using System.Linq;

using NeuralNodes.Editor.Controls;
using NeuralNodes.Editor.Ports;
using NeuralNodes.NeuralNodes;

using UnityEngine;

namespace NeuralNodes.Editor.Nodes
{
    [Title("Basic", "Vector1")]
    [NodeEditorType(typeof(Vector1NeuralNode))]
    public class Vector1NeuralNodeEditor : AbstractNeuralNodeEditor
    {
        [SerializeField]
        private float _value;

        [SerializeField]
        private bool _boolValue;

        private static readonly string[] Labels = {"X"};
        
        [NodeToggleControl("Bool")]
        public bool BoolValue
        {
            get { return _boolValue; }
            set
            {
                _boolValue = value;
                SetDirty();
            }
        }
        
        public override void ConstructNode()
        {
            var index = GetInputSlots<NeuralSlot>().Count() + 1;
            AddSlot(new VectorNeuralSlot(index,
                this,
                nameof(Vector1NeuralNode.Vector1Input),
                "V1Input",
                SlotDirection.Input,
                Labels,
                () => new Vector4(_value, 0, 0, 0),
                (newValue) => _value = newValue.x));
            index = GetInputSlots<NeuralSlot>().Count() + 1;
            AddSlot(new VectorNeuralSlot(
                index,
                this,
                nameof(Vector1NeuralNode.Vector1Output),
                "V1Output",
                SlotDirection.Output,
                Labels,
                null,
                null));
        }
    }
}