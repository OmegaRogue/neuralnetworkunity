﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DD_DragBar : MonoBehaviour, IDragHandler
{
	private GameObject    m_DataDiagram;
	private RectTransform m_DataDiagramRT;
	private GameObject    m_Parent;

	private DD_ZoomButton m_ZoomButton;

	public bool canDrag
	{
		get { return gameObject.activeSelf; }
		set
		{
			var le = GetComponent<LayoutElement>();
			if (null == le)
			{
				Debug.LogWarning(this + " : can not find LayoutElement");
			}
			else
			{
				if (value)
				{
					gameObject.SetActive(true);
					le.ignoreLayout = false;
				}
				else
				{
					gameObject.SetActive(false);
					le.ignoreLayout = true;
				}
			}
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (null == m_DataDiagramRT)
			return;

		m_DataDiagramRT.anchoredPosition += eventData.delta;
	}

	// Use this for initialization
	private void Start()
	{
		GetZoomButton();

		var dd = GetComponentInParent<DD_DataDiagram>();
		if (null == dd)
		{
			Debug.LogWarning(this + " : can not find any gameobject with a DataDiagram object");
			return;
		}

		m_DataDiagram = dd.gameObject;

		m_DataDiagramRT = m_DataDiagram.GetComponent<RectTransform>();

		if (null == m_DataDiagram.transform.parent)
			m_Parent = null;
		else
			m_Parent = m_DataDiagram.transform.parent.gameObject;
		if (null == m_Parent)
		{
			Debug.LogWarning(this + " : can not DataDiagram's parent");
			return;
		}

		//默认情况如果DataDiagram插件不在UI的最顶层，则不允许拖拽
		if (null == m_Parent.GetComponent<Canvas>())
			canDrag = false;
		else
			canDrag = true;
	}

	private void GetZoomButton()
	{
		if (null == m_ZoomButton)
		{
			var g = GameObject.Find("ZoomButton");
			if (null == g)
			{
				Debug.LogWarning(this + " : can not find gameobject ZoomButton");
			}
			else
			{
				if (null == g.GetComponentInParent<DD_DataDiagram>())
				{
					Debug.LogWarning(this + " : the gameobject ZoomButton is not under the DataDiagram");
				}
				else
				{
					m_ZoomButton = g.GetComponent<DD_ZoomButton>();
					if (null == m_ZoomButton)
						Debug.LogWarning(this + " : can not find object DD_ZoomButton");
					else
						m_ZoomButton.ZoomButtonClickEvent += OnCtrlButtonClick;
				}
			}
		}
		else
		{
			m_ZoomButton.ZoomButtonClickEvent += OnCtrlButtonClick;
		}
	}

	private void OnCtrlButtonClick(object sender, ZoomButtonClickEventArgs e)
	{
		if (null == m_DataDiagram.transform.parent)
		{
			Debug.LogWarning(this + " OnCtrlButtonClick : can not DataDiagram's parent");
			return;
		}

		if (m_Parent != m_DataDiagram.transform.parent.gameObject)
		{
			m_Parent = m_DataDiagram.transform.parent.gameObject;
			if (null != m_Parent.GetComponent<Canvas>())
				canDrag = true;
			else
				canDrag = false;
		}
	}
}